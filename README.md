**Research**
-
You can find more details on ReScan's methodology, architecture etc. in our research paper:

**_"ReScan: A Middleware Framework for Realistic and Robust Black-box Web Application Scanning"_** Kostas Drakonakis, Sotiris Ioannidis and Jason Polakis, NDSS 2023.

If you use ReScan for your research please [cite](https://dblp.org/rec/conf/ndss/DrakonakisIP23.html?view=bibtex) our paper.

**Setup**
-
 ReScan was implemented as a module on top of (and extended) [XDriver](https://gitlab.com/kostasdrk/xdriver3-open/).
Implemented and tested on Ubuntu 16.04 and 20.04 with python3.8. Should work on other debian-based systems as well.
* Clone the repo and `cd` into it
* `$ ./setup.sh`
* When prompted, download the webdriver binaries you need and place them under `./browsers/config/webdrivers`. If you want to do this at a later time, place them at that location in the install directory (e.g. `/usr/local/lib/python3.8/dist-packages/xdriver`)
* Make sure the webdrivers' versions are compatible with the corresponding browsers' version
* Run `./xutils/proxy/mitm/mitmdump` to create the `~/.mitmproxy` dir and add the generated mitmproxy certificate in the browsers' trust stores


**Run**
-

`$ ./rescan.py --help` to see the available options.

By default, ReScan runs on port 9191. Simply configure your web scanner/crawler to use http://127.0.0.1:9191 as a proxy.


