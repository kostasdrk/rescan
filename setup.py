from setuptools import setup, find_packages

setup(
  name = 'xdriver', # package name folder
  packages = ['xdriver',
			  'xdriver.browsers', 'xdriver.browsers.config',
			  'xdriver.browsers.config.webdrivers', 'xdriver.browsers.config.extensions',
			  'xdriver.js', 'xdriver.js.jak', 
			  'xdriver.xutils', 'xdriver.xutils.forms', 
			  'xdriver.xutils.proxy', 'xdriver.xutils.proxy.mitm',
			  'xdriver.rescan',
			  'xdriver.testing'],   # package name
  # packages = find_packages(include=["xdriver", "xdriver.*"]),   # package name
  package_dir = {
	'xdriver' : '.',
  },
  package_data = {'' : ["*.js", "*.json", "*.sh", "*.xdt", "*.tx", "chromedriver", "geckodriver", "operadriver", "mitmdump"]},
  version = '0.1', # version number
  license= None, # License e.g. 'MIT', "GPL2.0"
  description = '',   # Package descriptin
  author = 'Anon',
  author_email = 'anonk@example.com',
  url = '', # Github URL, e.g. https://github.com/user/reponame
  download_url = 'http://example.com', # Github download url, e.g. https://github.com/user/reponame/archive/v_01.tar.gz
  keywords = ['selenium', 'browser', 'automation', 'security'],
  install_requires = [ # Dependencies
		  "tldextract==2.2.3",
		  "Faker==8.1.3",
		  "PyVirtualDisplay==1.3.2",
		  "psutil==5.7.2",
		  "requests==2.20.1",
		  "googletrans==3.0.0",
		  "selenium==3.141.0",
		  "beautifulsoup4==4.8.2",
		  "apted==1.0.3",
		  "pandas==1.2.4",
		  "scipy==1.6.0",
		  "html5lib==1.1"
	  ],
  dependency_links = [ # Dependencies not in PyPI
  ],
  classifiers = [
	'Development Status :: 4 - Beta',  # Pick from "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as current state of the package
	'Intended Audience :: Security Researchers, Security Engineers', # Targeted audience
	'Topic :: Software Development :: ?',
	'License :: OSI Approved :: ?',   # Same as before
	'Programming Language :: Python :: 3.8' # Supported python versions
  ],
)
