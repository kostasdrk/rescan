#!/usr/bin/python3

from xdriver.XDriver import XDriver
from xdriver.xutils.proxy.ProxyWrapper import ProxyWrapper
from xdriver.xutils.Logger import Logger
from xdriver.xutils.URLUtils import URLUtils
from xdriver.xutils.Misc import uuid, pdump, generate_token, fix_html
from xdriver.xutils.Regexes import Regexes
from xdriver.xutils.ModuleTimer import Timer
Logger.set_debug_on()
from xdriver.xutils.Exceptions import stringify_exception
from xdriver.rescan.Graph import Edge, EdgeTypes, Node
from xdriver.rescan.BackgroundWorker import BackgroundWorker

from selenium.common.exceptions import StaleElementReferenceException

from bs4 import BeautifulSoup

from time import sleep
from copy import deepcopy

import sys
import os
import pickle as cp
from json import dumps as JSONdumps
import re
import traceback
from urllib.parse import urlparse,parse_qs

import pandas as pd
from scipy.stats import entropy

from datetime import datetime
from time import time

# Concurrency imports #
from queue import Empty

class Worker(object):
	_caller_prefix = "Worker"

	_reporting_interval = 300 # Report every 5 minutes
	_reboot_interval = 200 # Reboot browser every X requests

	def __init__(self, wid, target, request_queue, available_workers, submit_pipe, request_pipe, known_headers, isd, isd_links, credentials, auth, auth_oracle, authLax, injection_requests, successful_injections, done, events, url_events, cached_responses, submitted_posts, shared_cookies, unique_tokens, headless, stats_queue, api_url, xproxy_port = None):
		self._id = wid
		self._caller_prefix = "%s-%s" % (Worker._caller_prefix, self._id)

		self._api_url = api_url

		self._target = target

		self._request_pipe = request_pipe
		self._submit_pipe = submit_pipe
		self._stats_queue = stats_queue

		self._shared_cookies = shared_cookies

		self._url_events = url_events

		self._cached_responses = cached_responses

		self._isd_links = isd_links
		self._isd = isd

		self._submitted_posts = submitted_posts
		self._unique_tokens = unique_tokens

		self._injection_requests = injection_requests
		self._successful_injections = successful_injections

		self._request_queue = request_queue
		self._available_workers = available_workers
		self._done = done
		self._events = events

		''' Store first seen request headers; we don't want these to persist, i.e. we need to send the browser's headers, not the scanner's. (except cookies)
			 However, we'll use those to detect when the scanner tries to send a payload inside the headers; in such cases we need to proxy the scanner's specific header value. '''
		self._known_headers = known_headers

		if xproxy_port:
			XDriver.enable_proxy(port = xproxy_port) # Route all traffic through custom proxy for debugging (or store the flows, whatever). NOTE: All concurrent browsers will use the same proxy

		XDriver.enable_browser_args(headless = headless) # Only way to avoid multiple browsers' overhead. We can combine it with our evasion techniques to appear realistic
		XDriver.enable_internal_proxy(strip_media = True) # Reduce load overheads by stripping media files from responses
		XDriver.enable_mutation_observer()
		XDriver.enable_xhr_capture()
		self._driver = XDriver.boot(chrome = True)
		self._available_workers.value += 1
		
		self._auth_helper = auth
		self._auth_oracle = auth_oracle
		self._credentials = credentials
		self._authLax = authLax

		self._served_requests = 0 # Keep count of served requests, so we can reboot the browser instance and underlying subprocesses every `_reboot_interval` requests
	
	def _reserve_request(self):
		try: return self._request_queue.get_nowait()
		except Empty: return None

	def _submit_new_edges(self):
		src_url = URLUtils.strip_fragment(self._current_url) # Remove fragments from current URL to get base source URL for graph
		
		new_edges = []
		updated_edge = (self._edge.get_src_url(), self._edge.get_dst_url(), self._edge.get_parent_edge_id(), self._edge.get_type(), self._edge.get_payload(), self._edge.get_info())
		new_edges.append(updated_edge)
		parent_edge = self._edge

		if self._redirected_edge:
			new_edges.append((self._redirected_edge.get_src_url(), self._redirected_edge.get_dst_url(), self._redirected_edge.get_parent_edge_id(), EdgeTypes.REDIRECT, None, None))
			parent_edge = self._redirected_edge # If we were redirected, we need to update the parent for the remaining edges
		
		links = self._driver.get_internal_links(strip_files = True)
		form_descriptions = self._driver.get_form_descriptions()
		iframes = self._driver.get_iframes_src()
		num_of_edges = len(links) + len(form_descriptions) + len(iframes)

		for link in links: new_edges.append((src_url, URLUtils.strip_fragment(link), parent_edge.get_id(), EdgeTypes.GET, None, None))
		for fd in form_descriptions: new_edges.append((src_url, URLUtils.strip_fragment(fd[0]), parent_edge.get_id(), EdgeTypes.FORM, fd[2], {"xpath" : fd[3], "method" : fd[1]}))
		for iframe_src, dompath in iframes: new_edges.append((src_url, URLUtils.strip_fragment(iframe_src), parent_edge.get_id(), EdgeTypes.IFRAME, None, {"xpath" : dompath}))

		self._add_event_edges(new_edges, self._events_chains, src_url, parent_edge.get_id()) # Add all edges stemming from the event triggering phase; see below; We have already counted these during the discovery phase
		
		for edge in new_edges: self._submit_pipe.put(edge)

		if self._cached_responses:
			urls = list(links) + [frame[0] for frame in iframes]
			if self._redirected_edge:
				urls.append(self._redirected_edge.get_dst_url())

			for url in urls:
				if not URLUtils.get_query_parameters(url): continue # Not interested in URLs without query
				normalized_url = URLUtils.normalize_url(url)
				if normalized_url not in self._cached_responses: self._cached_responses[normalized_url] = None # Simply store the (possibly) similar URL in the BackgroundWorker's cache dict

		return len(new_edges)

	def _add_event_edges(self, new_edges, event_chains, src_url, parent_edge_id):
		for event, dompath in event_chains:
			edge_id = Edge.normalize_edge_id((EdgeTypes.EVENT, src_url, dompath))
			for xhr in event_chains[(event, dompath)]["xhrs"]:
				req_data = URLUtils.payload_to_dict(xhr[2]) if xhr[2] else xhr[2]
				new_edges.append((src_url, xhr[1], parent_edge_id, EdgeTypes.EVENT, "&".join(req_data.keys()) if req_data else None, {"events" : True, "xpath" : dompath, "event" : event, "method" : xhr[0], "req_data" : req_data, "xhr_tuple" : xhr})) # Add XHR edges
			if event_chains[(event, dompath)]["dom_changes"] or event_chains[(event, dompath)]["events"]:
				new_edges.append((src_url, src_url, parent_edge_id, EdgeTypes.EVENT, dompath, {"xpath" : dompath, "event" : event, "events" : True})) # Make sure we add an intermmediary unique edge (due to the dompath) for further nested events and DOM changes
			
			for dom_str, dom in event_chains[(event, dompath)]["dom_changes"]: # Add dynamic DOM edges
				sub_edge_type = EdgeTypes.GET if dom[0] == "a" else EdgeTypes.FORM if dom[0] == "form" else EdgeTypes.IFRAME
				sub_edge_paylaod = dom[2] if dom[0] == "form" else None
				sub_edge_info = {} if dom[0] == "a" else {"xpath" : dom[-1]}
				sub_edge_info["dom_str"] = dom_str
				if dom[0] == "form": sub_edge_info["method"] = dom[3]
				new_edges.append((src_url, dom[1], edge_id, sub_edge_type, sub_edge_paylaod, sub_edge_info))
			
			if event_chains[(event, dompath)]["events"]: self._add_event_edges(new_edges, event_chains[(event, dompath)]["events"], src_url, edge_id)

	# Given the incoming request info, construct its edge ID and ask the graph worker about it
	# Once the ID is validated/mapped/found, the graph worker also returns the workflow towards that edge
	def _get_edge_id_and_flow(self, req_method, req_url, req_data):
		if req_method == "REDIRECT": edge_type = EdgeTypes.REDIRECT # Explicitly checking for a redirect's edge
		else: edge_type = EdgeTypes.GET if req_method == "GET" and not req_data else EdgeTypes.FORM # Incoming requests can only be initially mapped to GET/FORM edges
		req_data = "&".join([key for key in req_data.keys() if key != "xdriver_filenames"]) if req_data else None
		edge_id = Edge.normalize_edge_id((edge_type, req_url, req_data))
		
		self._request_pipe.send(edge_id)
		# Besides the flow, get all dynamic info (XHRs, DOM changes) from a previous run, if we have loaded an existing graph, else simply initialize them
		response = self._request_pipe.recv()
		if req_method != "REDIRECT": graph_flow, self._recorded_requests, self._recorded_DOM_changes = response
		else: graph_flow, ignore, ignore = response
		if graph_flow: edge_id = graph_flow[-1].get_id() # Retrieve the final edge id
		return (edge_id, graph_flow)

	def _execute_flow(self, req_tuple):
		req_id, req_method, req_url, req_headers, req_data = req_tuple

		self._edge._info["req_data"] = req_data # Store the actual request data and method in the Edge (local; will be passed on later to the GraphWorker)
		self._edge._info["method"] = req_method

		submitted_values = []
		next_step = None # Used for EVENT look-aheads

		Logger.spit("Executing flow: %s" % str(self._graph_flow), debug = True, caller_prefix = self._caller_prefix)
		self._setup_cookiejar(req_url, req_headers) # Firstly, set the browser's cookie jar to acquire state
		self._driver.spoof_headers(headers = {}, domain_headers = [], reset = True, enabled = False) # Make sure any pre-set spoofed headers are overwritten
		flow = None
		for edge in self._graph_flow:
			if next_step and edge != next_step: # Skip intermmediary edges, until we reach the wanted edge
				Logger.spit("Skipping intermediary edge %s" % str(edge), debug = True, caller_prefix = self._caller_prefix)
				continue
			next_step = None # Make sure we reset it once reached

			edge_type = edge.get_type()
			edge_dst_url = edge.get_dst_url()
			edge_payload = edge.get_payload()
			edge_info = edge.get_info()

			# In case of an IFRAME or REDIRECT edge, we need to get the redirection flow when visiting the previous edge, as the actual request will not be trigggered by us
			if (self._edge_id[0] == EdgeTypes.IFRAME or self._edge_id[0] == EdgeTypes.REDIRECT) and edge == self._graph_flow[-2]:
				# self._setup_headers(req_url, req_headers) # We need to setup the request headers according to what the scanner sent for the *final* request
				self._driver._proxy.redirection_mode(self._edge_id[1], html = True, strict_query = False)
			elif self._edge_id[0] != EdgeTypes.IFRAME and self._edge_id[0] != EdgeTypes.REDIRECT and edge == self._edge: # If this is the last edge (requested), get the redirection flow to get all needed data and craft the response (i.e. status code, headers etc.)
				# self._setup_headers(req_url, req_headers) # We need to setup the request headers according to what the scanner sent for the *final* request
				self._driver._proxy.redirection_mode(edge_dst_url, html = True)
			
			if edge_type == EdgeTypes.REDIRECT: # No need to do anything for redirects
				Logger.spit("Exec REDIRECT %s" % edge_dst_url, debug = True, caller_prefix = self._caller_prefix)
				if URLUtils.normalize_url(self._driver.current_url()) != edge_dst_url:
					Logger.spit("Redirect mismatch (%s  ,  %s)" % (edge_dst_url, self._driver.current_url()), warning = True, caller_prefix = self._caller_prefix)
			elif edge_type == EdgeTypes.GET:
				Logger.spit("Exec GET %s" % edge_dst_url, debug = True, caller_prefix = self._caller_prefix)
				self._driver.get(edge_dst_url)
			elif edge_type == EdgeTypes.FORM:
				filenames = edge_info["req_data"].pop("xdriver_filenames", {})
				css_selector = edge_info.get("xpath")
				exists = self._driver.find_element_by_css_selector(edge_info.get("xpath")) if css_selector else False

				if not css_selector: Logger.spit("FORM edge missing CSS selector: %s" % str(edge), warning = True, caller_prefix = self._caller_prefix)
				if css_selector and not exists:
					Logger.spit("FORM does not exist. %s generate it. (%s, %s)" % ("Will" if edge == self._edge else "Will NOT" , edge_info.get("xpath"), self._driver.current_url()), warning = True, caller_prefix = self._caller_prefix)

				if (not css_selector or not exists) and edge != self._edge:
					Logger.spit("Skipping intermediary FORM edge", warning = True, caller_prefix = self._caller_prefix)
					continue
				Logger.spit("Exec FORM %s %s, data: %s, filenames: %s" % (edge_info["method"], edge_dst_url, str(edge_info["req_data"]), str(filenames)), debug = True, caller_prefix = self._caller_prefix)

				submitted_values = self._driver.create_and_fill_form_js(edge_info.get("xpath"), edge_info["req_data"], edge_dst_url, edge_info["method"], filenames = filenames, create = edge == self._edge) # Only create form (if not found), for the final/requested edge (last argument)
				if not submitted_values: Logger.spit("Skipping FORM: %s" % str(edge.get_id()) , debug = True, caller_prefix = self._caller_prefix)
				if edge != self._edge: submitted_values = [] # Only store submitted vales for the requested edge
				else: Logger.spit("Submitted FORM values %s" % str(submitted_values), debug = True, caller_prefix = self._caller_prefix)
			elif edge_type == EdgeTypes.IFRAME:
				Logger.spit("Exec IFRAME %s" % edge_dst_url, debug = True, caller_prefix = self._caller_prefix)
				iframe = self._driver.find_element_by_xpath(edge_info["xpath"])
				if iframe: self._driver.switch_to_frame(iframe)
			elif edge_type == EdgeTypes.EVENT:
				for check_edge in self._graph_flow[-1:self._graph_flow.index(edge):-1]: # Traverse the rest of the edges in reverse, checking if any one exists and is displayed
					edge_css_selector = check_edge.get_info().get("xpath")
					if not edge_css_selector: continue
					edge_element = self._driver.find_element_by_css_selector(edge_css_selector)
					if edge_element and self._driver.is_displayed(edge_element):
						Logger.spit("Event look ahead worked: %s\n\t%s" % (str(self._graph_flow), str(check_edge)), debug = True, caller_prefix = self._caller_prefix)
						next_step = check_edge
						break
				if next_step: continue # Look-ahead found an existing edge, so move directly to it

				# Much like the discovery phase, fill all inputs/select/textarea elements in the page, this time based on the scanner provided payload
				# in case the event needs a filled input to fire its XHR
				if "req_data" in edge_info: self._driver.fill_all_inputs(edge_info["req_data"])

				if edge == self._edge and edge_info.get("method"): # For request triggering events, spoof payload with original request's payload, method and headers
					Logger.spit("Exec XHR EVENT (%s %s) %s %s, data: %s" % (edge_info["event"], edge_info["xpath"], edge_info.get("method", "?"), edge_dst_url, str(edge_info.get("req_data"))), debug = True, caller_prefix = self._caller_prefix)
					self._driver.tamper_request(edge_dst_url, tamper = {"method" : edge_info.get("method"), "payload" : edge_info.get("req_data")}, conservative = True) # Tamper event's request to set scanner's payload on the fly -- `conservative` mode to make sure we don't overwrite any CSRF tokens
				else: Logger.spit("Exec DOM EVENT %s %s" % (edge_info["event"], edge_info["xpath"]), debug = True, caller_prefix = self._caller_prefix)

				triggered = self._driver.trigger_event(None, edge_info["event"], css_selector = edge_info["xpath"]) # Common for both XHR and DOM changing events
				if triggered: Logger.spit("Executed EVENT %s %s" % (edge_info["event"], edge_info["xpath"]), debug = True, caller_prefix = self._caller_prefix)
				
				if edge == self._edge and triggered and req_data: # Only store submitted values (if any) for the requested edge
					submitted_values = [(name, "", value) for name, value in req_data.items()]
					Logger.spit("Submitted EVENT values %s" % str(submitted_values), debug = True, caller_prefix = self._caller_prefix)
			self._check_injections() # Check for successful injections right after the execution of each edge in the workflow

		flow = self._driver._proxy.get_redirection_flow(timeout = 10)

		self._driver.tamper_request(None, enabled = False) # In case we tampered with a request, make sure we turn it off
		return (flow, submitted_values)

	def _setup_cookiejar(self, req_url, req_headers):
		for cookie in self._driver.get_cookies(): # Initially purge all cookies
			try: self._driver.delete_cookie(cookie["name"])
			except Exception as ex: print("%s  -  %s  " % (cookie["name"], str(ex)))

		if "cookie" not in req_headers: return # No cookies from scanner? Return

		cookie_string = req_headers["cookie"]
		for token in [cookie.split("=") for cookie in cookie_string.split(";")]:
			cookie_name = token[0].strip()
			cookie_value = "=".join(token[1:]).strip()
			if not cookie_name: continue

			try:
				self._driver.add_cookie({"name" : cookie_name, "value" : cookie_value, "domain" : "."+URLUtils.get_main_domain(req_url, suffix = True), "path" : "/", "secure" : False, "httpOnly" : False})
			except Exception as ex:
				Logger.spit("Exception when setting cookie: %s" % stringify_exception(ex), warning = True, caller_prefix = self._caller_prefix)
				Logger.spit(self._driver.current_url(), debug = True, caller_prefix = self._caller_prefix)
				Logger.spit(cookie_string, debug = True, caller_prefix = self._caller_prefix)
				Logger.spit((cookie_name, cookie_value), debug = True, caller_prefix = self._caller_prefix)

	def _setup_headers(self, req_url, req_headers):
		ignored_headers = set(["cookie", "content-length", "content-type", "content-encoding", "content-language"])
		spoofed_headers = {}
		for header, value in req_headers.items():
			if header in ignored_headers: continue # "Cookie" headers have been already setup in the browser's cookie jar. Content-length/type etc. must be set automatically by the browser
			if header in self._known_headers and self._known_headers[header] == value: continue # Known header with known value from scanner; skip
			Logger.spit("Diff scanner header: %s (%s -> %s)" % (header, self._known_headers.get(header), value), debug = True, caller_prefix = self._caller_prefix)
			spoofed_headers[header] = value

		if spoofed_headers: self._driver.spoof_headers(domain_headers = [{"domain" : self._target.value, "headers" : spoofed_headers}], reset = True) # Instruct XDriver's proxy to add the spoofed headers for the target domain only
		else: self._driver.spoof_headers(headers = {}, domain_headers = [], reset = True, enabled = False) # Make sure header spoofing is disabled


	def _store_stats(self, info, stats):
		self._stats_queue.put((info, stats))

	def _is_api_call(self, url):
		return url.startswith(self._api_url)

	def _handle_api_call(self, req_object):
		req_id, req_method, req_url, req_headers, req_data = req_object # Make sure the object can be de-pickled safely
		parsed = urlparse(req_url)
		path = parsed.path
		params = parse_qs(parsed.query)
		response = {}

		if path == "/graph":
			self._request_pipe.send(("GRAPH", ""))
			response["graph"] = self._request_pipe.recv()
		elif path == "/workflow":
			if "method" not in params: response["error"] = "Missing `method` parameter"
			elif "url" not in params: response["error"] = "Missing `url` parameter"
			elif "params" not in params: params["params"] = {}
			else:
				edge_id, flow = self._get_edge_id_and_flow(params["method"][0], params["url"][0], {key : '' for key in params["params"][0].split(",")})
				response["flow"] = [edge for edge in map(str, flow)]
		elif path == "/isd":
			if "method" not in params: response["error"] = "Missing `method` parameter"
			elif "url" not in params: response["error"] = "Missing `url` parameter"
			elif "params" not in params: response["error"] = "Missing `params` parameter"
			else:
				edge_id, flow = self._get_edge_id_and_flow(params["method"][0], params["url"][0], {key : '' for key in params["params"][0].split(",")})
				response["sink_urls"] = {}
				for param_name in params["params"][0].split(","):
					isd_src = (edge_id, param_name)
					if isd_src not in self._isd_links: continue
					response["sink_urls"][param_name] = list(set(self._isd_links[isd_src]))
		elif path == "/isdAll":
			response["isd_links"] = {}
			for isd_src in self._isd_links.keys():
				if not self._isd_links[isd_src]: continue
				edge_id = str(isd_src[0])
				param_name = isd_src[1]
				if edge_id not in response["isd_links"]: response["isd_links"][edge_id] = {}
				if self._isd_links[isd_src]: response["isd_links"][edge_id][param_name] = list(set(self._isd_links[isd_src]))
		elif path == "/auth":
			if not self._auth_oracle.get("method"): response["error"] = "Auth oracle has not be determined. Did you issue a login request?"
			else: response["authed"]  = self._oracle()
		else:
			Logger.spit("Unrecognized API call: %s" % path, warning = True, caller_prefix = self._caller_prefix)
			response["error"] = "Unrecognized API call: %s" % path

		status = 200 if "error" not in response else 400
		pdump((req_id, JSONdumps(response), status, {}), "/tmp/%s.pickle" % req_id)


	def start(self):
		custom_request = None
		initial_request = None
		report_time = time()
		first_req = True
		try:
			while not self._done.value: # Work until the main process says we're done
				if time() - report_time >= Worker._reporting_interval:
					report_time = time()
					Logger.spit("[%s] Alive" % datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), caller_prefix = self._caller_prefix)
				req_tuple = self._reserve_request() if not custom_request and not initial_request else custom_request if custom_request else initial_request
				if not req_tuple: continue # Keep requesting

				if self._is_api_call(req_tuple[2]):
					self._handle_api_call(req_tuple)
					continue

				is_injection = self._is_injection(req_tuple)
				Logger.spit("Reserved%srequest: %s %s (%s)" % (" injection " if is_injection else " ", req_tuple[1], req_tuple[2], req_tuple[0]), debug = True, caller_prefix = self._caller_prefix)
				
				if re.search(Regexes.LOGOUT, req_tuple[2]) or "/cart/action.php?action=delete" in req_tuple[2]: # TMP
					Logger.spit("Skipping logout request: %s" % req_tuple[2], warning = True, caller_prefix = self._caller_prefix)
					pdump((req_tuple[0], "", 302, {"Location" : self._target.value}), "/tmp/%s.pickle" % req_tuple[0]) # Send final response object to ReScan proxy so it can craft the response
					continue

				req_id, req_method, req_url, req_headers, req_data = req_tuple
				self._cookie_bearing_request = bool(req_headers.get("cookie", False))

				total_timer = Timer()

				# Necessary check for workers that are still on google.com (xdriver's heartbeat) or were redirected outside the target app and are about to set cookies for the domain we evaluate
				try:
					if URLUtils.get_main_domain(req_url, suffix = True) != URLUtils.get_main_domain(self._driver.current_url(), suffix = True):
						Logger.spit("Changing initial URL to %s" % req_url, debug = True, caller_prefix = self._caller_prefix)
						self._driver.get(req_url)
				except Exception as ex:
					Logger.spit("Exception when checking domain: (%s, %s): %s" % (req_url, self._driver.current_url(), stringify_exception(ex)), debug = True, caller_prefix = self._caller_prefix)
					Logger.spit("Falling back to base target %s" % self._target.value, debug = True, caller_prefix = self._caller_prefix)
					self._driver.get(self._target.value)
				

				try:
					''' Graph retrace/replay
					'''
					self._arbitrary = False
					self._driver.switch_to_default_content() # Make sure we are in the main frame
					
					timer = Timer()
					self._edge_id, self._graph_flow = self._get_edge_id_and_flow(req_method, req_url, req_data)
					self._store_stats("GET_FLOW", {"time" : timer.end(), "flow_len" : len(self._graph_flow) if self._graph_flow else 0})
					Logger.spit("Got flow for %s %s in %.2fs" % (req_method, req_url, timer.get_time()), debug = True, caller_prefix = self._caller_prefix)

					if not self._graph_flow:
						self._arbitrary = True if not first_req else False
						self._graph_flow = [Edge(None, Node(self._edge_id[1]), None, self._edge_id[0], payload = self._edge_id[2], info = {"arbitrary" : not first_req})]
						Logger.spit("Arbitrary edge: %s %s , data: %s" % (req_method, req_url, req_data), warning = self._edge_id[0] != EdgeTypes.GET, debug = self._edge_id[0] == EdgeTypes.GET, caller_prefix = self._caller_prefix) # Do not report arbitrary GET requests as warnings, as most of them will be scanner injections in URL parameters
					self._edge = self._graph_flow[-1]

					### Auth helper ###
					auth_req = False
					if req_data: # Only for payload-bearing requests
						username_field = None
						username_fields = []
						for key in req_data.keys():
							if re.search(Regexes.PASSWORD, key, re.IGNORECASE) and req_data[key]:
								''' Only store the password the first time we observe it. Rationale is that the first auth-like request
									we see will use the credentials we provide; subsequent auth-like requests might (and usually) use the scanner's defaults, e.g. username: ZAP, passwd: ZAP '''
								if "password" not in self._credentials:
									self._credentials["password"] = req_data[key]
									Logger.spit("Captured password: %s" % req_data[key], caller_prefix = self._caller_prefix)
								if self._auth_helper.value: Logger.spit("Overwriting passwd field %s: %s with %s" % (key, req_data[key], self._credentials["password"]), caller_prefix = self._caller_prefix)
								req_data[key] = self._credentials["password"] if self._auth_helper.value else req_data[key] # Overwrite with known password. Even if this is not a login request, we want to use the same passwd, e.g. if we are changing the account's password
								auth_req = True # mark as auth-like request
							if re.search(r"u(ser)?(-|_)?name|e(-|_)?mail|^log$|^name$", key, re.IGNORECASE) and req_data[key] and " " not in key: # Watch out for composite parameter names. They do exist.
								if "username" not in self._credentials:
									self._credentials["username"] = req_data[key] # Similar to passwd
									Logger.spit("Captured username/email: %s" % req_data[key], caller_prefix = self._caller_prefix)
								username_fields.append(key) # Might appear more than once, e.g. confirming the e-mail address in account settings page
								username_field = key # We can't replace the field immediately, since this might not be an auth request; store the field for now
						if username_field: # Only overwrite the username if we did so for the password too
							for username_field in username_fields:
								if self._auth_helper.value: Logger.spit("Overwriting username field %s: %s with %s" % (username_field, req_data[username_field], self._credentials["username"]), caller_prefix = self._caller_prefix)
								req_data[username_field] = self._credentials["username"] if self._auth_helper.value else req_data[username_field]
					if auth_req and "username" not in self._credentials: auth_req = False # Avoid cases where only a passwd is sent with an empty username

					if auth_req and not custom_request and not initial_request and self._arbitrary: # If arbitrary and auth-looking request
						initial_request = req_tuple
						custom_request = ("999", "GET", req_url, req_headers, {}) # Inject custom GET request in the same URL, so we can attempt to locate and model the login form in the graph, *before* we execute the FORM edge
						Logger.spit("Injecting custom request... (%s)" % req_url, caller_prefix = self._caller_prefix)
						continue

					timer = Timer()
					Logger.spit("Will execute flow for %s %s (%s edges)" % (req_method, req_url, len(self._graph_flow)), debug = True, caller_prefix = self._caller_prefix)
					redirection_flow, submitted_values = self._execute_flow(req_tuple)
					self._store_stats("EXECUTE_FLOW", {"time" : timer.end(), "flow" : [edge.get_type() for edge in self._graph_flow]})
					Logger.spit("Executed flow for %s %s (%s edges) in %.2fs" % (req_method, req_url, len(self._graph_flow), timer.get_time()), debug = True, caller_prefix = self._caller_prefix)

					first_auth_req = False
					if self._auth_helper.value and auth_req and not self._auth_oracle: # If authentication helper is enabled, this is an auth request and we haven't detected our oracle yet, detect it once
						Logger.spit("First auth-like request. Will try to detect oracle", debug = True, caller_prefix = self._caller_prefix)
						login_url = self._graph_flow[0].get_dst_url()
						landing_page = self._driver.current_url()
						if self._detect_auth_oracle(login_url, landing_page): # The first edge of the workflow is the login page, the current url is the landing page after login
							self._auth_oracle["login_url"] = login_url
							self._auth_oracle["auth_req"] = req_tuple # Remember precise auth request info, so we can use it for relogins
							self._auth_oracle["login_form_css_selector"] = self._edge.get_info().get("xpath")
							Logger.spit("Set login url to %s (%s)" % (login_url, self._auth_oracle["login_form_css_selector"]), caller_prefix = self._caller_prefix)
							first_auth_req = True # Remember this is the first auth request, so we don't retry it in case of relogin
						self._driver.get(landing_page) # Return to landing page so we can go on with the rest of the steps

					if self._auth_helper.value and not custom_request and self._cookie_bearing_request and "username" in self._credentials and "password" in self._credentials:
						login_forms = self._driver.get_login_forms() # If we detect any login form at the landing page, submit it with the detected credentials (if complete). Skip for cookie-less requests, as we expect these to break the session
						if login_forms: # Useful as proactive re-login (if needed), and also for double-logins
							Logger.spit("Detected login form. Will try to relogin with detected credentials...", warning = True, caller_prefix = self._caller_prefix)
							try: self._driver.fill_and_submit(login_forms[0], override_rules = {Regexes.USERNAME : self._credentials["username"], Regexes.EMAIL : self._credentials["username"], Regexes.PASSWORD : self._credentials["password"]})
							except StaleElementReferenceException as ex: Logger.spit("Stale login form, moving on...", warning = True, caller_prefix = self._caller_prefix)

					relogin = False # This will indicate if the current edge leads to a broken session. If it does we always relogin, so we don't have to execute all other mechanisms (ISD, events etc.)
					if self._auth_helper.value and not custom_request and (self._cookie_bearing_request or first_auth_req) and self._auth_oracle.get("method"): # If auth helper is enabled and we have detected our oracle
						timer = Timer()
						logged_in = self._oracle()
						self._store_stats("ORACLE", {"time" : timer.end()})
						if not logged_in: # Session broke, need to relogin
							timer = Timer()
							relogin = self._relogin()
							self._store_stats("RELOGIN", {"time" : timer.end()})
							if not relogin: # Could not relogin, something is very wrong with the app
								if not self._authLax:
									self._done.value = 1 # Fatal; Abort for everyone
									break
								else:
									Logger.spit("Cannot relogin for %s %s " % (req_tuple[1], req_tuple[2]), warning = True, caller_prefix = self._caller_prefix)
									Logger.spit("Auth oracle is not robust anymore. Will ignore.", error = True, caller_prefix = self._caller_prefix)
									self._auth_helper.value = 0 # Disable auth oracle for everyone, since we can't rely on it anymore
							
							if not first_auth_req and not initial_request: # Set request tuple to be retried *once* in the next iteration. If retried, this will not be executed again
								req_headers.pop("cookie", None)
								req_headers["cookie"] = self._cookies_to_string() # Make sure to update the request's Cookie header to the new cookies values after the relogin
								initial_request = (req_id, req_method, req_url, req_headers, req_data)
								Logger.spit("Retrying request: %s %s" % (req_tuple[1], req_tuple[2]), warning = True, caller_prefix = self._caller_prefix)
								continue

					if not first_auth_req and relogin:
						Logger.spit("Request breaks session %s %s" % (req_tuple[1], req_tuple[2]), warning = True, caller_prefix = self._caller_prefix)

					self._shared_cookies.value = self._cookies_to_string() # Make sure the BG worker will have a set of valid cookies (if the auth helper is enabled)
					if self._isd.value and not self._arbitrary and not auth_req and req_method == "POST" and req_data:
						Logger.spit("Pushing POST for ISD link detection (%s, %s)" % (req_method, req_url), debug = True, caller_prefix = self._caller_prefix)
						isd_req_data = deepcopy(req_data)
						isd_req_data.pop("xdriver_filenames", None)
						self._submitted_posts.put((self._edge_id, URLUtils.dict_to_payload(isd_req_data), time())) # Inform the BG worker of submitted POST values for ISD detection

					source = self._driver.rendered_source()
					self._current_url = URLUtils.normalize_url(self._driver.current_url()) # Update current URL to be the landing URL after all redirections
					self._redirected_edge = None
					if not relogin and (not self._arbitrary or first_req) and self._current_url != self._edge.get_dst_url():
						self._redirected_edge_id, _redirected_graph_flow = self._get_edge_id_and_flow("REDIRECT", self._current_url, None) # Need to get redirect info (e.g. if we already processed its events)
						self._redirected_edge = _redirected_graph_flow[-1] if _redirected_graph_flow else Edge(Node(self._edge.get_dst_url()), Node(self._current_url), self._edge, EdgeTypes.REDIRECT, None, None)
						Logger.spit("Got REDIRECT: %s ------> %s" % (self._edge_id, self._redirected_edge.get_id()) , debug = True, caller_prefix = self._caller_prefix)
						self._driver.setup_page_scripts() # Make sure to setup scripts after load (the mutation observer) for this redirection, as it is not automatically setup by XDriver

					self._check_injections()
					
					# Collect all iframe DOMs
					self._iframe_doms = []
					for iframe in self._driver.find_elements_by_tag_name("iframe") + self._driver.find_elements_by_tag_name("frame"):
						try:
							Logger.spit("Collected iframe DOM: %s" % self._driver.get_attribute(iframe, "src"), debug = True, caller_prefix = self._caller_prefix)
							self._driver.switch_to_frame(iframe)
							self._iframe_doms.append(self._driver.page_source())
						except Exception as ex: Logger.spit("Exception when collecting iframe: %s" % stringify_exception(ex), debug = True, caller_prefix = self._caller_prefix)
						finally: self._driver.switch_to_default_content()
					
					'''Inter-state dependency link detection
					'''
					new_values = []
					if not relogin and self._isd.value and redirection_flow: # Only work on ISD if enabled and we executed the flow properly
						ENTROPY_THRESHOLD = 1.4 # Used to identify current values of interest (candidate payloads) that we need to check for in the already detected sinks
						all_values = []
						for param_name, old_value, new_value in submitted_values:
							old_value = str(old_value)
							new_value = str(new_value)
							value_entropy = self._entropy(new_value)
							
							if old_value == new_value:
								Logger.spit("Pre-filled value: %s = %s" % (param_name, old_value), debug = True, caller_prefix = self._caller_prefix)
								continue # Pre-filled value in form

							if value_entropy >= ENTROPY_THRESHOLD:
								Logger.spit("New high-entropy value %s = %s" % (param_name, new_value) , debug = True, caller_prefix = self._caller_prefix)
								new_values.append((param_name, new_value))
					
					''' Event triggering
					'''
					# Record all DOM changes and async requests in the current page
					if self._recorded_DOM_changes or self._recorded_requests:
						Logger.spit("Retrieved %s DOM changes and %s XHRs from previous runs" % (len(self._recorded_DOM_changes), len(self._recorded_requests)), caller_prefix = self._caller_prefix)
					
					self._dom_edge_events = set()
					self._req_edge_events = set()

					self._events_chains = {}
					num_of_events = 0
					if not relogin and self._events.value and not auth_req and redirection_flow and ((not self._redirected_edge and not self._edge.get_info().get("events", False)) or (self._redirected_edge and not self._redirected_edge.get_info().get("events", False))):
						timer = Timer()
						self._driver.store_reference_element(self._current_url) # Make sure we store the reference element so we can check for redirections during event triggering
						Logger.spit("Will trigger events for %s" % self._current_url, debug = True, caller_prefix = self._caller_prefix)
						num_of_events = self._trigger_events() # Trigger events, *only* if: 1) enabled, 2) we executed the flow properly and 3) we haven't done so previosuly for this edge
						self._store_stats("EVENT_DISCOVERY", {"time" : timer.end(), "num_of_events" : num_of_events})
						Logger.spit("Triggered events for %s in %.2fs (%s)" % (self._current_url, timer.get_time(), num_of_events), debug = True, caller_prefix = self._caller_prefix)
						if self._redirected_edge: self._redirected_edge.get_info().update({"events" : True}) # Make sure we remember the event discovery phase was completed for the redirected edge
						else: self._edge.get_info().update({"events" : True}) # Make sure we remember the event discovery phase was completed for this edge

					''' Collect ISD-link responses
					'''
					self._recorded_sink_DOMs = set()
					if not relogin and self._isd.value and redirection_flow: # Collect sink DOMs, *only* if enabled and we executed the flow properly
						timer = Timer()
						Logger.spit("Will collect ISD sinks for %s" % str(self._edge_id), debug = True, caller_prefix = self._caller_prefix)
						sink_urls = self._collect_isd_sinks(new_values)
						Logger.spit("Collected %s ISD sinks for %s in %.2fs" % (sink_urls, str(self._edge_id), timer.get_time()), debug = True, caller_prefix = self._caller_prefix)
						self._store_stats("COLLECT_ISD_SINKS", {"time" : timer.end(), "num_of_sinks" : sink_urls})

					timer = Timer()
					response_body = self._make_DOM_changes(source, redirection_flow[-1]["html"] if redirection_flow and "html" in redirection_flow[-1] else "", req_url, new_values) # Make final DOM changes based on triggered events
					self._store_stats("MAKE_DOM_CHANGES", {"time" : timer.end(), "dom" : len(self._recorded_DOM_changes), "xhrs" : len(self._recorded_requests), "sinks" : len(self._recorded_sink_DOMs)})

					''' Submit discovered edges to graph worker
					'''
					if not relogin:
						timer = Timer()
						Logger.spit("Will submit new edges for %s" % self._current_url, debug = True, caller_prefix = self._caller_prefix)
						num_of_edges = self._submit_new_edges()
						self._store_stats("SUBMIT_EDGES", {"time" : timer.end(), "num_of_edges" : num_of_edges + num_of_events})
						Logger.spit("Submitted new edges for %s in %.2fs" % (self._current_url, timer.get_time()), debug = True, caller_prefix = self._caller_prefix)
					first_req = False

					if not redirection_flow or "response_headers" not in redirection_flow[-1]:
						Logger.spit("No redirection flow for: %s %s" % (str(self._edge_id), "" if not redirection_flow else "(partial)"), warning = True, caller_prefix = self._caller_prefix)
						Logger.spit("%s" % str(self._graph_flow), debug = True, caller_prefix = self._caller_prefix)
						Logger.spit("%s" % str(redirection_flow), debug = True, caller_prefix = self._caller_prefix)
						redirection_flow = [{"response_headers": {"content-type" : "text/html; charset=iso-8859-1"}, "status_code" : 200}] # Something went wrong in extracting the flow; return dummy 200 OK response

					
					response_headers = redirection_flow[-1]["response_headers"]
					if "html" not in response_headers.get("content-type", "") and req_method == "POST":
						Logger.spit("Non-HTML content type: %s. Will convert to text/html" % response_headers.get("content-type", ""), warning = True, caller_prefix = self._caller_prefix)
						response_headers["content-type"] = "text/html; charset=UTF-8"
					if "set-cookie" in response_headers: response_headers.pop("set-cookie")
					set_cookie_headers = self._cookies_to_set_cookies()
					if set_cookie_headers: response_headers["set-cookie"] = set_cookie_headers # if we have any cookies, add them as custom Set-Cookie headers

					res_object = (req_id, response_body, redirection_flow[-1]["status_code"], response_headers)
				except Exception as ex:
					Logger.spit(req_tuple, debug = True, caller_prefix = self._caller_prefix)
					print(self._edge_id)
					print(self._graph_flow)
					Logger.spit("ex in start: %s" % stringify_exception(ex), warning = True, caller_prefix = self._caller_prefix)
					raise ex
					res_object = (req_id, "", 204, {})

				self._store_stats("TOTAL", {"time" : total_timer.end()})

				if custom_request:
					custom_request = None
					Logger.spit("Served custom request...", caller_prefix = self._caller_prefix)
					continue
				if initial_request: Logger.spit("Served initial request...", caller_prefix = self._caller_prefix)
				initial_request = None
				self._check_injections()
				pdump(res_object, "/tmp/%s.pickle" % res_object[0]) # Send final response object to ReScan proxy so it can craft the response
				Logger.spit("Finished request handling for %s %s in %.2fs" % (req_method, req_url, total_timer.get_time()), debug = True, caller_prefix = self._caller_prefix)
				self._driver.clear_state() # Make sure we cleanup the references held by the XDriver instance, so we don't eventually flood the memory. Each request is independent from the next one, so it's safe to do this here
				self._served_requests += 1
				if self._served_requests >= Worker._reboot_interval:
					Logger.spit("Rebooting browser after %s requests" % self._served_requests, debug = True, caller_prefix = self._caller_prefix)
					self._served_requests = 0
					self._driver.reboot()
		except KeyboardInterrupt as ex:
			Logger.spit("Worker shutting down...", caller_prefix  = self._caller_prefix) # ^C by user to terminate the process
		except Exception as ex:
			Logger.spit(stringify_exception(ex), warning = True, caller_prefix = self._caller_prefix)
			print(stringify_exception(ex))
			with open("/tmp/ex_%s_%s.txt" % (self._caller_prefix, uuid()), "w") as wfp:
				wfp.write(traceback.format_exc())
				wfp.write("\n")
			raise
		finally:
			try: self._driver.quit() # Clean exit for the browser
			except Exception as ex: Logger.spit("Could not shutdown browser", warning = True, caller_prefix = self._caller_prefix)

	''' Convert in-browser cookies to equivalent Set-Cookie headers; useful to pass state back to the scanner
	'''
	def _cookies_to_set_cookies(self):
		Logger.spit("Constructing Set-Cookie headers", debug = True, caller_prefix = self._caller_prefix)
		set_cookie_headers = []
		cookie_jar = self._driver.get_cookies()
		cookies = {}
		for cookie in cookie_jar:
			cookie_name = cookie["name"]
			if cookie_name not in cookies:
				cookies[cookie_name] = cookie
				continue
			existing_cookie = cookies[cookie_name]
			if cookie["path"] > existing_cookie["path"] or cookie["domain"] > existing_cookie["domain"] or cookie.get("expiry", 0) > existing_cookie.get("expiry", 0): cookies[cookie_name] = cookie

		for cookie_name, cookie in cookies.items():
			set_cookie_headers.append("%s=%s%s%s%s%s%s" % (cookie["name"], cookie["value"], "; Max-Age=9999999", "; Domain=%s" % cookie["domain"], "; Path=%s" % cookie["path"], "; Secure" if cookie["secure"] else "", "; HttpOnly" if cookie["httpOnly"] else ""))
			Logger.spit("%s" % set_cookie_headers[-1], debug = True, caller_prefix = self._caller_prefix)
		return set_cookie_headers

	''' Convert in-browser cookies to equivalent request Cookie header; useful when retrying requests after a relogin
	'''
	def _cookies_to_string(self):
		cookie_string = ""
		cookie_jar = self._driver.get_cookies()
		for cookie in cookie_jar:
			cookie_string += "%s=%s; " % (cookie["name"], cookie["value"])
		return cookie_string

	''' Dynamically detect the proper authentication oracle to use
	'''
	def _detect_auth_oracle(self, login_url, landing_page):
		cookie_jar = [dict(cookie) for cookie in self._driver.get_cookies()] # Create new copy of cookie jar
		
		landing_page_source = self._driver.rendered_source()

		self._driver.delete_all_cookies() # purge cookie jar
		self._driver.get(landing_page) # Get landing page after login, without *any* cookies

		nocookies_landing_page_source = self._driver.rendered_source()

		# Logout keyword appears only in cookie-enabled request
		if re.search(Regexes.LOGOUT, landing_page_source) and not re.search(Regexes.LOGOUT, nocookies_landing_page_source):
			self._auth_oracle["check_url"] = landing_page
			self._auth_oracle["method"] = "logout_keyword"
			for cookie in cookie_jar: self._driver.add_cookie(cookie) # Restore cookie jar
			Logger.spit("Detected logout-based auth oracle (%s)" % landing_page, caller_prefix = self._caller_prefix)
			return True

		# If we have captured a username, it exists in the landing page after login and we can't find it in the same page when not sending cookies, we're done
		if "username" in self._credentials and re.search(re.escape(self._credentials["username"]), landing_page_source) and not re.search(re.escape(self._credentials["username"]), nocookies_landing_page_source):
			self._auth_oracle["check_url"] = landing_page
			self._auth_oracle["method"] = "username"
			for cookie in cookie_jar: self._driver.add_cookie(cookie) # Restore cookie jar
			Logger.spit("Detected username-based auth oracle (%s in %s)" % (self._credentials["username"], landing_page), caller_prefix = self._caller_prefix)
			return True

		for cookie in cookie_jar: self._driver.add_cookie(cookie) # Restore cookie jar
		
		self._driver.get(login_url)
		login_forms = self._driver.get_login_forms()
		if not login_forms: # No login form appears in the login URL, done
			self._auth_oracle["check_url"] = login_url
			self._auth_oracle["method"] = "login_form"
			Logger.spit("Detected form-based auth oracle (%s)" % login_url, caller_prefix = self._caller_prefix)
			return True

		# Did not detect oracle, No method worked. Resubmit login form just to be sure we didn't break the session
		self._driver.fill_and_submit(login_forms[0], override_rules = {Regexes.USERNAME : self._credentials["username"], Regexes.EMAIL : self._credentials["username"], Regexes.PASSWORD : self._credentials["password"]})
		self._auth_oracle["method"] = None
		Logger.spit("Could not detect auth oracle", warning = True, caller_prefix = self._caller_prefix)
		return False


	''' Based on the detected oracle, perform the necessary checks to see if our authenticated session is still valid
	'''
	def _oracle(self):
		if not self._auth_oracle.get("method"):
			Logger.spit("Auth oracle has not been defined", error = True, caller_prefix = self._caller_prefix)
			self._done.value = 1
			raise Exception("Auth oracle has not been defined")

		main_window_handle = self._driver.current_window_handle()

		self._driver.exec_script('''window.open("", "_blank");''') # Open new tab to run oracle in
		while len(self._driver.window_handles()) < 2: sleep(0.01) # Make sure the oracle tab opened in time

		oracle_tab = set(self._driver.window_handles()) - set([main_window_handle]) # Should just be one tab remaining
		if not oracle_tab:
			Logger.spit("Oracle tab blinked", warning = True, caller_prefix = self._caller_prefix)
			self._driver.switch_to_window(main_window_handle)
			return True

		oracle_tab = list(oracle_tab)[0]
		try: self._driver.switch_to_window(oracle_tab) # Switch to tab
		except Exception as ex:
			Logger.spit("Exception when switching to oracle tab: %s" % stringify_exception(ex), debug = True, caller_prefix = self._caller_prefix)
			return True

		self._driver.get(self._auth_oracle["check_url"])
		rendered_source = self._driver.rendered_source()

		method = self._auth_oracle["method"]
		
		session = False
		if method == "username": session = bool(re.search(re.escape(self._credentials["username"]), rendered_source))
		if method == "logout_keyword": session = bool(re.search(Regexes.LOGOUT, rendered_source))
		if method == "login_form": session = not bool(self._driver.get_login_forms())

		if not session: Logger.spit("Session has broken...", warning = True, caller_prefix = self._caller_prefix)

		if len(self._driver.window_handles()) > 1: self._driver.close() # Make sure if we close the tab it's not the last one
		else: main_window_handle = self._driver.window_handles()[0]
		try: self._driver.switch_to_window(main_window_handle)
		except Exception as ex: Logger.spit("Exception when switching to main window: %s" % stringify_exception(ex), debug = True, caller_prefix = self._caller_prefix)
		return session

	''' Attempt to relogin with previously detected credentials
	'''
	def _relogin(self):
		Logger.spit("Attempting relogin", warning = True, caller_prefix = self._caller_prefix)
		self._driver.delete_all_cookies() # purge cookie jar
		Logger.spit("Getting %s" % self._auth_oracle["login_url"], debug = True, caller_prefix = self._caller_prefix)
		self._driver.get(self._auth_oracle["login_url"])
		
		if not self._driver.find_element_by_css_selector(self._auth_oracle["login_form_css_selector"]):
			Logger.spit("Could not locate form for relogin", error = True, caller_prefix = self._caller_prefix)
			return False
		self._driver.create_and_fill_form_js(self._auth_oracle["login_form_css_selector"], self._auth_oracle["auth_req"][4], self._auth_oracle["auth_req"][2], self._auth_oracle["auth_req"][1], filenames = None, create = False)

		logged_in = self._oracle()
		if not logged_in:
			Logger.spit("Could not relogin", error = True, caller_prefix = self._caller_prefix)
			return False

		Logger.spit("Successful relogin!", caller_prefix = self._caller_prefix)
		return True


	''' Given a request tuple, check if it looks like an injection attempt based on common heuristics and if so, add it in the seen injection attempts
	'''
	def _is_injection(self, req_tuple):
		req_id, req_method, req_url, req_headers, req_data = req_tuple

		payload = URLUtils.get_query_as_list(req_url) if req_method.upper() == "GET" else [(key, value) for key, value in req_data.items() if key != "xdriver_filenames"]
		payload += [(key, value) for key, value in req_data.get("xdriver_filenames", {}).items()] if req_data else []

		injection_parameters = []
		for param_name, param_value in payload:
			if not re.search("alert|confirm|prompt|javascript", param_value, re.IGNORECASE): continue
			injection_parameters += [(param_name, param_value)] # We are only interested in the parameters that actually seem to contain a payload

		if not injection_parameters: return False
		self._injection_requests.append((req_method, req_url, tuple(sorted(injection_parameters, key = lambda el: el[0]))))
		return True

	''' Check whether the app tried to open an alert/prompt/confirm. If yes, retrieve its text and check it against the injection requests we have seen so far.
		If we detect a successful injection, store and report it. '''
	def _check_injections(self):
		alerts = self._driver.get_alerts()
		if not alerts: return

		try: set(alerts)
		except Exception as ex:
			print(stringify_exception(ex))
			print(alerts)
			raise

		current_url = None
		for alert_text in set(alerts): # Make the alert texts a set, since a payload might be triggered more than once, e.g. if it is rendered in two separate places in the page
			to_remove = set()
			successful_injections = []

			attributed = False
			for sinjection in self._successful_injections:
				if alert_text != sinjection["alert_text"]: continue
				attributed = True
				Logger.spit("Skipping attributed payload: %s %s (%s = %s) in %s" % (sinjection["method"], sinjection["url"], sinjection["parameter"], sinjection["payload"], self._driver.current_url()), debug = True, caller_prefix = self._caller_prefix)
				break
			if attributed: continue # This payload has already been used to attribute a previous injection. This can either occur due to a persistent xss OR if the scanner reuses payloads, e.g. zap alert(1)

			for injection in self._injection_requests:
				for param_name, param_value in injection[2]:
					if not current_url: current_url = self._driver.current_url()
					if not re.search(re.escape(alert_text), param_value, re.IGNORECASE): continue
					# Successful injection!
					# successful_injections.append((injection[0], injection[1], param_name, param_value, current_url))
					successful_injections.append((injection[0], injection[1], param_name, param_value, current_url, alert_text))
					to_remove.add((injection, alert_text)) # Remember to remove this injection, since we already attributed it to a TP (source)
					break # No need to check remaining params for this injection

			if not to_remove:
				Logger.spit("Present alerts could not be mapped to injections: %s (%s)" % (current_url if current_url else self._driver.current_url(), str(alerts)), warning = True, caller_prefix = self._caller_prefix)
				return # Did not find any alers or they were not mapped to an injection (possibly due to legitimate alert by the web app)
			unique = len(to_remove) == 1
			for injection in successful_injections:
				Logger.spit("XSS: %s %s (%s, %s) --> %s" % (injection[0], injection[1], injection[2], injection[3], injection[4]), error = unique, warning = not unique, caller_prefix = self._caller_prefix)
				self._successful_injections.append({"method" : injection[0], "url" : injection[1], "parameter" : injection[2], "payload" : injection[3], "sink" : injection[4], "alert_text" : injection[5]})

			for injection in to_remove:
				try: self._injection_requests.remove(injection[0])
				except Exception as ex: Logger.spit("Exception when removing successful injection: %s" % stringify_exception(ex), debug = True, caller_prefix = self._caller_prefix)


	def _entropy(self, value):
		return float(entropy(pd.Series(list(value)).value_counts()))

	''' Traverse all high-entropy values we just submitted, check if we locate them in any of the sinks correlated with the edge we executed (+ param name) and keep their DOMs
	'''
	def _collect_isd_sinks(self, values):
		fetched_sinks = 0
		sink_sources = {}
		for param_name, param_value in values:
			isd_src = (self._edge_id, param_name)
			if isd_src not in self._isd_links: continue
			sink_urls = set(self._isd_links[isd_src]) # Get all candidate sinks for this value
			for sink_url in sink_urls:
				if sink_url not in sink_sources:
					Logger.spit("Fetching candidate sink %s" % sink_url, debug = True, caller_prefix = self._caller_prefix)
					self._driver._proxy.redirection_mode(sink_url, html = True)
					self._driver.get(sink_url)
					flow = self._driver._proxy.get_redirection_flow()
					self._check_injections()
					fetched_sinks += 1
					sink_sources[sink_url] = {"source" : None, "prerendered_source" : None}
					sink_sources[sink_url]["source"] = self._driver.page_source() # Store sink URL in case it's needed for other params as well
					sink_sources[sink_url]["prerendered_source"] = flow[-1]["html"] if flow and "html" in flow[-1] else None

				page_source = sink_sources[sink_url]["source"]
				prerendered_source = sink_sources[sink_url]["prerendered_source"]

				value_regex = r".*%s[^>]*>{0,1}" % re.escape(param_value) # Match whole line that includes value
				splitted = page_source.split(param_value)
				if len(splitted) > 1:
					matches = []
					Logger.spit("Got submitted value in rendered sink (%s = %s > %s)" % (param_name, param_value, sink_url), debug = True, caller_prefix = self._caller_prefix)
					for tok in splitted:
						if matches:
							for idx in range(len(tok)):
								if tok[idx] == "\n":
									matches[-1] += tok[:idx]
									break
						if tok == splitted[-1]: continue # Ignore last token
						for idx in range(len(tok)-1, -1, -1):
							if tok[idx] == "\n": # Get line start
								matches.append("%s%s" % (tok[idx:], param_value))
								break

					html = ""
					for match in matches[:5]:
						if len(match) > 1024: continue # Avoid huge lines
						html += "%s\n" % fix_html(match, value = param_value)
					self._recorded_sink_DOMs.add((sink_url, html)) # Found it, store the DOM
		
		return fetched_sinks


	''' Trigger events in page. Follows BFS approach (i.e. events that are immediately displayed, then level-1 events etc.) and maintains dependency chains between events,
		e.g. triggering event A will also make event B available. '''
	def _get_new_events(self, prev_events, new_events, temp_revealed):
		return new_events - prev_events - temp_revealed
	def _get_deleted_events(self, prev_events, new_events):
		return prev_events - new_events
	def _check_consistent_events(self, prev_events, new_events, temp_revealed):
		return prev_events & (new_events - temp_revealed) == prev_events

	'''Recursively execute the dependency chain for a given event, e.g. trigger event A with the dependency chain C -> B -> A,
		If an intermmediary event is not displayed, its chain will be executed recursively. If an intermmediary event fails, all chain execution fails. '''
	def _execute_chain(self, chains, event, dompath, level):
		# Logger.spit("%sFollowing chain: %s" % ("\t"*level, chains[(event, dompath)]), debug = True, caller_prefix = self._caller_prefix)
		for (cevent, cdompath) in chains[(event, dompath)]:
			cel = self._driver.find_element_by_css_selector(cdompath)
			if not cel or not self._driver.is_displayed(cel): # If not found or not displayed, trigger that event's chain
				if not self._execute_chain(chains, cevent, cdompath, level+1) or self._check_redirection():
					return False
				if not cel or not self._driver.is_displayed(cel):
					Logger.spit("%sEvent failed: %s" % ("\t"*level, (cevent, cdompath)), debug = True, caller_prefix = self._caller_prefix)
					return False
			if not self._trigger_event(cel, cevent, css_selector = cdompath): return False
		return True

	# Try to trigger a specific event
	def _trigger_event(self, el, event, css_selector = None):
		try: self._driver.trigger_event(el, event, css_selector = css_selector)
		except Exception as e:
			print(stringify_exception(e))
			if "stale element" in stringify_exception(e).lower(): return False
			raise
		sleep(0.2)
		return True
	
	def _check_redirection(self):
		if self._driver.is_redirected(self._current_url, timeout = 0.05): # If redirected go back to URL under processing
			redirected_url = self._driver.current_url()
			Logger.spit("Event caused redirection %s > %s" % (self._current_url, redirected_url), debug = True, caller_prefix = self._caller_prefix)
			self._driver.get(self._current_url)
			self._driver.store_reference_element(self._current_url)
			return redirected_url
		return False

	def _count_events(self, event_chains, num, lvl = 0): # used for debug
		for event, dompath in event_chains:
			num += 1
			if event_chains[(event, dompath)]["events"]: num = self._count_events(event_chains[(event, dompath)]["events"], num, lvl = lvl + 1)
		return num
	
	def _trigger_events(self):
		_EVENTS_THRESHOLD = 100

		main_url = URLUtils.get_main_url(self._current_url)
		if main_url not in self._url_events: # Initialize URL's events
			self._url_events[main_url] = set()

		filter_events = set(["click", "onclick", "submit", "onsubmit", "input", "oninput", "onchange", "compositionstart"])
		displayed, not_displayed = self._driver.get_elements_with_events(filter_events = filter_events)
		
		base_events = set([(event, dompath) for el, event, dompath in displayed]) # Store level-0 events as a starting point of reference
		self._events_chains = {key : {"events" : {}, "xhrs" : [], "dom_changes" : []} for key in base_events} # Initialize event chains (level-0 events don't have any) -- only used for pretty printing

		temp_revealed = set() # Store newly seen events so we can properly exclude them from our checks
		chains = {}

		level = 0
		levels = {0 : base_events} # Store events for each level

		for key in base_events:
			if key not in chains: chains[key] = [] # Top-level events don't have any chains
		current_events = base_events
		level_threshold = 5 # Make sure we don't get ourselves in an infinite event loop

		while level < level_threshold:
			for event, dompath in current_events:
				if (event, dompath) in self._url_events[main_url]:
					Logger.spit("Skipping previous event: %s %s %s" % (event, dompath, self._current_url), debug = True, caller_prefix = self._caller_prefix)
					continue # We already triggered this precise event previously (same base URL, same event type, same (unique) CSS selector)

				num_of_events = self._count_events(self._events_chains, 0)
				if num_of_events >= _EVENTS_THRESHOLD:
					Logger.spit("Reached events threhold, stopping discovery", debug = True, caller_prefix = self._caller_prefix)
					return num_of_events # Make sure we don't spend all day triggering infinite events

				url_events = self._url_events[main_url]
				url_events.add((event, dompath))
				self._url_events[main_url] = url_events


				Logger.spit("Will trigger %s %s" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
				self._driver.block_xhr() # Make sure we block *all* XHRs leaving the browser so we don't trigger any state-changing actions
				self._driver.get_async_requests() # dummy op to reset xhr list in-browser

				el = self._driver.find_element_by_css_selector(dompath) # Fetch the element
				if not el or not self._driver.is_displayed(el): # If the element cannot be found OR is not displayed, we need to trigger its dependency chain
					Logger.spit("Not displayed or does not exist (%s %s)" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
					if not self._execute_chain(chains, event, dompath, 0):
						Logger.spit("Chain failed for %s %s" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
						continue # Could not find element (or make it displayed) and could not trigger its dependency chain -- skip
					Logger.spit("Executed chain for %s %s" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
					el = self._driver.find_element_by_css_selector(dompath)
					if not el or not self._driver.is_displayed(el):
						Logger.spit("Element still not displayed or does not exist %s %s . Skipping" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
						continue # We made it so far, but still no dice on the element

				# We got the element at this point; before triggering make sure it doesn't match any delete/spam/trash/logout/etc. regexes
				element_src = self._driver.get_element_src(el, full = False)
				if re.search(r"theme|plugin|addon|delete|remove|spam|trash|(log|sign)(_|-|\s)*(out|off)", element_src, re.IGNORECASE):
					Logger.spit("Skipping element: %s" % element_src.split("\n")[0][:50], debug = True, caller_prefix = self._caller_prefix)
					continue

				# Fill all input/select/textarea elements in the page with dummy values in case the event we're about to trigger requires a filled input to trigger its XHR
				self._driver.fill_all_inputs()
				self._driver.capture_mode() # Small chance of race condition, if another async request is fired right before we trigger the event
				if not self._trigger_event(el, event, css_selector = dompath):
					Logger.spit("Got event but trigger returned False: %s %s. Skipping" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
					continue
				redirect_http = self._driver.get_captured_http()
				redirect_request = None
				if self._check_redirection():
					if redirect_http:
						for http_msg in redirect_http:
							if http_msg.get("response").get("status_code") != 404:
								redirect_request = http_msg["request"]
								break
						if not redirect_request: redirect_request = redirect_http[0]["request"]
						Logger.spit("Event caused redirection: %s %s: %s" % (event, dompath, str(redirect_request)), debug = True, caller_prefix = self._caller_prefix)
						self._recorded_requests.add((redirect_request["method"], redirect_request["url"], URLUtils.dict_to_payload(redirect_request["payload"])))
					check_displayed, check_not_displayed = self._driver.get_elements_with_events(filter_events = filter_events)
					check_events = set([(cevent, cdompath) for cel, cevent, cdompath in check_displayed])
				
				event_object = self._events_chains
				if chains[(event, dompath)]:
					for (sevent, sdompath) in chains[(event, dompath)]: # Locate the exact event object (dict), to store XHRs and dom changes
						event_object = event_object[(sevent, sdompath)] if "events" not in event_object else event_object["events"][(sevent, sdompath)]
					event_object = event_object["events"][(event, dompath)]
				else: event_object = self._events_chains[(event, dompath)]

				if redirect_request:
					event_object["xhrs"].append(tuple([redirect_request["method"], redirect_request["url"], URLUtils.dict_to_payload(redirect_request["payload"])])) # Make sure to store edges for redirects
					continue # Event redirected us; no need to check for XHRs and DOM changes

				xhrs = self._driver.get_async_requests()
				for xhr in xhrs:
					Logger.spit("Event caused XHR: %s %s > %s %s data %s" % (event, dompath, xhr["method"], xhr["url"], str(xhr["parameters"])), debug = True, caller_prefix = self._caller_prefix)
					self._recorded_requests.add(tuple([xhr["method"], xhr["url"], URLUtils.urldecode(xhr["parameters"])]))
					event_object["xhrs"].append(tuple([xhr["method"], xhr["url"], URLUtils.urldecode(xhr["parameters"])]))

				mutations = None
				try: mutations = self._driver.get_mutations()
				except Exception as ex:
					Logger.spit("Exception when getting mutations: %s" % stringify_exception(ex), debug = True, caller_prefix = self._caller_prefix)
					mutations = {"dom_mutations" : []}
				dom_mutations = mutations["dom_mutations"]
				for new_dom_element in dom_mutations:
					Logger.spit("Event added DOM: %s %s > %s" % (event, dompath, new_dom_element["str"]), debug = True, caller_prefix = self._caller_prefix)
					self._recorded_DOM_changes.add(new_dom_element["str"])
					dom_object = BeautifulSoup(new_dom_element["str"], "html5lib")
					for el in dom_object.find_all("a"):
						if el.get("href"):
							event_object["dom_changes"].append((new_dom_element["str"], ("a", el.get("href"))))
					for el in dom_object.find_all("form"):
						names = []
						names += [field.get("name", field.get("id")) for field in el.find_all("input")]
						names += [field.get("name", field.get("id")) for field in el.find_all("textarea")]
						names += [field.get("name", field.get("id")) for field in el.find_all("select")]
						names = set(names)
						if None in names: names.remove(None)
						event_object["dom_changes"].append((new_dom_element["str"], ("form", el.get("action", self._current_url), "&".join(names), el.get("method", "GET"), new_dom_element["xpath"])))
					for el in dom_object.find_all("iframe"):
						if el.get("src"):
							event_object["dom_changes"].append((new_dom_element["str"], ("iframe", el.get("src"), new_dom_element["xpath"])))

				check_displayed, check_not_displayed = self._driver.get_elements_with_events(filter_events = filter_events)
				check_events = set([(cevent, cdompath) for cel, cevent, cdompath in check_displayed])
				new_events = self._get_new_events(base_events, check_events, temp_revealed)
				deleted_events = self._get_deleted_events(base_events, check_events)

				if deleted_events: # Case 1: Previoous events are now hidden/deleted. We need to trigger the event again in hope we make them reappear
					Logger.spit("Event made other base events diasspear %s %s. Re-triggering" % (event, dompath), debug = True, caller_prefix = self._caller_prefix)
					self._trigger_event(el, event, css_selector = dompath) # Also need to store that this specific event hides elements

				if new_events: # Case 2: We discovered new elements with events (either hidden and are now visible or dynamically generated). We need to model the chain of events
					Logger.spit("Event made new events appear %s %s > %s" % (event, dompath, len(new_events)), debug = True, caller_prefix = self._caller_prefix)
					temp_revealed = temp_revealed | new_events
					for nevent, ndompath in new_events:
						if level+1 not in levels: levels[level+1] = set()
						levels[level+1].add((nevent, ndompath)) # Keep as next level event
						chains[(nevent, ndompath)] = chains[(event, dompath)] + [(event, dompath)] # Construct event's chain, i.e. parent's chain + parent
						ec = self._events_chains
						for step in chains[(nevent, ndompath)]:
							ec = ec[step] if "events" not in ec else ec["events"][step]
						ec["events"][(nevent, ndompath)] = {"events" : {}, "xhrs" : [], "dom_changes" : []}

				if xhrs:
					self._driver.refresh() # Clean state in page -- e.g. some XHRs (which are blocked here), might block all other events in the page if not completed
					self._driver.store_reference_element(self._current_url)

			level = level+1
			Logger.spit("Moving on to next event level: %s" % level, debug = True, caller_prefix = self._caller_prefix)
			if level not in levels:
				Logger.spit("No more events. Stopping", debug = True, caller_prefix = self._caller_prefix)
				break # No next level events were found, so stop
			elif level >= level_threshold: Logger.spit("Reached event level threhold. Stopping", debug = True, caller_prefix = self._caller_prefix)
			current_events = levels[level]

		return self._count_events(self._events_chains, 0)


	''' Given the page source as a string, append the recorded DOM changes in the HTML body as well as the async requests and ISD sinks
	'''
	def _make_DOM_changes(self, source, prerendered_source, req_url, new_values):
		''' Add relevant DOM objects from event triggering
		'''
		soup = BeautifulSoup(source, "html5lib")
		if soup.body:
			for de in self._recorded_DOM_changes:
				dom_object = BeautifulSoup(de, "html5lib") # Directly add the new element's stringified form
				soup.body.append(dom_object)

		if self._redirected_edge: # Make sure to add the redirected edge as a static URL -- needed for cases where the landing page e.g. after a form submission, is not accessible otherwise
			anchor = soup.new_tag("a", href = self._redirected_edge.get_dst_url())
			if soup.body:
				soup.body.append(anchor)

		# Collect all iframe DOMs so we can pass all embedded info back to the scanner
		for dom in self._iframe_doms:
			dom_soup = BeautifulSoup(dom, "html5lib")
			for body_element in dom_soup.body.findChildren(recursive = False): soup.body.append(body_element)

		''' Fill *ALL* inputs in page, so the scanner will not submit its own default values (e.g. wapiti first submits the value "default" for inputs that are empty, ZAP gives "ZAP" etc.)
			This produces a lot of candidate ISD sinks. Instead, if we give our own unique, pre-filled values, the scanner will submit those instead and we will be able to precisely detect ISD
			(not candidate) sinks. This needs to be done before transcribing XHRs etc. '''
		hidden_styles = ["visibility:hidden", "display:none"]
		prefilled_inputs = set()
		for element in soup.find_all("input") + soup.find_all("textarea"):
			if element.get("value") or element.get("style", "").replace(" ", "") in hidden_styles or element.get("type") in ["hidden", "submit", "reset", "image"] : continue # Skip pre-filled or hidden elements
			element_name = element.get("name")
			if not element_name: continue

			if element_name in self._unique_tokens: # Use same token each time for this parameter, otherwise the scanner will learn multiple values and will possibly try to use all of them during fuzzing
				value = self._unique_tokens[element_name]
				Logger.spit("Got form value: %s = %s" % (element_name, value), debug = True, caller_prefix = self._caller_prefix)
			else:
				maxlength = int(element.get("maxLength", 8))
				value_length = maxlength if maxlength != -1 else 8
				value =  generate_token(value_length) # Generate unique token/value for this element
				self._unique_tokens[element_name] = value
				Logger.spit("Set form value: %s = %s" % (element_name, value), debug = True, caller_prefix = self._caller_prefix)
			
			prefilled_inputs.add(element_name)
			element["value"] = value

		''' Transcribe async requests trigerred by events, to static HTML elements
		'''
		save = False
		for req in self._recorded_requests:
			method, url, data = req
			if data or method != "GET": # If we have a payload OR any method other than GET, we need to transcribe it as a form
				form = soup.new_tag("form", action = url, method = method)
				found_submit = False
				if data:
					for param in data.split("&"):
						finput = soup.new_tag("input", type = "text") if not re.search(r"submit", param, re.IGNORECASE) else soup.new_tag("input", type = "submit")
						if "=" in param:
							finput["name"] = param.split("=")[0] # Extract the param name
							finput["value"] = param.split("=")[1] # Extract the param value
						else:
							finput["name"] = param
							finput["value"] = ""

						form.append(finput)
						if finput["type"] == "submit": found_submit = True
				if not found_submit:
					fsubmit = soup.new_tag("input", type = "submit")
					form.append(fsubmit) # Custom submit button so we can submit the form when injected
				soup.body.append(form)
				save = True
				Logger.spit("Added form for XHR %s %s, data: %s" % (method, url, str(data)), debug = True, caller_prefix = self._caller_prefix)
			else:
				anchor = soup.new_tag("a", href = url) # Simple GET requests (without payloads) can be transcribed with <a>s
				soup.body.append(anchor)
				Logger.spit("Added static anchor for XHR %s" % url, debug = True, caller_prefix = self._caller_prefix)

		for element in soup.find_all("textarea"):
			element.name = "input" # Hack for w3af which for some reason ignores textareas :)
			Logger.spit("Converted textarea to input", debug = True, caller_prefix = self._caller_prefix)

		input_types = ["radio", "checkbox", "color", "hidden"]
		for itype in input_types:
			for element in soup.find_all("input", {"type" : itype}): element["type"] = "text"

		current_url = self._driver.current_url()
		current_scheme = URLUtils.get_scheme(current_url)
		current_url = current_url.replace("%s://" % current_scheme, "")
		current_url_parts = current_url.split("/")[:-1]
		if current_url_parts: # Might be empty from the splitting above, e.g. for http://example.com
			for element in [a for a in soup.find_all("a")] + [f for f in soup.find_all("form")] + [fr for fr in soup.find_all("iframe")]: # w3af (and possibly other scanners), cannot properly handle relative URLs and are lead to discover false ones
				href = element.get("href") if element.name == "a" else element.get("action") if element.name == "form" else element.get("src")
				if not href or not href.startswith("../"): continue # We are only interested in relative URLs
				current_url_parts = current_url.split("/")[:-1] # Need to split main URL again
				for backstep in range(href.count("../")): current_url_parts.pop() # For each directory backstep, remove last dir from current URL
				fixed_absolute_url = "%s://%s/%s" % (current_scheme, "/".join(current_url_parts), href.split("../")[-1]) # Reconstruct w/ scheme, filtered path and the original href's last part
				if element.name == "a": element["href"] = fixed_absolute_url
				elif element.name == "form": element["action"] = fixed_absolute_url
				else: element["src"] = fixed_absolute_url
				Logger.spit("Fixed relative URL: %s > %s" % (href, fixed_absolute_url), debug = True, caller_prefix = self._caller_prefix)
		
		str_soup = str(soup)

		sink_doms = ""
		for sink_url, html in self._recorded_sink_DOMs:
			sink_doms += "%s\n" % html
		if sink_doms:
			soup_end_body_idx = str_soup.index("</body") # Where we need to prepend the ISD sink DOMs
			str_soup = str_soup[:soup_end_body_idx] + sink_doms + str_soup[soup_end_body_idx:]
			Logger.spit("Added DOM sinks: %s\n" % sink_doms, debug = True, caller_prefix = self._caller_prefix)

		if not prerendered_source: return str_soup

		for table_el in soup.find_all("table"):
			if not table_el.find("form"): continue
			return_prerendered = True
			Logger.spit("Response contains non-compliant HTML form inside table. Will return pre-rendered source.", debug = True, caller_prefix = self._caller_prefix)
			return prerendered_source

		if not new_values: return str_soup

		for name, value in new_values:
			if not any([char in value for char in ["\"", "'", "`"]]): continue # No need to check the pre-rendered source for values with no escaping sequences; they should appear in the rendered source as-is
			value_regex = r".*%s[^>]*>{0,1}" % re.escape(value) # Match whole line that includes value
			splitted = prerendered_source.split(value)

			if len(splitted) > 1:
				matches = []
				for tok in splitted:
					if matches: # Already had a tok
						for idx in range(len(tok)):
							if tok[idx] == "\n":
								matches[-1] += tok[:idx]
								break

					if tok == splitted[-1]: continue # Ignore last token
					for idx in range(len(tok)-1, -1, -1):
						if tok[idx] == "\n": # Get line start
							matches.append("%s%s" % (tok[idx:], value))
							break

				html = ""
				for match in matches[:5]: # Consider only first 5 matches in the page.
					if len(match) > 1024: continue # Avoid huge lines
					html += "%s\n" % fix_html(match, value = value)

				Logger.spit("Added pre-rendered value: %s\n" % html, debug = True, caller_prefix = self._caller_prefix)
				soup_end_body_idx = str_soup.index("</body") # Where we need to prepend the ISD sink DOMs
				str_soup = str_soup[:soup_end_body_idx] + html + str_soup[soup_end_body_idx:]
		
		return str_soup