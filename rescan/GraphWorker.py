#!/usr/bin/python3

from xdriver.xutils.Logger import Logger
Logger.set_debug_on()
from xdriver.xutils.Exceptions import stringify_exception
from xdriver.xutils.Misc import pload, pdump, uuid
from xdriver.rescan.Graph import Graph, Node, Edge, EdgeTypes

import sys
from os.path import exists
from time import time
from datetime import datetime
import traceback

# Concurrency imports #
from queue import Empty
from threading import Thread

class GraphWorker(object):
	_caller_prefix = "GraphWorker"

	_reporting_interval = 300 # Report every 5 minutes
	
	def __init__(self, graph_file, target, available_workers, done, *args):
		self._graph_file = graph_file if graph_file else "/tmp/graph_%s" % uuid() # Generate new graph file if none is given
		Logger.spit("Set graph file to: %s" % self._graph_file, caller_prefix = GraphWorker._caller_prefix)

		self._target = target

		self._edges_pipes = args[:len(args)//2] # Pipes where workers will submit newly discovered edges; one slot per worker
		self._flows_pipes = args[len(args)//2:] # Pipes where workers will request flows and the graph worker will respond; one slot per worker

		self._done = done
		self._first_req = False

		self._available_workers = available_workers
		self._available_workers.value += 1

		self._workflow_thread = Thread(target = self._serve_flows, args = (), daemon = True)

		if exists(self._graph_file):
			Logger.spit("Loading existing graph..", caller_prefix = GraphWorker._caller_prefix)
			self._graph = pload(self._graph_file) # Load an existing graph if it exists
		else: self._graph = Graph(self._target)
		Logger.spit("Ready", caller_prefix = GraphWorker._caller_prefix)


	def _shutdown(self):
		Logger.spit("Dumping graph in %s" % self._graph_file, caller_prefix = GraphWorker._caller_prefix)
		pdump(self._graph, self._graph_file) # Pickle and save learned graph

	def start(self):
		self._workflow_thread.start()
		report_time = time()
		try:
			while not self._done.value: # Work until the main process says we're done
				if time() - report_time >= GraphWorker._reporting_interval:
					report_time = time()
					Logger.spit("[%s] Alive" % datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), caller_prefix = GraphWorker._caller_prefix)
				i = 0
				for pipe in self._edges_pipes:
					try: data = pipe.get_nowait()
					except Empty as ex: continue
					src_url, dst_url, parent_edge_id, edge_type, edge_payload, edge_info = data
					self._graph.add_edge(src_url, dst_url, parent_edge_id, edge_type, payload = edge_payload, info = edge_info)
		except KeyboardInterrupt as ex:
			Logger.spit("Graph worker shutting down...", caller_prefix  = GraphWorker._caller_prefix) # ^C by user to terminate the process
		except Exception as ex:
			Logger.spit(stringify_exception(ex), warning = True, caller_prefix  = GraphWorker._caller_prefix)
			with open("/tmp/ex_%s_%s.txt" % (GraphWorker._caller_prefix, uuid()), "w") as wfp:
				wfp.write(traceback.format_exc())
				wfp.write("\n")
			raise
		finally:
			self._shutdown()

	def _serve_flows(parent_thread):
		_caller_prefix = "%s[FLOW]" % GraphWorker._caller_prefix
		report_time = time()
		try:
			while not parent_thread._done.value:
				if time() - report_time >= GraphWorker._reporting_interval:
					report_time = time()
					Logger.spit("[%s] Alive" % datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), caller_prefix = _caller_prefix)
				for pipe in parent_thread._flows_pipes:
					if not pipe.poll(): continue
					edge_id = pipe.recv() # Got a request for a given edge
					if edge_id[0] == "GRAPH": # Request for whole graph (API call)
						pipe.send([edge for edge in map(str, parent_thread._graph._edges)])
						continue
					flow = []
					start = time()
					while not flow and time() < start + 1:
						try: flow = parent_thread._graph.get_workflow(edge_id) if edge_id[1] != "*" else parent_thread._graph.get_all_edges(edge_id[0]) # Get reconstructed path/flow from graph OR all edges that match the given type
						except Exception as ex:
							print("GRAPH EX: %s" % stringify_exception(ex))
							continue
					recorded_requests, recorded_DOM_changes = set(), set()
					if not parent_thread._first_req:
						parent_thread._first_req = True # Only get all dynamic info once
						recorded_requests, recorded_DOM_changes = parent_thread._graph.get_all_dynamic_info()
					if not flow: Logger.spit("No closest matching edge for %s" % str(edge_id), debug = True, caller_prefix = _caller_prefix)
					pipe.send((flow, recorded_requests, recorded_DOM_changes)) # And send it back
					# print("GRAPH SERVE FLOW (%s): %.2f" % (len(flow), time() - start))
		except KeyboardInterrupt as ex:
			Logger.spit("Graph worker shutting down...", caller_prefix = _caller_prefix) # ^C by user to terminate the process
		except Exception as ex:
			Logger.spit(stringify_exception(ex), warning = True, caller_prefix = _caller_prefix)
			with open("/tmp/ex_%s_%s.txt" % (_caller_prefix, uuid()), "w") as wfp:
				wfp.write(traceback.format_exc())
				wfp.write("\n")
			raise