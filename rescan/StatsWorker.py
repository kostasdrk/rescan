#!/usr/bin/python3

from xdriver.xutils.Logger import Logger
Logger.set_debug_on()
from xdriver.xutils.Exceptions import stringify_exception
from xdriver.xutils.Misc import pload, pdump, uuid

from time import time
from datetime import datetime
import traceback

# Concurrency imports #
from queue import Empty

class StatsWorker(object):
	_caller_prefix = "StatsWorker"

	_reporting_interval = 300 # Report every 5 minutes

	def __init__(self, stats_queue, available_workers, done):
		self._stats_queue = stats_queue
		self._stats_file = "/tmp/stats_%s.pickle" % uuid()
		Logger.spit("Set stats file to: %s" % self._stats_file, caller_prefix = StatsWorker._caller_prefix)
		self._available_workers = available_workers
		self._available_workers.value += 1
		self._done = done
		self._stats = {} # Store stats where the key is the info, e.g. "EXECUTE_FLOW", and the value is a list of stats dicts, e.g. [{"time" : 22}]
		Logger.spit("Ready", caller_prefix = StatsWorker._caller_prefix)

	def _get_stats_object(self):
		try: return self._stats_queue.get_nowait()
		except Empty: return None

	def start(self):
		report_time = time()
		try:
			while not self._done.value:
				if time() - report_time >= StatsWorker._reporting_interval:
					report_time = time()
					Logger.spit("[%s] Alive" % datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), caller_prefix = StatsWorker._caller_prefix)
				stats_object = self._get_stats_object()
				if not stats_object: continue

				info, stats = stats_object
				if info not in self._stats: self._stats[info] = []
				self._stats[info].append(stats)
		except KeyboardInterrupt as ex:
			Logger.spit("Stats worker shutting down...", caller_prefix  = StatsWorker._caller_prefix) # ^C by user to terminate the process
		except Exception as ex:
			Logger.spit(stringify_exception(ex), warning = True, caller_prefix  = StatsWorker._caller_prefix)
			with open("/tmp/ex_%s_%s.txt" % (StatsWorker._caller_prefix, uuid()), "w") as wfp:
				wfp.write(traceback.format_exc())
				wfp.write("\n")
			raise
		finally:
			Logger.spit("Dumping stats in %s" % self._stats_file, caller_prefix = StatsWorker._caller_prefix)
			pdump(self._stats, self._stats_file)
