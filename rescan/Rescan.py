#!/usr/bin/python3

from xdriver.XDriver import XDriver
from xdriver.xutils.proxy.ProxyWrapper import ProxyWrapper
from xdriver.xutils.Logger import Logger
from xdriver.xutils.Regexes import Regexes
Logger.set_debug_on()
from xdriver.xutils.Exceptions import stringify_exception
from xdriver.xutils.Misc import uuid, pdump
from xdriver.xutils.URLUtils import URLUtils
from xdriver.rescan.Worker import Worker
from xdriver.rescan.GraphWorker import GraphWorker
from xdriver.rescan.BackgroundWorker import BackgroundWorker
from xdriver.rescan.StatsWorker import StatsWorker
from xdriver.xutils.ModuleTimer import Timer

import os, sys
import pickle as cp
from time import sleep
from time import time
from datetime import datetime
import re
import traceback
import resource
from urllib.parse import urlparse,parse_qs
from json import dumps as JSONdumps

# Concurrency imports #
from multiprocessing import Process, Value, Queue, Pipe, Manager, Lock
from queue import Empty
from ctypes import c_wchar_p
import signal


_abs_path = os.path.dirname(os.path.abspath(__file__))

class ReScan(object):
	_caller_prefix = "ReScan"

	_mitm_script = os.path.join(_abs_path, "Intercept.py")

	_reporting_interval = 300 # Report every X seonds
	@classmethod
	def banner(cls):
		lines = [" ####################################################### ",
				 " #                                                     # ",
				 " #  ██████  ███████ ███████  ██████  █████  ███    ██  # ",
				 " #  ██   ██ ██      ██      ██      ██   ██ ████   ██  # ",
				 " #  ██████  █████   ███████ ██      ███████ ██ ██  ██  # ",
				 " #  ██   ██ ██           ██ ██      ██   ██ ██  ██ ██  # ",
				 " #  ██   ██ ███████ ███████  ██████ ██   ██ ██   ████  # ",
				 " #                                                     # ",
				 " ####################################################### "]

		print("\033[92m")
		for line in lines:
			print(line)
			sleep(0.05)
		print("\033[0m")

	def __init__(self, proxy_port = None, workers = 2, events = False, isd = False, clustering = False, auth = False, authLax = False, graph_file = None, headless = True, xproxy_port = None):
		ReScan.banner()
		resource.setrlimit(resource.RLIMIT_NOFILE, (50000, 50000)) # Increase the limit for open files for ReScan and its proxy subprocess

		Logger.set_debug_off()
		
		self._manager = Manager()

		ProxyWrapper.rescan_mode()
		self._proxy = ProxyWrapper(port = proxy_port, mitm_script = ReScan._mitm_script)
		self._proxy_port = proxy_port
		self._api_url = "http://127.0.0.1:%s/" % self._proxy_port

		self._target = self._manager.Value(c_wchar_p, "")

		self._clustering = clustering
		# self._isd = isd
		self._isd = Value('i', isd)

		self._preserved_requests = 25
		self._chunk_requests = set()
		self._seen_requests = 0 # Count # of seen requests. Useful for more efficient queueing
		self._request_queue = self._manager.Queue() # Thread-safe queue for incoming requests; orchestrator (this module) will enQ, browser workers will deQ 

		self._shared_cookies = self._manager.Value(c_wchar_p, "") # Shared (auth) cookie string. Will be updated by browser workers after verifying auth state (if auth helper is enabled)
		
		self._isd_links = self._manager.dict() if isd else None # key: source edge id, value: <list> sink URLs (i.e. nodes)
		self._submitted_posts = self._manager.Queue() # Browser workers will submit any POST requests they executed -- BG worker will dequeue and detect ISD links
		self._unique_tokens = self._manager.dict() # Key: parameter name, value: unique token we generated and the scanner learned for this value

		self._known_headers = self._manager.dict() # Store the default header values sent by the scanner

		self._events = Value('i', events)
		self._url_events = self._manager.dict() if events else None # key: main URL (without query and fragments), value : <set> of <tuple> with precise event info, i.e. (event, css_selector)

		self._credentials = self._manager.dict()
		self._auth = Value("i", auth)
		self._auth_oracle = self._manager.dict() if auth else None
		self._authLax = authLax

		self._injection_requests = self._manager.list() # Store requests that might be injection attempts. Item structure is tuple(str(method), str(URL), list([tuple("param_name", "param_value"), ...]))
		self._successful_injections = self._manager.list() # Store (most likely) TP injections. key: tuple(method, URL, param_name, param_value), value: {"sink" : <url that injection was trigerred>, "confidence" : <bool>}
		self._successful_injections_file = "/tmp/rescan_xss_%s.pickle" % uuid()
		Logger.spit("Will save successful XSS in %s" % self._successful_injections_file, caller_prefix = ReScan._caller_prefix)

		self._cached_responses = self._manager.dict() if clustering else None
		self._cache_rules = self._manager.dict() if clustering else None # Whenever a request towrads a possibly similar URL comes through, check it against the redirection rules, as set by the BG worker
		self._ndd_requests_queue = self._manager.Queue() if clustering else None # If no rules match, enqueue it, so the BG worker can figure out what to do
		self._all_known_values = self._manager.dict() if clustering else None # Store all known parameter values for each base URL
		self._known_parameters = self._manager.dict() if clustering else None # Store all parameter values passed back to the scanner
		self._pending_similar = [] # Hold pending requests from the BG worker, so we can unblock and serve subsequent requests in the meantime_preserved_requests

		self._available_workers = Value('i', 0)
		self._done = Value('i', 0)

		self._workers = []
		self._request_pipes = []
		self._submit_pipes = [] # Used for IPC between workers and the graph worker
		self._stats_queue = self._manager.Queue() # Used for IPC between workers and stats_worker subprocess
		Logger.spit("Launching browser workers...", caller_prefix = ReScan._caller_prefix)
		for i in range(workers):
			self._request_pipes.append(Pipe())
			self._submit_pipes.append(self._manager.Queue())
			self._workers.append(Process(target = _boot_worker, args = (i, self._target, self._request_queue, self._available_workers, self._submit_pipes[-1], self._request_pipes[-1][1], self._known_headers, self._isd, self._isd_links, self._credentials, self._auth, self._auth_oracle, self._authLax, self._injection_requests, self._successful_injections, self._done, self._events, self._url_events, self._cached_responses, self._submitted_posts, self._shared_cookies, self._unique_tokens, headless, self._stats_queue, self._api_url, xproxy_port))) # Register worker
			self._workers[-1].daemon = True
			self._workers[-1].start() # Start worker subprocess

		if clustering or isd: # Laucnh background worker only if URL clustering or inter-state dependency detection are enabled
			Logger.spit("Launching background worker...", caller_prefix = ReScan._caller_prefix)
			self._request_pipes.append(Pipe())
			self._submit_pipes.append(self._manager.Queue())
			self._bg_worker = Process(target = _boot_bg_worker, args = (self._target, clustering, self._isd, self._available_workers, self._done, self._cached_responses, self._cache_rules, self._ndd_requests_queue, self._all_known_values, self._known_parameters, self._isd_links, self._submitted_posts, self._shared_cookies, self._submit_pipes[-1], self._request_pipes[-1][1], headless))
			self._bg_worker.start()

		Logger.spit("Launching graph worker...", caller_prefix = ReScan._caller_prefix)
		self._graph_worker = Process(target = _boot_graph_worker, args = (graph_file, self._target, self._available_workers, self._done, *[sp for sp in self._submit_pipes], *[rp[0] for rp in self._request_pipes]))
		self._graph_worker.start()


		Logger.spit("Launching stats worker...", caller_prefix = ReScan._caller_prefix)
		self._stats_worker = Process(target = _boot_stats_worker, args = (self._stats_queue, self._available_workers, self._done))
		self._stats_worker.start()
		
		extra_workers = 3 if self._clustering else 2
		while self._available_workers.value != workers + extra_workers: continue # Wait for all browser workers + graph worker + BG worker + stats worker to boot

	def _load_requests(self):
		while True:
			try:
				config = self._proxy.load_config()
				new_requests = len(config["rescan_mode"]["requests"])
				for req_object in config["rescan_mode"]["requests"]:
					req_id, req_method, req_url, req_headers, req_data = req_object # Make sure the object can be de-pickled safely
					if not self._target.value: # Identify the target domain in the first request
						self._target.value = URLUtils.get_main_domain(req_url, suffix = True, port = True)
						Logger.spit("Setting target to %s" % self._target.value, caller_prefix = ReScan._caller_prefix)
					if not self._known_headers: # Detect the scanner's default header values in the first request
						for header, value in req_headers.items():
							Logger.spit("Setting default scanner header: %s = %s" % (header, value), caller_prefix = self._caller_prefix)
							self._known_headers[header] = value # Need to store them one by one, since this is a multiprocessing dict object
				break
			except Exception as ex: continue # Make sure we don't load an incomplete/malformed pickle

		if new_requests < self._seen_requests: # Requests' list was reset by proxy
			first_new = True
			for req_object in config["rescan_mode"]["requests"]: # Need to traverse whole list since it was reset
				if req_object[0] in self._chunk_requests: continue # Already processed or pending similar request; necessary checking
				if first_new: # If this is the first new request after the reset
					self._chunk_requests = set() # Reset recorded requests for this chunk
					first_new = False # Make sure the recorded chunk is not reset by the remaining requests

				is_api_call = self._is_api_call(req_object[2])
				if is_api_call:
					if self._handle_api_call(req_object): continue # If the Orchestrator can handle the API call, move on (i.e. don't pass on to the workers)

				if not is_api_call:
					is_cached, res_object = self._is_cached(req_object)
					if is_cached is None: # No rules applied to this request; feed to BG worker and add to pending
						self._ndd_requests_queue.put(req_object)
						self._pending_similar.append(req_object)
					elif is_cached: self._craft_redirect(req_object, res_object) # Found applying rule -> redirect to base URL
					else: self._request_queue.put(req_object) # Original base URL or arbitrary to base or non-similar URL
				else: self._request_queue.put(req_object) # Directly pass API calls to the worker (if not already handled)

				self._chunk_requests.add(req_object[0])
		elif new_requests > self._seen_requests:
			for req_object in config["rescan_mode"]["requests"][self._seen_requests:]: # Start enQing right after the last seen request, so we don't need to traverse the whole list and check req IDs
				is_api_call = self._is_api_call(req_object[2])
				if is_api_call:
					if self._handle_api_call(req_object): continue # If the Orchestrator can handle the API call, move on (i.e. don't pass on to the workers)

				if not is_api_call:
					is_cached, res_object = self._is_cached(req_object)
					if is_cached is None: # No rules applied to this request; feed to BG worker and add to pending
						self._ndd_requests_queue.put(req_object)
						self._pending_similar.append(req_object)
					elif is_cached: self._craft_redirect(req_object, res_object) # Found applying rule -> redirect to base URL
					else: self._request_queue.put(req_object) # Original base URL or arbitrary to base or non-similar URL
				else: self._request_queue.put(req_object) # Directly pass API calls to the worker (if not already handled)
				
				self._chunk_requests.add(req_object[0]) # Store seen request IDs; used when requests' list is reset by mitmproxy
		self._seen_requests = new_requests # In any case, store requests' list length

		pending = len(self._pending_similar)
		handled = []
		for req_object in self._pending_similar:
			is_cached, res_object = self._is_cached(req_object, debug = False)
			if is_cached is None: continue # No decision yet
			else:
				if is_cached: self._craft_redirect(req_object, res_object) # Redirect to base
				else: self._request_queue.put(req_object) # Pass through ReScan
				handled.append(req_object)

		for req_object in handled: self._pending_similar.remove(req_object) # Remove handled requests from pending

		new_pending = len(self._pending_similar)
		if new_pending != pending: Logger.spit("... pending %s ..." % new_pending, caller_prefix = ReScan._caller_prefix)

	def _craft_redirect(self, req_object, res_object):
		res_object["headers"].update({"Location" : res_object["url"]})
		res_object = (req_object[0], "", 301, res_object["headers"])
		pdump(res_object, "/tmp/%s.pickle" % res_object[0]) # Send dummy redirect response object to ReScan proxy so it can craft the response towards the scanner

	''' Check if given URL must be redirected to another URL. Returns `False` if the requests has to go through ReScan,
		`None` if the Orchestrator must wait for the BG worker's decision/rule and the redirection URL if it must be redirected. '''
	def _is_cached(self, req_object, debug = True):
		if not self._clustering: return (False, None) # Clustering is disabled; pass everything through the system
		req_id, req_method, req_url, req_headers, req_data = req_object
		if req_method != "GET": return (False, None) # Pass through ReScan anything that isn't a GET
		
		req_url = URLUtils.normalize_url(req_url)
		base_url = URLUtils.get_main_url(req_url)

		parameter_set = tuple(set(URLUtils.get_query_parameters(req_url)))
		if not parameter_set: return (False, None) # No query params

		if re.search(Regexes.NONCE, URLUtils.get_query(req_url), re.IGNORECASE):
			Logger.spit("Passing through NONCE: %s" % req_url, caller_prefix = ReScan._caller_prefix)
			return (False, None) # Pass through ReScan if the URL contains a nonce-like parameter
		
		is_arbitrary, arbitrary_param = self._is_arbitrary(req_url)
		if is_arbitrary:
			parameters = URLUtils.get_query_as_list(req_url)
			swapped_query = ""
			for param, value in parameters:
				try:
					if param == arbitrary_param or value in self._known_parameters[base_url][param]: # Ignore arbitrary parameter or with known base values
						swapped_query += "%s=%s&" % (param, value)
						continue
				except Exception as ex:
					print(req_url)
					raise
				swapped_query += "%s=%s&" % (param, list(self._known_parameters[base_url][param])[0]) # Swap value with known parameter base value to be used for redirect

			redirect_url = URLUtils.normalize_url("%s?%s" % (base_url, swapped_query[:-1]))
			if redirect_url == req_url: # True injection towards base param URL (e.g. cID=8&page=<script>...) OR fuzzing the param(e.g. cID=<script>....)
				Logger.spit("Passing through ARBITRARY: %s" % req_url, caller_prefix = ReScan._caller_prefix)
				return (False, None)
			Logger.spit("Redirecting ARBITRARY: %s -> %s" % (req_url, redirect_url), caller_prefix = ReScan._caller_prefix)
			self._store_stats("REDIRECT_ARBITRARY", {"req_id" : req_object[0], "url" : req_url, "to" : redirect_url})
			return (True, {"headers" : {}, "url" : redirect_url}) # Arbitrary requests (i.e., with >=1 unknown parameter values)

		if base_url not in self._cache_rules or parameter_set not in self._cache_rules[base_url]: # Unknown base URL or parameter set
			Logger.spit("Unknown base url OR parameters: %s" % req_url, debug = True, caller_prefix = ReScan._caller_prefix)
			return (None, None)

		parameters = {param : value for param, value in URLUtils.get_query_as_list(req_url)}
		matches = None
		matching_rule = None
		rules = self._cache_rules[base_url][parameter_set]
		for rule in rules: # Check if any rule applies to the request
			if rule["rescan_redirect"]["url"] == req_url:
				Logger.spit("Passing through original: %s" % req_url, caller_prefix = ReScan._caller_prefix)
				return (False, None) # The requested URL was the same as the redirection URL, pass to ReScan
			matches = True
			for param, value in rule.items():
				if param == "rescan_redirect": continue
				if parameters[param] == value or value == "*": continue # Matching parameter value or wildcard
				matches = False # Mismatch in parameter value; move on to next rule
				break
			if matches:
				matching_rule = rule
				break

		if matches:
			Logger.spit("Redirecting: %s -> %s" % (req_url, matching_rule["rescan_redirect"]["url"]), caller_prefix = ReScan._caller_prefix)
			self._store_stats("REDIRECT_SIMILAR", {"req_id" : req_object[0], "url" : req_url, "to" : matching_rule["rescan_redirect"]["url"]})
			return (True, matching_rule["rescan_redirect"]) # Found a matching rule; redirect
		Logger.spit("No matching rules: %s" % req_url, debug = True, caller_prefix = ReScan._caller_prefix)
		return (None, None) # No matching rule, the BG worker must take over

	def _is_arbitrary(self, url):
		base_url = URLUtils.get_main_url(url)
		if base_url not in self._all_known_values or base_url not in self._known_parameters: return (False, None) # Never seen the base URL before, pass to BG worker

		parameters = URLUtils.get_query_as_list(url)
		arbitrary = False
		arbitrary_param = None
		for param, value in parameters:
			if param not in self._all_known_values[base_url] or param not in self._known_parameters[base_url]: return (False, None) # Unknown parameter, pass to BG worker
			if value and value not in self._all_known_values[base_url][param]:
				arbitrary = True
				arbitrary_param = param
		return (arbitrary, arbitrary_param) # All known values, pass to BG worker or match rule

	def _is_api_call(self, url):
		return url.startswith(self._api_url)

	def _handle_api_call(self, req_object):
		req_id, req_method, req_url, req_headers, req_data = req_object # Make sure the object can be de-pickled safely
		parsed = urlparse(req_url)
		path = parsed.path
		params = parse_qs(parsed.query)
		response = {"success" : True}
		Logger.spit("API call: %s" % path, warning = True, caller_prefix = ReScan._caller_prefix)

		if path == "/xss": response["successful_injections"] = list(self._successful_injections)
		elif path == "/isClustered":
			if "url" not in params: response["error"] = "Missing `url` parameter"
			else:
				is_cached, res_object = self._is_cached(("NOID", "GET", params["url"][0], {}, {}), debug = False)
				if is_cached:
					response["clustered"] = True
					response["redirected_to"]  = res_object["url"]
				else: response["clustered"] = False
		elif path == "/enableISD": self._isd.value = 1
		elif path == "/disableISD": self._isd.value = 0
		elif path == "/enableAuth": self._auth.value = 1
		elif path == "/disableAuth": self._auth.value = 0
		elif path == "/enableEvents": self._events.value = 1
		elif path == "/disableEvents": self._events.value = 0
		elif path == "/enableClustering": self._clustering = 1
		elif path == "/disableClustering": self._clustering = 0
		else: return False # Orchestrator cannot handle this API call; pass on to browser workers
		status = 200 if "error" not in response else 400
		pdump((req_id, JSONdumps(response), status, {}), "/tmp/%s.pickle" % req_id)
		return True


	def _store_stats(self, info, stats):
		self._stats_queue.put((info, stats))

	def is_done(self):
		return self._done

	def _shutdown(self):
		self._done.value = 1 # Instruct workers that we are done
		successful_injections = list(self._successful_injections)
		if successful_injections and type(successful_injections[0]) == int: successful_injections.pop(0) # for some reason an <int> value is stored in the xss list during the coversion above; remove it
		if not successful_injections: Logger.spit("No XSS vulnerabilities were detected", warning = True, caller_prefix = ReScan._caller_prefix)
		else:
			Logger.spit("Dumping detected XSS in %s" % self._successful_injections_file, caller_prefix = ReScan._caller_prefix)
			pdump(successful_injections, self._successful_injections_file)
		# for worker in self._workers:
		# 	worker.join() # Clean exit for all subprocesses
		# 	Logger.spit("Joined browser worker", caller_prefix = ReScan._caller_prefix)
		self._graph_worker.join()
		Logger.spit("Joined graph worker", caller_prefix = ReScan._caller_prefix)
		self._stats_worker.join()
		Logger.spit("Joined stats worker", caller_prefix = ReScan._caller_prefix)
		if self._clustering or self._isd:
			self._bg_worker.join()
			Logger.spit("Joined background worker", caller_prefix = ReScan._caller_prefix)
		self._proxy.shutdown()
		Logger.spit("Shutdown proxy", caller_prefix = ReScan._caller_prefix)

	def start(self):
		def worker_exited(sig, frame):
			Logger.spit("WORKER EXITED", error = True, caller_prefix = ReScan._caller_prefix)
		signal.signal(signal.SIGCHLD, worker_exited)
		Logger.spit("Waiting for incoming connections...", caller_prefix  = ReScan._caller_prefix)
		report_time = time()
		poll_time = time()
		try:
			while not self._done.value:
				if time() - report_time >= ReScan._reporting_interval:
					report_time = time()
					Logger.spit("[%s] Alive (%s pending)" % (datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), self._request_queue.qsize()), caller_prefix = ReScan._caller_prefix)
				self._load_requests() # Simply load requests intercepted by the ReScan proxy; they will be consumed by the browser workers
		except KeyboardInterrupt as ex:
			Logger.spit("Shutting down...", caller_prefix  = ReScan._caller_prefix) # ^C by user to terminate the process
		except Exception as ex:
			Logger.spit(stringify_exception(ex), warning = True, caller_prefix = ReScan._caller_prefix)
			with open("/tmp/ex_%s_%s.txt" % (ReScan._caller_prefix, uuid()), "w") as wfp:
				wfp.write(traceback.format_exc())
				wfp.write("\n")
			raise
		finally:
			self._shutdown()

def _boot_worker(wid, target, request_queue, available_workers, submit_pipe, request_pipe, known_headers, isd, isd_links, credentials, auth, auth_oracle, authLax, injection_requests, successful_injections, done, events, url_events, cached_responses, submitted_posts, shared_cookies, unique_tokens, headless, stats_queue, api_url, xproxy_port):
	worker = Worker(wid, target, request_queue, available_workers, submit_pipe, request_pipe, known_headers, isd, isd_links, credentials, auth, auth_oracle, authLax, injection_requests, successful_injections, done, events, url_events, cached_responses, submitted_posts, shared_cookies, unique_tokens, headless, stats_queue, api_url, xproxy_port)
	worker.start()

def _boot_graph_worker(graph_file, target, available_workers, done, *args):
	graph_worker = GraphWorker(graph_file, target, available_workers, done, *args)
	graph_worker.start()

def _boot_bg_worker(target, clustering, isd, available_workers, done, responses, cache_rules, orchestrator_requests_queue, all_known_values, known_parameters, isd_links, submitted_posts, shared_cookies, submit_pipe, request_pipe, headless):
	bg_worker = BackgroundWorker(target, clustering, isd, available_workers, done, responses, cache_rules, orchestrator_requests_queue, all_known_values, known_parameters, isd_links, submitted_posts, shared_cookies, submit_pipe, request_pipe, headless)
	bg_worker.start()

def _boot_stats_worker(stats_queue, available_workers, done):
	stats_worker = StatsWorker(stats_queue, available_workers, done)
	stats_worker.start()
