#!/usr/bin/python3

from xdriver.xutils.Logger import Logger
from xdriver.xutils.URLUtils import URLUtils
from xdriver.xutils.Misc import uuid

from enum import Enum

import re

''' Navigation model / traversal graph for the web app
'''
class Graph(object):
	_caller_prefix = "Graph"

	def __init__(self, target):
		self._target = target
		self._edges = [] # List of graph edges, in the order we encounter them
		self._edge_ids = {} # Map edge key to edge object for O(1) lookup
		self._nodes = {} # Map node key (URL) to node object for fast lookups

	# Either fetch the node corresponding to the given URL OR create a new one if it does not exist
	def _get_node(self, url):
		url = URLUtils.normalize_url(url) if url else url
		self._nodes[url] = self._nodes.get(url, Node(url))
		return self._nodes[url]

	# Create new edge in the graph with the given nodes
	def add_edge(self, src_url, dst_url, parent_edge_id, edge_type, payload = None, info = None):
		if URLUtils.get_main_domain(dst_url, suffix = True, port = True) != self._target.value:
			Logger.spit("Skipping out of scope edge: %s" % dst_url, debug = True, caller_prefix = Graph._caller_prefix)
			return
		src_node = self._get_node(src_url)
		dst_url = URLUtils.normalize_url(dst_url) # Normalize URL to avoid missing the correct edge (e.g. index.php?id=1&page=2 is stored as the edge, but the scanner requests index.php?page=2&id=1)
		dst_node = self._get_node(dst_url)
		parent_edge = self._edge_ids.get(Edge.normalize_edge_id(parent_edge_id), None)

		if self._edges and not parent_edge:
			parent_edge = self._edges[0]  # For arbitrary scanner requests, set their parent edge to the root

		
		new_edge = Edge(src_node, dst_node, parent_edge, edge_type, payload = payload, info = info)
		new_edge_id = new_edge.get_id()

		if not self._edges and src_url != None: # The first time, create an initial edge between an empty node --GET--> src node
			self.add_edge(None, src_url, None, EdgeTypes.GET, payload = None)

		
		if new_edge_id not in self._edge_ids: # New edge
			self._edges.append(new_edge)
			Logger.spit("Added new%sedge %s" % (" arbitrary " if new_edge.is_arbitrary() else " ", new_edge.get_id()), debug = True, caller_prefix = Graph._caller_prefix)
			self._edge_ids[new_edge_id] = new_edge
		else:
			if info and "req_data" in info:
				Logger.spit("Updated existing edge %s with data %s" % (new_edge_id, str(info)), debug = True, caller_prefix = Graph._caller_prefix)
				self._edge_ids[new_edge_id]._info["req_data"] = info["req_data"]
		return self._edge_ids[new_edge_id]

	# Given an edge, construct the workflow leading up to it from the first GET request. Wait for `timeout` seconds if the edge_id is not found
	def get_workflow(self, edge_id, timeout = 0.5):
		edge = self.get_closest_matching_edge(edge_id) # Fetch the given edge and if it doesn't exist as-is, try to find a best match
		if edge: Logger.spit("Closest matching edge for %s is %s" % (edge_id, edge.get_id()), debug = True, caller_prefix = Graph._caller_prefix)
		if not edge: return [] # Fully arbitrary edge was requested, we couldn't find anything similar to it, thus no flow

		flow = [edge] # Target edge we want to follow
		if edge.is_safe(): return flow # If the edge itself is a GET, that's it

		while not edge.get_parent_edge().is_safe(): # Otherwise, move up the graph until we find a safe request (up to the initial edge which is always a GET)
			flow.insert(0, edge._parent_edge)
			edge = edge.get_parent_edge()
		flow.insert(0, edge.get_parent_edge())
		return flow

	def get_closest_matching_edge(self, edge_id):
		etype, eurl, edata = edge_id
		edge = self._edge_ids.get(edge_id) # Try to fetch it in O(1) (if it is an exact match)
		if edge or etype == EdgeTypes.REDIRECT: return edge # If found, return OR if a REDIRECT was explicitly asked for

		if etype == EdgeTypes.GET: # For simple GET requests (no payload), if we didn't hit an exact match, try to locate a REDIRECT, an IFRAME and finally an EVENT edge
			return self._edge_ids.get((EdgeTypes.REDIRECT, eurl, edata), self._edge_ids.get((EdgeTypes.IFRAME, eurl, edata), self._edge_ids.get((EdgeTypes.EVENT, eurl, edata))))
		
		edge = self._edge_ids.get((EdgeTypes.EVENT, eurl, edata)) # Payload bearing requests, can be either a FORM (which we checked) or an EVENT edge
		if edge: return edge

		# For such requests we also have to check for similar edges, e.g. due to the scanner sending a subset of the payload params, so we don't have an exact match, but we still need to locate it
		epath = URLUtils.get_path(eurl)
		edata = set(edata.split("&")) if edata else set() # Get payload names as a set
		common = -1
		closest_path_edge = None
		closest_path_common = -1
		for cur_edge_id, cur_edge in self._edge_ids.items():
			cur_etype, cur_eurl, cur_edata = cur_edge_id
			cur_epath = URLUtils.get_path(cur_eurl)
			'''At this point, we only care about payload comparisons, need to find an edge towards the same URL (or in the worst case same path, different query)
			   and we are looking for either FORM or EVENT edges (the last check is implied; written for clarity). '''
			if (eurl != cur_eurl and epath != cur_epath) or (cur_etype != EdgeTypes.FORM and cur_etype != EdgeTypes.EVENT): continue
			cur_edata = set(cur_edata.split("&")) if cur_edata else set()
			cur_common = len(cur_edata & edata)
			if cur_common and cur_common > common and eurl == cur_eurl: # If the current edge has more common payload names, keep it as the best match (at least one common param required)
				edge = cur_edge
				common = cur_common
			if cur_common and cur_common > closest_path_common and epath == cur_epath:
				closest_path_edge = cur_edge
				closest_path_common = cur_common

		if not edge and closest_path_edge: edge = closest_path_edge

		return edge

	def get_all_edges(self, edge_type): # Return all edge destination URLs with a given type
		return [edge.get_dst_url() for edge in self._edges if edge.get_type() == edge_type and edge.get_dst_url() and not edge.is_arbitrary()]

	# Iterates entire graph and returns all dynamic DOM changes and XHRs we have collected
	def get_all_dynamic_info(self):
		recorded_requests = set()
		recorded_DOM_changes = set()
		for edge in self._edges:
			info = edge.get_info()
			if "xhr_tuple" in info: recorded_requests.add(info["xhr_tuple"])
			if "dom_str" in info: recorded_DOM_changes.add(info["dom_str"])
		return recorded_requests, recorded_DOM_changes

	def ptraverse(self):
		print("============== GRAPH ================")
		for e in self._edges: print(e)
		print("============= END-GRAPH =============")

''' Nodes represent the state of the web app
'''
class Node(object):
	_caller_prefix = "Node"

	def __init__(self, url):
		self._url = URLUtils.normalize_url(url) if url else url # Normalize URL to avoid missing the correct edge (e.g. index.php?id=1&page=2 is stored as the edge, but the scanner requests index.php?page=2&id=1)

	def get_url(self): return self._url

	def __repr__(self):
		return "%s" % self._url

''' Edges transition the web app from one state (node) to another and can be one of the EdgeTypes enum
'''
class Edge(object):
	_caller_prefix = "Edge"

	def __init__(self, src_node, dst_node, parent_edge, edge_type, payload = None, info = None):
		''' Construct the edge's ID by using the edge type and destination URL. If there is a payload, also include the keys
		E.g. (GET, http://example.com/page.php) or (POST, http://example.com/login.php, "username&password&csrf") 
		Intuition for the latter is that the destination URL and edge type might be the same, but the form payload might be different, thus resulting in a different state '''
		self._id = Edge.normalize_edge_id((edge_type, dst_node._url, payload))
		self._type = edge_type
		self._src_node = src_node
		self._dst_node = dst_node
		self._parent_edge = parent_edge
		self._payload = payload
		self._info = info if info else {}

	# Getters
	def get_id(self): return self._id
	def get_type(self): return self._type
	def get_src_url(self): return self._src_node.get_url() if self._src_node else None # Handle initial None node
	def get_dst_url(self): return self._dst_node.get_url()
	def get_payload(self): return self._payload
	def get_info(self): return self._info
	def get_parent_edge(self): return self._parent_edge
	def get_parent_edge_id(self): return self._parent_edge.get_id() if self._parent_edge else None # Handle initial None node
	def is_arbitrary(self): return self._info.get("arbitrary", False)

	@classmethod
	def normalize_edge_id(cls, edge_id):
		if not edge_id: return edge_id # None
		return (edge_id[0], URLUtils.normalize_url(edge_id[1]), "&".join(sorted(edge_id[2].split("&"))) if edge_id[2] else edge_id[2]) # Simply sort them alhpabetically. This is to avoid mixing up same IDs due to different payload ordering

	# True if the given edge is a GET request
	def is_safe(self):
		return self._type is EdgeTypes.GET

	def __repr__(self):
		return "%s ==[%s]%s==> %s" % (self._src_node.__repr__(), self._type.name, "" if not self._payload else "(%s)" % self._id[2], self._dst_node.__repr__())


class EdgeTypes(Enum):
	GET = 1
	FORM = 2
	IFRAME = 3
	EVENT = 4
	REDIRECT = 5