#!/usr/bin/python3

import os

import sys
sys.path.extend([
	os.path.join(os.path.expanduser("~"), ".local/lib/python3.8/site-packages"),
	'/usr/local/lib/python3.8/dist-packages',
    '/usr/lib/python3/dist-packages',
    '/usr/lib/python38.zip',
    '/usr/lib/python3.8',
    '/usr/lib/python3.8/plat-x86_64-linux-gnu',
    '/usr/lib/python3.8/lib-dynload'])

from mitmproxy.script import concurrent
from mitmproxy import http

from time import sleep, time
import re
import _pickle
import json

from random import randint
from uuid import uuid4

from threading import Lock


def load(filename):
	with open(filename, 'rb') as fp:
		return _pickle.load(fp, encoding = "utf-8")
def dump(obj, filename):
	with open(filename, 'wb') as wfp:
		_pickle.dump(obj, wfp, protocol = 2)

def persist_load(filename, max_retries = 200): # Keep loading until successful, or the max retries are exceeded
	retries = 0
	while retries < max_retries:
		try:
			config = load(filename)
			return config
		except (EOFError): continue
		except Exception as ex:
			retries += 1
			if retries >= max_retries: raise
			continue

if len(sys.argv) < 1:
	print("[!] Need proxy intercomm filename")
	sys.exit(1)

_install_dir = sys.argv[3] # xdriver's install directory
sys.path.extend([_install_dir]) # xdriver's imports below this line
# from xdriver.xutils.Misc import uuid
from xdriver.xutils.URLUtils import URLUtils

def uuid():
	return str(uuid4())

print("[!] BOOTED mitmdump script")
_requests_intercomm_file = sys.argv[1] # Lock-based writing only
_config = persist_load(_requests_intercomm_file) # Only need to load it once on boot

_requests = _config["rescan_mode"]["requests"] # lock-based write # Should be initialized as an empty list, but if the mitmdump script gets reloaded, it would get overwritten
_requests_len = 0 # Current requests' length, so we don't have to call len() on a huge list all the time
_requests_intercomm_file_lock = Lock() # Same lock for requests file and requests list

_requests_threshold = 200
_preserved_requests = int(_requests_threshold/2) # How many requests to preserve when resetting the requests' list

_delayed_requests = set() # lock-based write
_delayed_requests_lock = Lock()

_last_seen_get = {}
_last_seen_get_threshold = 20
_last_seen_get_lock = Lock()

_known_headers = []
_known_headers_lock = Lock()

_timeout_interval = 120
_max_timeout_limit = 990

_passthrough_suffixes = r"\.(js|json|css|crt|mp3|wav|wma|ogg|mkv|zip|gz|tar|xz|rar|z|deb|iso|csv|tsv|dat|txt|log|sql|xml|mdb|apk|bat|bin|exe|jar|wsf|fnt|fon|otf|ttf|ai|bmp|gif|ico|jp(e)?g|png|ps|psd|svg|tif|tiff|cer|rss|key|odp|pps|ppt|pptx|c|class|cpp|cs|h|java|sh|swift|vb|odf|xlr|xls|xlsx|bak|cab|cfg|cpl|cur|dll|dmp|drv|icns|ini|lnk|msi|sys|tmp|3g2|3gp|avi|flv|h264|m4v|mov|mp4|mp(e)?g|rm|swf|vob|wmv|doc(x)?|odt|pdf|rtf|tex|wks|wps|wpd|woff|eot|xap)$"

def add_request(req_object):
	global _requests, _requests_len, _requests_threshold, _preserved_requests
	with _requests_intercomm_file_lock: # Acquire lock
		_requests_len = len(_requests)
		if _requests_len == _requests_threshold: # Reset requests' list if we hit the threshold; This is necessary since pickling is limited at 4096. Also, better to keep the file small for reduced I/O loads
			_requests = _requests[_requests_threshold-_preserved_requests:] # Preserve last chunk of requests so we give time to the main framework to read all requests
			print("[!] RESET REQUESTS")
		_requests.append(req_object) # Append new request
		_config["rescan_mode"]["requests"] = _requests
		dump(_config, _requests_intercomm_file) # And write requests list safely so it can be retrieved by the framework

def read_response_from_pickle(req_object):
	response_file = "/tmp/%s.pickle" % req_object[0]
	response = None
	start = hard_start = time()
	delayed = False
	global _delayed_requests, _delayed_requests_lock
	while not response:
		if time() - start >= _timeout_interval:
			print("[!] Request takes too long to complete: %s %s (%s) (%s delayed)" % (req_object[1], req_object[2], req_object[0], len(_delayed_requests)))
			start = time()
			if not delayed:
				delayed = True
				with _delayed_requests_lock:
					_delayed_requests.add(req_object[0])
		if time() - hard_start >= _max_timeout_limit:
			print("[!] Request did not get a response; passing to web app: %s %s (%s)" % (req_object[1], req_object[2], req_object[0]))
			with _delayed_requests_lock: _delayed_requests.remove(req_object[0])
			return (None, None, 666, None)
		if not os.path.exists(response_file): continue # The response has not been written yet
		response = persist_load(response_file)
		os.remove(response_file) # Cleanup
		if delayed:
			with _delayed_requests_lock: _delayed_requests.remove(req_object[0])
			print("[<] Delayed request handled: %s %s (%s) (%s delayed)" % (req_object[1], req_object[2], req_object[0], len(_delayed_requests)))
	return response

def decode_payload_data(req_data):
	if not req_data: return req_data # empty payload
	if type(list(req_data.keys())[0]) == str: return req_data # regular str data
	# Otherwise, we have bytes from multipart-form data
	decoded_data = {}
	for k, v in req_data.items():
		try: decoded_data[k.decode()] = v.decode()
		except Exception as e: decoded_data[k.decode()] = str(v).replace("b'", "")[:-1]
	return decoded_data

PRINT = False

@concurrent
def request(flow):
	if "XDRIVER-HEARTBEAT" in flow.request.headers: return  # Skip boot/heartbeat requests from XDriver
	if "owaspzap-news" in flow.request.url or "raw.githubusercontent.com/zaproxy" in flow.request.url or "owaspzap-2" in flow.request.url: return

	global PRINT
	request_url = flow.request.url
	''' Don't pass these requests through ReScan; instead redirect them directly to the web app.
		Intuition is that such files are not state-changing and thus we don't want to burden the system with
		redundant requests to such resources. However, we still want the scanner to retrieve them, as we need it to
		be as close to the real thing as possible and it generally should get all info it requests. '''
	if re.search(_passthrough_suffixes, request_url, re.IGNORECASE): return

	req_repr = (flow.request.method, request_url)
	global _last_seen_get, _last_seen_get_lock, _last_seen_get_threshold
	with _last_seen_get_lock:
		if not _last_seen_get:
			_last_seen_get["url"] = req_repr
			_last_seen_get["count"] = 0

		''' w3af has an internal bug which, at some point during the scan, will send a lot of consecutive GET requests to the same URL. Let the first `_last_seen_get_threshold` go through, but do not pass anymore through ReScan
		'''
		if flow.request.method == "GET" and _last_seen_get.get("url") == req_repr:
			_last_seen_get["count"] += 1
			if _last_seen_get["count"] > _last_seen_get_threshold:
				if _last_seen_get["count"] % _last_seen_get_threshold == 1: print("Returning cached response for %s consecutive (%s, %s, %s)" % (_last_seen_get["count"], _last_seen_get.get("url")[0], _last_seen_get["response"].status_code, _last_seen_get.get("url")[1]))
				flow.response = _last_seen_get["response"] # Return cached reponse
				return
		else:
			_last_seen_get["url"] = req_repr
			if _last_seen_get["count"] >= _last_seen_get_threshold: print("Resuming regular requests... (%s, %s)" % (_last_seen_get.get("url")[0], _last_seen_get.get("url")[1]))
			_last_seen_get["count"] = 1

	req_id = uuid()

	request_headers = dict({k.lower() : v for k,v in list(flow.request.headers.items())})
	
	''' Yet another w3af mod; w3af seems to occasionally mess up the Set-Cookie headers we send back (not sure why, the Set-Cookie header has the correct format) and *sometimes* sends the entire
		Set-Cookie header as a request Cookie header, essentially sending back cookie attributes as separate cookies, e.g. Path=/; Domain=.example.com etc.
		Thankfully, the corrupt Cookie header includes the valid cookies as well, so we can remove the unwanted parts. '''
	if "cookie" in request_headers:
		filtered_cookie_string = ""
		cookie_string = request_headers["cookie"]
		for token in [cookie.split("=") for cookie in cookie_string.split(";")]:
			cookie_name = token[0].strip()
			cookie_value = "=".join(token[1:]).strip()
			if not cookie_name or cookie_name in ["Path", "Max-Age", "Domain"]: continue
			filtered_cookie_string += "%s=%s; " % (cookie_name, cookie_value)
		if filtered_cookie_string: request_headers["cookie"] = filtered_cookie_string
		else: request_headers.pop("cookie")

	req_data = dict(flow.request.urlencoded_form) # By default, get the URL-encoded form data (might be empty)
	if flow.request.multipart_form: req_data = dict(flow.request.multipart_form) # If multipart-form is given, take that (requests can have only one of two)

	req_data = decode_payload_data(dict(req_data))

	if flow.request.multipart_form:
		multipart_form_content = flow.request.content.decode("utf-8", "ignore")
		for line in multipart_form_content.split("\n"):
			filename_match = re.search(r"filename=(\"|')(.*)(\"|')", line, re.IGNORECASE)
			if filename_match:
				parameter_name_match = re.search(r"name=(\"|')(.*)(\"|');", line, re.IGNORECASE)

				if "xdriver_filenames" not in req_data: req_data["xdriver_filenames"] = {}
				req_data["xdriver_filenames"][parameter_name_match.group(2)] = filename_match.group(2) # Map multipart form data parameters to their filenames, so we can properly set them later
	
	req_object = (req_id, flow.request.method, request_url, request_headers, req_data)
	add_request(req_object) # Append in requests' list

	resp = read_response_from_pickle(req_object) # Read response from main ReScan framework, when ready

	body, status_code, headers = resp[1:] # Structure is tuple(<req_id>, str(response_body), int(status_code), dict(response_headers))
	if status_code == 666: return # Dummy status code that indicates the request was not completed in the time limit; proxy directly to the web app

	response_headers = []
	for header, value in headers.items():
		if header == "set-cookie": # Special treatment :). See https://github.com/mitmproxy/mitmproxy/blob/v2.0.2/mitmproxy/net/http/cookies.py
			for set_cookie in value: response_headers.append(("Set-Cookie".encode(), set_cookie.encode("utf8", "surrogateescape"))) # Encode everything in bytes
		else: response_headers.append((header.encode("utf8", "surrogateescape"), value.encode("utf8", "surrogateescape")))

	global _known_headers, _known_headers_lock
	if len(headers.keys()) == 1: # Redirected arbitrary request from ReSscan (URL clustering), that only inclues the "Location" header
		print("[+] Filling in remaining headers for redirected request")
		for header in _known_headers: # Fill in remaining known headers
			response_headers.append(header)

	if not _known_headers: # Just once, learn the default header values the app sends back, so we can use them later for realistic redirects (if clustering is enabled)
		with _known_headers_lock:
			if not _known_headers: # Make sure nobody else filled them in the meantime
				for header, value in headers.items():
					if header == "set-cookie" or header == "content-length": continue # Varying headers that can either affect the state or cause the scanner to hang if the content-lengths don't match
					_known_headers.append((header.encode("utf8", "surrogateescape"), value.encode("utf8", "surrogateescape")))
				print("[+] Setting default headers from %s to: " % request_url)
				for header in _known_headers:
					print("\t> %s : %s" % (header[0], header[1]))

	crafted_response = http.HTTPResponse.make(status_code = status_code, headers = response_headers)
	crafted_response.text = body
	flow.response = crafted_response

	with _last_seen_get_lock:
		if flow.request.method == "GET" and _last_seen_get.get("url") == req_repr:
			_last_seen_get["response"] = crafted_response

@concurrent
def response(flow):
	pass