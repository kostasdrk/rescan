#!/usr/bin/python3

from xdriver.XDriver import XDriver
from xdriver.xutils.Logger import Logger
from xdriver.xutils.URLUtils import URLUtils
from xdriver.rescan.Graph import EdgeTypes
from xdriver.xutils.NDD import NDD
from xdriver.xutils.Regexes import Regexes
from xdriver.xutils.Misc import uuid, pdump
from xdriver.xutils.ModuleTimer import Timer
Logger.set_debug_on()
from xdriver.xutils.Exceptions import stringify_exception

from time import time, sleep
from datetime import datetime
import traceback

import pandas as pd
from scipy.stats import entropy

import re

from itertools import combinations

# Concurrency imports #
from queue import Empty

from multiprocessing import Process, Manager, Lock, Queue

class BackgroundWorker(object):
	_caller_prefix = "BG-Worker"

	_NDD_THRESHOLD = 0.009 # All actually similar URLs (+ DOMs) that don't have any meaningful differences (other than content) do not exceed this
	_NDD_PRUNING_LEVEL = 0 # In ReScan's case we don't want to prune the DOM trees, as we might have to capture subtle changes deep inside the tree to consider two DOMs as different -- A more fine grained pruning is employed though
	_NDD_TRHEADS = 4 # Numbfer of NDD worker threads

	_ISD_SINK_TRHESHOLD = 5 # Max ISD sinks to consider for each ISD source

	_reporting_interval = 300 # Report every 

	def __init__(self, target, clustering, isd, available_workers, done, cached_responses, cache_rules, orchestrator_requests_queue, all_known_values, known_parameters, isd_links, submitted_posts, shared_cookies, submit_pipe, request_pipe, headless = True):
		self._available_workers = available_workers
		self._done = done

		self._target = target

		self._isd = isd
		self._clustering = clustering

		self._submit_pipe = submit_pipe
		self._request_pipe = request_pipe

		self._shared_cookies = shared_cookies

		self._isd_links = isd_links
		self._submitted_posts = submitted_posts
		self._isd_checks = {} # ISD detection state. Key: (source edge id, param name), value: dict({"values" : [list of submitted values for this param], "checked" : [list of GETs that we already checked]})

		self._manager = Manager()
		self._rules_locks = self._manager.dict() if clustering else None
		self._base_lock = self._manager.Lock() if clustering else None

		self._cached_responses = cached_responses # Hold responses for each URL we encounter. key: URL, value: response object (DOM, headers, status code)
		self._cache_rules = cache_rules # Decided cache rules; will be used in a read-only manner by the orchestartor and intercepting proxy
		self._orchestrator_requests_queue = orchestrator_requests_queue

		self._known_parameters = known_parameters # Store parameter values that have been passed back to the scanner -- Key: base url (i.e. without query params), value: dict(key: param name, value: set(known values))

		self._inspected_urls = set()
		self._all_known_values = all_known_values # Store all known parameter values -- Key: base url (i.e. without query params), value: dict(key: param name, value: set(known values))

		XDriver.enable_internal_proxy(strip_media = True) # Reduce load overheads by stripping media files from responses
		XDriver.enable_browser_args(headless = headless)
		self._driver = XDriver.boot(chrome = True)

		self._driver.spoof_headers({"X_RESCAN_BG_WORKER" : "1"}) # Send custom header in all BG Worker requests, so they are not counted for the code coverage as they are not scanner originated

		''' The requests thread will get pending responses from the self._cached_responses dict in the order of the keys() method.
			However, the queue is needed when an NDD thread needs immediately the response for a given URL; the requests thread will priorize that. '''
		self._immediate_requests_queue = Queue() if clustering else None
		if clustering: # Boot NDD threads only if URL clustering is enabled
			self._ndd_threads = [Process(target = _start_ndd_thread, args = (i, self._done, self._manager, self._orchestrator_requests_queue, self._immediate_requests_queue, self._known_parameters, self._cache_rules, self._cached_responses, self._rules_locks, self._base_lock)) for i in range(BackgroundWorker._NDD_TRHEADS)] # NDD threads
		self._available_workers.value += 1
		Logger.spit("Ready", caller_prefix = BackgroundWorker._caller_prefix)

	def _shutdown(self):
		from json import dumps
		self._driver.quit()
		
		rules = {}
		rules_outfile = "/tmp/rules_%s.pickle" % uuid()
		try:
			for base_url in self._cache_rules.keys():
				rules[base_url] = {}
				for parameter_set in self._cache_rules[base_url]:
					rules[base_url][parameter_set] = []
					for rule in self._cache_rules[base_url][parameter_set]:
						rules[base_url][parameter_set].append(dict(rule))
						rule["rescan_redirect"].pop("headers", None)
			Logger.spit("Cache rules in: %s" % rules_outfile, caller_prefix = BackgroundWorker._caller_prefix)
			pdump(rules, rules_outfile)
		except Exception as ex:
			print("[!] BG Worker exception when dumping rules: \n%s" % stringify_exception(ex))

		for thread in self._ndd_threads: thread.join()
		Logger.spit("Joined NDD threads", caller_prefix = BackgroundWorker._caller_prefix)

	def _setup_cookiejar(self, req_url):
		try:
			if URLUtils.get_main_domain(req_url, suffix = True) != URLUtils.get_main_domain(self._driver.current_url(), suffix = True):
				Logger.spit("Changing initial URL to %s" % req_url, debug = True, caller_prefix = BackgroundWorker._caller_prefix)
				self._driver.get(req_url)
		except Exception as ex:
			Logger.spit("Exception when checking domain: (%s, %s): %s" % (req_url, self._driver.current_url(), stringify_exception(ex)), debug = True, caller_prefix = self._caller_prefix)
			Logger.spit("Falling back to base target %s" % self._target.value, debug = True, caller_prefix = self._caller_prefix)
			self._driver.get(self._target.value)


		for cookie in self._driver.get_cookies(): # Initially purge all cookies
			try: self._driver.delete_cookie(cookie["name"])
			except Exception as ex: print("%s  -  %s  " % (cookie["name"], str(ex)))

		if not self._shared_cookies.value:
			Logger.spit("No shared cookies were set", warning = True, caller_prefix = self._caller_prefix)
			return

		cookie_string = self._shared_cookies.value
		for token in [cookie.split("=") for cookie in cookie_string.split(";")]:
			cookie_name = token[0].strip()
			cookie_value = "=".join(token[1:]).strip()
			if not cookie_name: continue

			try:
				self._driver.add_cookie({"name" : cookie_name, "value" : cookie_value, "domain" : "."+URLUtils.get_main_domain(req_url, suffix = True), "path" : "/", "secure" : False, "httpOnly" : False})
			except Exception as ex:
				Logger.spit("Exception when setting cookie: %s" % stringify_exception(ex), warning = True, caller_prefix = self._caller_prefix)
				Logger.spit(self._driver.current_url(), debug = True, caller_prefix = self._caller_prefix)
				Logger.spit(cookie_string, debug = True, caller_prefix = self._caller_prefix)
				Logger.spit((cookie_name, cookie_value), debug = True, caller_prefix = self._caller_prefix)

	''' Main thread
	'''
	def start(self):
		if self._clustering:
			for ndd_thread in self._ndd_threads: ndd_thread.start()
		try:
			report_time = time()
			last_inspected_get = None
			while not self._done.value:
				if time() - report_time >= BackgroundWorker._reporting_interval:
					report_time = time()
					Logger.spit("[%s] Alive" % datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), caller_prefix = BackgroundWorker._caller_prefix)

				if self._clustering: self._serve_ndd_threads() # Always prioritize mnDD threads' requests, since they need to be served ASAP
				if self._isd.value: # Only if ISD detection is enabled
					self._collect_posts()
					if self._isd_checks:
						get_edges = self._get_all_GET_edges()
						ll = last_inspected_get # Only for debugging
						last_inspected_get = self._detect_isd_links(get_edges, last_inspected_get)
						if not last_inspected_get and ll:
							Logger.spit("Restarting edges (%s)" % len(get_edges), warning = True, caller_prefix = self._caller_prefix)
		except KeyboardInterrupt as ex:
			Logger.spit("Background worker shutting down...", caller_prefix = BackgroundWorker._caller_prefix) # ^C by user to terminate the process
		except Exception as ex:
			Logger.spit(stringify_exception(ex), warning = True, caller_prefix = BackgroundWorker._caller_prefix)
			with open("/tmp/ex_%s_%s.txt" % (BackgroundWorker._caller_prefix, uuid()), "w") as wfp:
				wfp.write(traceback.format_exc())
				wfp.write("\n")
			raise
			self._done.value = 1
		finally:
			self._shutdown()

	def _detect_isd_links(self, get_edges, last_inspected_get):
		for get_url in get_edges:
			if last_inspected_get and get_url != last_inspected_get: continue # Skip until we reach the last inspected GET
			if last_inspected_get: # Reached last inspected GET
				last_inspected_get = None
				continue

			fetched = False # Mark if we made an actual GET, so we can return and check for mNDD sub-thread requests
			source = None
			prerendered_source = None

			for isd_src in self._isd_checks:
				if self._isd_checks[isd_src]["capped"]: continue # Do not consider ISD source if the max number of sinks has already been detected -- rare case, but preevents a prohibitive number of sinks for some value that appears everywhere
				if get_url in self._isd_checks[isd_src]["checked"]: continue # Already checked this GET for this ISD source
				if not fetched: # Just fetch it once
					try:
						self._setup_cookiejar(get_url)
						self._driver._proxy.redirection_mode(get_url)
						self._driver.get(get_url)
						flow = self._driver._proxy.get_redirection_flow()
						fetched = True
						source = self._driver.rendered_source()
						prerendered_source = flow[-1]["html"] if flow and "html" in flow[-1] else None
						self._submit_new_edges() # Make sure we inform the Graph Worker for any new edges we find
					except Exception as ex: break

				values_regex = "|".join([re.escape(value) for value in self._isd_checks[isd_src]["values"]])
				if re.search(values_regex, source) or (prerendered_source and re.search(values_regex, prerendered_source)): # Check both in the rendered and pre-rendered sources
					self._isd_links[isd_src] += [get_url] # Got candidate sink. Update so browser workers can see it
					Logger.spit("ISD: %s %s (%s) > %s" % (isd_src[0][0], isd_src[0][1], isd_src[1], get_url), debug = True, caller_prefix = self._caller_prefix)
					if len(set(self._isd_links[isd_src])) >= BackgroundWorker._ISD_SINK_TRHESHOLD:
						Logger.spit("ISD sink cap reached: %s %s (%s)" % (isd_src[0][0], isd_src[0][1], isd_src[1]), debug = True, caller_prefix = self._caller_prefix)
						self._isd_checks[isd_src]["capped"] = True

				self._isd_checks[isd_src]["checked"].add(get_url)
			if fetched: return get_url # If not `fetched` this GET has already been checked against all known POSTs, so move on to next one
		return None # All GETs have been exhausted; return None, so in the next iteration we'll start from the beginning

	def _collect_posts(self): # Collect all POST requests executed by the browser workers and filter out the higher-entropy and forbidden values (or params) for ISD detection
		ENTROPY_THRESHOLD = 1.4
		forbidden_keywords = set(["default", "submit", "save", "confirm", "okay", "ok", "search", "cancel", "reset", "preview", "post", "delete", "true", "false", "json", "view", "announce"])
		allowed_keywords = set([])
		while True:
			post_req = None
			try: post_req = self._submitted_posts.get_nowait()
			except Empty as ex: break

			edge_id, payload_str, timestamp = post_req

			payload = URLUtils.payload_to_dict(payload_str)
			for param_name, param_value in payload.items():
				if param_value.lower() not in allowed_keywords and param_value.lower() in forbidden_keywords or re.search(Regexes.NONCE, param_name, re.IGNORECASE) or any([keyword in param_name.lower() for keyword in forbidden_keywords]) or self._entropy(param_value) < ENTROPY_THRESHOLD: continue # Low entropy or forbidden value (or nonce param). No need to consider for ISD, since we will flood with candidate sinks, i.e. performance/detection trade-off
				Logger.spit("Adding ISD value %s" % param_value, debug = True, caller_prefix = self._caller_prefix)
				isd_src = (edge_id, param_name) # ISD source is the source edge id + the actual parameter name
				if isd_src not in self._isd_checks:
					self._isd_checks[isd_src] = {"values" : set(), "checked" : set(), "capped" : False}
					self._isd_links[isd_src] = []
				self._isd_checks[isd_src]["values"].add(param_value)

	def _entropy(self, value):
		return float(entropy(pd.Series(list(value)).value_counts()))
		
	def _submit_new_edges(self):
		src_url = URLUtils.strip_fragment(self._driver.current_url()) # Remove fragments from current URL to get base source URL for graph
		new_edges = []
		links = self._driver.get_internal_links(strip_files = True)
		for link in links: self._submit_pipe.put((src_url, URLUtils.strip_fragment(link), (EdgeTypes.GET, src_url, None), EdgeTypes.GET, None, None))

	def _get_all_GET_edges(self):
		self._request_pipe.send((EdgeTypes.GET, "*"))
		get_edges = self._request_pipe.recv()[0]
		self._request_pipe.send((EdgeTypes.IFRAME, "*"))
		iframe_edges = self._request_pipe.recv()[0]
		self._request_pipe.send((EdgeTypes.REDIRECT, "*"))
		redirect_edges = self._request_pipe.recv()[0]
		edges = set(get_edges + iframe_edges + redirect_edges)

		exclude_regex = r"passw(or)?d|theme|plugin|addon|settings|activate|enable|disable|unapprove|delete|remove|spam|trash|(log|sign)(_|-|\s)*(out|off|in|on|up)|register|update|upgrade|install"

		return [edge_url for edge_url in edges if not re.search(exclude_regex, edge_url, re.IGNORECASE)] # Make sure to filter out "dangerous" URLs, e.g. logount, login, delete etc.

	def _serve_ndd_threads(self): # Check if any NDD sub-thread has requested a URL
		self._update_all_known_values()
		try:
			req_object = self._immediate_requests_queue.get_nowait()
			req_id, req_method, req_url, req_headers, req_data = req_object
			url = URLUtils.normalize_url(req_url)
			self._cached_responses[url] = None
			Logger.spit("Will serve: %s" % url, debug = True, caller_prefix = BackgroundWorker._caller_prefix)
		except Empty as ex: return

		self._setup_cookiejar(req_url)
		self._driver._proxy.redirection_mode(url) # Get redirection flow
		self._driver.get(url)
		flow = self._driver._proxy.get_redirection_flow()
		response_headers = flow[-1]["response_headers"] if flow else {}
		if "set-cookie" in response_headers: response_headers.pop("set-cookie") # Remove anything that might break the session
		self._cached_responses[url] = {"source" : self._driver.rendered_source(), "status_code" : flow[-1]["status_code"] if flow else 200, "headers" : response_headers, "url" : url}
		self._driver.clear_state()


	def _update_all_known_values(self): # Collect all known values for each parameter per base URL
		urls = self._cached_responses.keys()
		for url in urls:
			if url in self._inspected_urls: continue
			
			base_url = URLUtils.get_main_url(url)
			parameters = URLUtils.get_query_as_list(url)
			if base_url not in self._all_known_values: self._all_known_values[base_url] = {}

			known_values_dict = self._all_known_values[base_url]
			for param, value in parameters:
				if param not in known_values_dict: known_values_dict[param] = set()
				known_values_dict[param].add(value)
			self._all_known_values[base_url] = known_values_dict
			self._inspected_urls.add(url)


def _request_url(req_object, _immediate_requests_queue, _cached_responses):
	req_id, req_method, req_url, req_headers, req_data = req_object
	url = URLUtils.normalize_url(req_url)
	if url not in _cached_responses or not _cached_responses[url]:
		_immediate_requests_queue.put(req_object)
		while url not in _cached_responses or not _cached_responses[url]: continue # Wait until response is ready
	return _cached_responses[url]

def _start_ndd_thread(tid, _done, manager, _orchestrator_requests_queue, _immediate_requests_queue, _known_parameters, _cache_rules, _cached_responses, _rules_locks, _base_lock):
	_caller_prefix = "%s[NDD-%s]" % (BackgroundWorker._caller_prefix, tid)
	try:
		report_time = time()
		while not _done.value:
			if time() - report_time >= BackgroundWorker._reporting_interval:
				report_time = time()
				Logger.spit("[%s] Alive" % datetime.fromtimestamp(report_time).strftime("%Y-%m-%d %H:%M:%S"), caller_prefix = _caller_prefix)

			try: req_object = _orchestrator_requests_queue.get_nowait()
			except Empty as ex: continue

			req_id, req_method, req_url, req_headers, req_data = req_object
			url = URLUtils.normalize_url(req_url) # We expect the orchestrator to normalize the URL, but we need to be sure
			base_url = URLUtils.get_main_url(url)

			if base_url not in _rules_locks: # First time seeing this base URL
				with _base_lock: # Acquire base lock for all _rules_locks to safely initialize base URL lock
					if base_url not in _rules_locks: _rules_locks[base_url] = manager.Lock() # Make sure noone initialized it already (RC in the previous two LoC)

			locked = _rules_locks[base_url].acquire(blocking = False) # Do not block until the lock for this base URL is released
			if not locked:
				Logger.spit("Rescheduling %s" % req_url, debug = True, caller_prefix = _caller_prefix)
				_orchestrator_requests_queue.put(req_object) # Enqueue request object again and move on to the next pending
				continue

			res_object = _request_url(req_object, _immediate_requests_queue, _cached_responses)
			dom = res_object["source"]
			res_object.pop("source", None)
			res_object.pop("status_code", None)

			parameters = URLUtils.get_query_as_list(url)
			parameter_set = tuple(set(URLUtils.get_query_parameters(url)))
			if base_url not in _known_parameters: # First time we ecounter the base URL
				Logger.spit("Setting initial rule: %s" % url, caller_prefix = _caller_prefix)
				_known_parameters[base_url] = {}

				params_dict = _known_parameters[base_url]
				for param, value in parameters: params_dict[param] = set([value])
				_known_parameters[base_url] = params_dict

				_cache_rules[base_url] = { # Initial rule will show to the URL itself (so basically no redirection will occur)
					parameter_set : [
						{param : value for param, value in parameters + [("rescan_redirect", res_object)]}
					]
				}
				_rules_locks[base_url].release() # Release base URL lock for other threads to proceed with related URLs
				continue # No need for anything else

			ignored_params = []
			known_and_differing_params = []
			for param, value in parameters:
				 # If we haven't seen that parameter name yet OR if we know that parameter's value OR we have more than one known value already (most likely this param lead to different pages), ignore
				if param not in _known_parameters[base_url] or value in _known_parameters[base_url][param] or len(_known_parameters[base_url][param]) > 1:
					ignored_params.append((param, value))
					continue
				known_and_differing_params.append((param, value)) # Parameters we have seen before AND have an unknown value in this URL

			base_check_url = "%s?" % base_url

			redirect_url = None
			redirect_res_object = None
			wildcard_params = []
			combo_lengths = range(len(known_and_differing_params), 0, -1)
			for combo_length in combo_lengths: # Start by swapping *all* known and differing parameters and decrease until we find a similar URL OR we exhausted the params
				for parameter_combo in combinations(known_and_differing_params, combo_length):
					check_url = base_check_url
					for param, value in parameter_combo:
						check_url += "%s=%s&" % (param, list(_known_parameters[base_url][param])[0]) # Swap parameter value with known value
					for param, value in parameters:
						if (param, value) not in parameter_combo: # For smaller combos, also maintain the base parameter values
							check_url += "%s=%s&" % (param, value)
					check_url = URLUtils.normalize_url(check_url[:-1]) # Remove last "&" and normalize
					check_req_object = (req_id, req_method, check_url, req_headers, req_data)
					check_res_object = _request_url(check_req_object, _immediate_requests_queue, _cached_responses)
					check_dom = check_res_object["source"]
					check_res_object.pop("source", None)
					check_res_object.pop("status_code", None)
					# check_res_object.pop("headers", None)
					ndd_start = time()
					ndd = NDD.compare(dom, check_dom, smart_pruning = True)
					Logger.spit("NDD: %s  |  %s  (%.4f) -- %.2f" % (URLUtils.get_query(url), URLUtils.get_query(check_url), ndd, time() - ndd_start), debug = True, caller_prefix = _caller_prefix)
					if ndd > BackgroundWorker._NDD_THRESHOLD: continue # Different DOM, cannot infer anything yet
					wildcard_params = [param for param, value in parameter_combo] # Swapped params that led to a similar URL+DOM
					redirect_url = check_url
					redirect_res_object = check_res_object
					break
				if redirect_url: break # Found similar URL, no need to check any other parameter combinations


			if parameter_set not in _cache_rules[base_url]: # Init newly seen parameter set for that base URL
				parameter_sets_dicts = _cache_rules[base_url]
				parameter_sets_dicts[parameter_set] = []
				_cache_rules[base_url] = parameter_sets_dicts

			if not redirect_url: # No similar URL was found; add new rule as-is, based on original URL's parameters
				params_dict = _known_parameters[base_url]
				for param, value in parameters:
					if param not in params_dict: params_dict[param] = set()
					params_dict[param].add(value) # Consdier all parameters as newly seen values
				_known_parameters[base_url] = params_dict
				parameter_sets_dicts = _cache_rules[base_url] # MP dict values need to be updated as a whole, otherwise the changes are not handled by the Manager
				parameter_sets_dicts[parameter_set] += [{ # Cannot append() in shared mp dicts
						param : value for param, value in parameters + [("rescan_redirect", res_object)]
					}]
				_cache_rules[base_url] = parameter_sets_dicts
				Logger.spit("Setting new base: %s" % url, caller_prefix = _caller_prefix)
			else: # Redirected to similar URL
				params_dict = _known_parameters[base_url]
				for param, value in URLUtils.get_query_as_list(redirect_url):
					if param not in params_dict: params_dict[param] = set()
					params_dict[param].add(value) # Consider all redirect URL's parameters as newly seen values
				_known_parameters[base_url] = params_dict

				parameter_sets_dicts = _cache_rules[base_url] # MP dict values need to be updated as a whole, otherwise the changes are not handled by the Manager
				updated_rule = {}
				rule_to_update = None

				for rule in parameter_sets_dicts[parameter_set]:
					if rule["rescan_redirect"]["url"] != redirect_url: continue # Locate corresponding rule to enhance, by finding the redirect URL that came up
					for param in rule: updated_rule[param] = rule[param] # Copy existing rule
					for param in wildcard_params: updated_rule[param] = "*" # Update wildcards
					rule_to_update = rule
					break

				if rule_to_update: # Update existing rule
					parameter_sets_dicts[parameter_set].remove(rule_to_update) # Cannot modify in place, so remove it and add the updated rule
					parameter_sets_dicts[parameter_set] += [updated_rule]
					Logger.spit("Updated redirect rule %s  -->   %s" % (url, redirect_url), caller_prefix = _caller_prefix)
				else: # Add new rule for parameter set
					parameter_sets_dicts[parameter_set] += [{
						param : value if param not in wildcard_params else "*" for param, value in parameters + [("rescan_redirect", redirect_res_object)]
					}]
					Logger.spit("Setting PROACTIVE redirect %s  -->   %s" % (url, redirect_url), caller_prefix = _caller_prefix)
				_cache_rules[base_url] = parameter_sets_dicts

			_rules_locks[base_url].release() # Release base URL lock for other threads to proceed with related URLs

	except KeyboardInterrupt as ex:
		Logger.spit("NDD thread shutting down...", caller_prefix = _caller_prefix) # ^C by user to terminate the process
	except Exception as ex:
		Logger.spit(stringify_exception(ex), warning = True, caller_prefix = _caller_prefix)
		with open("/tmp/ex_%s_%s.txt" % (_caller_prefix, uuid()), "w") as wfp:
			wfp.write(traceback.format_exc())
			wfp.write("\n")
		raise