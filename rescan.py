#!/usr/bin/python3

from xdriver import XDriver
from xdriver.rescan.Rescan import ReScan
from xdriver.xutils.Logger import Logger

from argparse import ArgumentParser

def get_args():
	parser = ArgumentParser()
	parser.add_argument("-i", "--input", action = "store", dest = "infile", help = "Input pickle file to read ReScan graph from")
	parser.add_argument("-H", "--headless", action = "store_true", dest = "headless", help = "Run headless browsers")
	parser.add_argument("-w", "--workers", action = "store", type = int, dest = "workers", default = 1, help = "Number of browser workers")
	parser.add_argument("-S", "--isd", action = "store_true", dest = "isd", help = "Enable inter-state dependency detection")
	parser.add_argument("-E", "--events", action = "store_true", dest = "events", help = "Enable event discovery")
	parser.add_argument("-C", "--clustering", action = "store_true", dest = "clustering", help = "Enable similar URL clustering")
	parser.add_argument("-A", "--auth", action = "store_true", dest = "auth", help = "Enable authentication helper")
	parser.add_argument("-L", "--authLax", action = "store_true", dest = "authLax", help = "Enable authentication helper lax mode, i.e. keep running even if a relogin is not possible.")
	parser.add_argument("-p", "--port", action = "store", type = int, dest = "port", default = 9191, help = "Port to listen on for incoming requests")
	parser.add_argument("-x", "--xport", action = "store", type = int, dest = "xport", help = "Custom proxy port for debugging")
	parser.add_argument("-o", "--logfile", action = "store", dest = "logfile", help = "Logfile")
	return parser.parse_args()

if __name__ == "__main__":
	args = get_args()
	# Logger.set_verbose(False)
	if args.logfile:
		Logger.set_logfile(args.logfile)

	rescan = ReScan(proxy_port = args.port, workers = args.workers, graph_file = args.infile, isd = args.isd, events = args.events, clustering = args.clustering, auth = args.auth, authLax = args.authLax, headless = args.headless, xproxy_port = args.xport)
	rescan.start()
