////// Kudos to SSOScan for the scripts! /////
//      _onTopLayer_func_script             //
//      _isChildElement_func_script        //
////////////////////////////////////////////

XDRIVER_LIB_SETUP = true;

// Override alerts/prompts/confirms. Simply push their text in a global array # -- ReScan
xdriver_captured_alerts = Array();
window.alert = function() {
    xdriver_captured_alerts.push(arguments[0].toString())
};
window.prompt = function() {
    xdriver_captured_alerts.push(arguments[0].toString())
};
window.confirm = function() {
    xdriver_captured_alerts.push(arguments[0].toString())
};

onTopLayer = function(ele){
    if (!ele) return false;
	var document = ele.ownerDocument;
	var inputWidth = ele.offsetWidth;
	var inputHeight = ele.offsetHeight;
	/*if (inputWidth >= screen.availWidth/3 || inputHeight >= screen.availHeight/4) return false;*/
	if (inputWidth <= 0 || inputHeight <= 0) return false;
    var position = ele.getClientRects()[0];
    var j = 0;
    var score = 0;
    position.top = position.top - window.pageYOffset;
	position.left = position.left - window.pageXOffset;
    var maxHeight = (document.documentElement.clientHeight - position.top > inputHeight)? inputHeight : document.documentElement.clientHeight - position.top;
	var maxWidth = (document.documentElement.clientWidth > inputWidth)? inputWidth : document.documentElement.clientWidth - position.left;
	for (j = 0; j < 10; j++){
    	score = isChildElement(ele,document.elementFromPoint(position.left+1+j*maxWidth/10, position.top+1+j*maxHeight/10)) ? score + 1 : score;
    }
    if (score >= 5) return true;
	else return false;
}

isChildElement = function(parent, child){
	if (child == null) return false;
	if (parent == child) return true;
	if (parent == null || typeof parent == 'undefined') return false;
	if (parent.children.length == 0) return false;
	var i = 0;
	for (i = 0; i < parent.children.length; i++){
        if (isChildElement(parent.children[i],child)) return true;
    }
	return false;
}

gPt=function(e){if(parentn=e.parentNode,null==parentn||'HTML'==e.tagName)return'';if(e===document.body||e===document.head)return'/'+e.tagName;for(var t=0,a=e.parentNode.childNodes,n=0;n<a.length;n++){var r=a[n];if(r===e)return gPt(e.parentNode)+'/'+e.tagName+'['+(t+1)+']';1===r.nodeType&&r.tagName===e.tagName&&t++}};

function getElementsByXPath(xpath, parent){let results = [];let query = document.evaluate(xpath,parent || document,null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);for (let i=0, length=query.snapshotLength; i<length; ++i) {results.push(query.snapshotItem(i));}return results;}

getDescendants = function(node, accum) {
    var i;
    accum = accum || [];
    for (i = 0; i < node.childNodes.length; i++) {
        accum.push(node.childNodes[i]);
        getDescendants(node.childNodes[i], accum);
    }
    return accum;
}

var get_dompath=function(e){
    if(parentn=e.parentNode,null==parentn||'HTML'==e.tagName)return'';if(e===document.body||e===document.head)return'/'+e.tagName;for(var t=0,a=e.parentNode.childNodes,n=0;n<a.length;n++){var r=a[n];if(r===e)return get_dompath(e.parentNode)+'/'+e.tagName+'['+(t+1)+']';1===r.nodeType&&r.tagName===e.tagName&&t++}
};

var getElementByXpath = function(path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

var get_element_src=function(el){
    return el.outerHTML.split(">")[0]+">";
}

var get_element_full_src=function(el){
    return el.outerHTML;
}

var get_password_forms=function(){
    var forms = document.getElementsByTagName("form");
    password_forms = []
    for (let form of forms){
        for (let child of getDescendants(form)){
            var nodetag = child.nodeName.toLowerCase();
            var etype = child.type;
            if (nodetag == "input" && etype == "password"){
                password_forms.push([form, finder(form)]);
            }
        }
    }
    return password_forms;
}

var get_local_storage = function(){
    try{
        var storage = {};
        for(var i = 0; i < localStorage.length; i++){
            var key = localStorage.key(i);
            storage[key] = localStorage[key];
        }
        return storage;
    } catch(error){
        return {};
    }
}

var get_session_storage = function(){
    try{
        var storage = {};
        for(var i = 0; i < sessionStorage.length; i++){
            var key = sessionStorage.key(i);
            storage[key] = sessionStorage[key];
        }
        return storage;
    } catch(error){
        return {};
    }
}

var get_attributes = function(el){
    var items = {};
    for (index = 0; index < arguments[0].attributes.length; ++index){
        items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value
    } return items;
}

// Returns a dictionary of the form {"login" : [...], "signup" : [...]}
// where each list contains the corresponding form WebElements
var get_account_forms = function(){
    SIGNUP = "sign([^0-9a-zA-Z]|\s)*up|regist(er|ration)?|(create|new)([^0-9a-zA-Z]|\s)*(new([^0-9a-zA-Z]|\s)*)?(acc(ount)?|us(e)?r|prof(ile)?)";
    LOGIN = "(log|sign)([^0-9a-zA-Z]|\s)*(in|on)|authenticat(e|ion)|/(my([^0-9a-zA-Z]|\s)*)?(user|account|profile|dashboard)"
    ret = {"login" : [], "signup" : []}
    var forms = document.getElementsByTagName("form");
    for (let form of forms) {
        if (!onTopLayer(form)){
            continue;
        }
        var inputs = 0;
        var hidden_inputs = 0;
        var passwords = 0;
        var hidden_passwords = 0;

        var checkboxes_and_radio = 0;
        var checkboxes = 0;
        var radios = 0;

        var signup_type_fields = 0

        var login_submit = false;
        var signup_submit = false;

        for (let child of getDescendants(form)){
            var nodetag = child.nodeName.toLowerCase();
            var etype = child.type;
            var el_src = get_element_full_src(child);
            if (nodetag == "button" || etype == "submit" || etype == "button" || etype == "image"){
                if(el_src.match(SIGNUP)) signup_submit = true;
                if(el_src.match(LOGIN)) login_submit = true;
            }
            if (nodetag != "input" && nodetag != "select" && nodetag != "textarea") continue;
            if (etype == "submit" || etype == "button" || etype == "hidden" || etype == "image" || etype == "reset") continue;
            if (etype == "password"){
                if(onTopLayer(child)) passwords += 1;
                else hidden_passwords += 1;
            }else if(etype == "checkbox" || etype == "radio"){ // count them as well, but not as inputs
                if(etype == "checkbox") checkboxes += 1;
                else radios += 1;
                checkboxes_and_radio += 1;
            }else{
                if(onTopLayer(child)) inputs +=1;
                else hidden_inputs += 1;
                if(etype == "tel" || etype == "date" || etype == "datetime-local" || etype == "file" || etype == "month" || etype == "number" || etype == "url" || etype == "week"){
                    signup_type_fields += 1;
                }
            }
        }
        var total_inputs = inputs + hidden_inputs;
        var total_passwds = passwords + hidden_passwords;
        var total_visible = inputs + passwords;
        var total_hidden = hidden_inputs + hidden_passwords;

        if (total_passwds == 0){
            continue; // irrelevant form
        }

        var signup = false;
        var login = false

        reason = null;

        form_src = get_element_src(form)
        if (total_passwds > 1) {signup = true; reason = 1;}
        else{ // Only one password field
            if(login_submit && !signup_submit) {login = true; reason = 2;} // Only one should match, otherwise, rely on structure
            else if(signup_submit && !login_submit) {signup = true; reason = 3;}
            else if(total_inputs == 1) {login = true; reason = 4;}
            else {
                if (signup_type_fields > 0) {signup = true; reason = 5;}
                else{
                    if (inputs > 1) {signup = true; reason = 6; }// more than one visible inputs
                    else if (inputs == 1) {login = true; reason = 7;}
                    else{ // no visible inputs
                        if (form_src.match(SIGNUP) != null) {signup = true; reason = 8;}
                        else if (form_src.match(LOGIN) != null) {login = true; reason = 9;}
                        else {signup = true; reason = 9;} // no luck with regexes
                    }
                }
            }
        }

        if(passwords == 1 && form.className == "xenForm"){ // freaking xenforms
            signup = false;
            login = true;
        }

        if(signup){
            ret["signup"].push([form, finder(form)])
            console.log(reason)
        }else if(login){
            ret["login"].push([form, finder(form)])
            console.log(reason)
        }
    }
    return ret;
};


const forbidden_suffixes = /\.(js|json|css|crt|mp3|wav|wma|ogg|mkv|zip|gz|tar|xz|rar|z|deb|bin|iso|csv|tsv|dat|txt|log|sql|xml|sql|mdb|apk|bat|bin|exe|jar|wsf|fnt|fon|otf|ttf|ai|bmp|gif|ico|jp(e)?g|png|ps|psd|svg|tif|tiff|cer|rss|key|odp|pps|ppt|pptx|c|class|cpp|cs|h|java|sh|swift|vb|odf|xlr|xls|xlsx|bak|cab|cfg|cpl|cur|dll|dmp|drv|icns|ini|lnk|msi|sys|tmp|3g2|3gp|avi|flv|h264|m4v|mov|mp4|mp(e)?g|rm|swf|vob|wmv|doc(x)?|odt|pdf|rtf|tex|txt|wks|wps|wpd|woff|eot|xap)$/g;
var get_internal_links = function(remove_query, strip_files){
    ret = []
    for(let a of document.getElementsByTagName('a')){
        if(a.host != window.location.host) continue; // Skip links pointing to a different domain
        href = a.href
        if (href.indexOf("?") > 0) no_query_href = href.substring(0, a.href.indexOf("?"));
        else no_query_href = href;
        
        if(remove_query) href = no_query_href;
        if(strip_files && no_query_href.match(forbidden_suffixes)) continue; // Skip files we want to block
        ret.push(href)
    }
    return ret;
}

var get_form_descriptions = function(){
    ret = []
    tags = ["INPUT", "SELECT", "TEXTAREA"]
    for(let f of document.getElementsByTagName("form")){
        fields = []
        for(let element of f.elements){
            etag = element.tagName
            if(!tags.includes(etag)) continue;
            if(!element.hasAttribute("name") && !element.hasAttribute("id")) continue;
            ename = element.name || element.id
            fields.push(ename)
        }
        if(f.action && typeof f.action == "string") var action = f.action;
        else var action = f.getAttribute("action") || location.href;
        var action = action.split("#")[0]; // Make sure to remove fragment, as it is not relevant
        var method = f.getAttribute("method") || f.method;
        ret.push([action, method, fields.join("&"), finder(f)])
    }
    return ret
}

var get_iframes_src = function(){
    ret = []
    iframes = Array.from(document.getElementsByTagName("iframe"));
    frames = Array.from(document.getElementsByTagName("frame"));
    for(let iframe of iframes.concat(frames)){
        try{var doc = iframe.contentDocument || iframe.contentWindow.document} // Hack to check if iframe is cross-origin. If yes, don't include it
        catch(err) {continue}
        ret.push([iframe.src, finder(iframe)])
    }
    return ret;
}

var rand_int = function(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var is_displayed = function(element){
    return !(element.type == "hidden" || window.getComputedStyle(element).display == "none")
}

var xdr_fill_all_inputs = function(data){ // # -- ReScan
    for(let element of document.querySelectorAll("input, textarea")){ // Iterate over all inputs of the page
        if(element.type && element.type == "submit" || element.type == "reset" || element.type == 'button' || element.type == "hidden") continue; // Skip inputs that are not supposed to be filled
        if(data && element.name in data){
            element.value = data[element.name]; // Use given value
        }else{
            if(!data) element.value = xdr_fill_input(element) // Generate suitable value, if no specific values have been given
        }
    }

    for(let element of document.querySelectorAll("select")){
        if(data && element.name in data){
            var option = document.createElement("option");
            option.text = data[element.name];
            option.value = data[element.name];
            element.add(option);
            option.selected = true;
        }else{
            if(!data) xdr_fill_select(element);
        }

    }
}

var xdr_fill_input = function(element){ // # -- ReScan
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var value = "";
    var length = element.maxLength != -1 ? element.maxLength : 8;
    for(var i = 0; i < length; i++){
        value += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return value;
}

var xdr_fill_select = function(element){ // # -- ReScan
    var options = element.options;
    var selected = Math.floor(Math.random() * options.length)
    options[selected].selected = true;
}

var create_and_fill_form = function(css_selector, data, action, method, filenames, create){ // create_and_fill_form
    var submitted_values = Array();
    var form = document.querySelector(css_selector);
    var submit_elements = Array();
    var default_filename = "dummy.txt";

    if(!form){ // Create new one
        if(!create) return submitted_values; // tmp
        console.log("New form..")
        var form = document.createElement("form");
        for(var d in data){
            var input = document.createElement("input");
            input.setAttribute("name", d);
            input.value = data[d];
            submitted_values.push([d, "", data[d]])
            form.appendChild(input);
        }
        document.getElementsByTagName('body')[0].appendChild(form);

    }else{
        for(let element of form.elements){
            tag = element.tagName.toLowerCase();
            type = element.type;
            if(type == "submit" && element.outerHTML.search(/cancel|reset/g) == -1) submit_elements.push(element);
            if(element.name in data){
                var element_name = element.name;
                var element_value = element.value;
                var data_value = data[element_name];
                var filename = filenames[element_name] || data_value;  // Use correct filename, if provided for this param, or default to the element's value (for scanners that don't fuzz filenames)
                if((element_name.search(/token|csrf|nonce|key|form_id/g) != -1) || (!data_value)) continue; // Skip nonce-like parameters or empty scanner values
                if(tag == "input"){
                    element.maxLength = 999; // make sure the value will fit
                    if (type != "file") element.value = data_value;
                    if (type == "file") fill_file_input(element, data_value, filename);
                    if (type == "reset") continue;
                }else if(tag == "textarea"){
                    element.maxLength = 999; // make sure the value will fit
                    element.value = data_value;
                }else if(tag == "select"){ // For <select> elements we need to create and append our own custom <option> carrying the value
                    var option = document.createElement("option");
                    option.text = data_value;
                    option.value = data_value;
                    element.add(option);
                    element.value = data_value;
                }
                if(type != "submit") submitted_values.push([element_name, element_value, data_value]); // exclude submit buttons
            }
        }
    }
    form.setAttribute("action", action); // Overwrite existing or set new form's action & method
    form.setAttribute("method", method);
    /*** tmp ***/
    if(submit_elements.length > 0){
        submit_elements[0].click()
        return submitted_values;
    }
    /*** /tmp ***/
    if(submit_elements.length == 1){ // If we locate exactly one submit button, click it
        submit_elements[0].click()
        console.log("clicked it")
    }
    else{ // Otherwise (no submit buttons or more than 1), trigger the form's submit()
        HTMLFormElement.prototype.submit.call(form); // Get prototype's submit func, since it's quite common for input elements to be named 'submit' and overwrite it
        console.log("submitted")
    }
    return submitted_values; // Return all values in the form [old, new] -- empty if the form was created dynamically
}

var xdr_submit_form = function(form){ // # -- ReScan
    var submit_elements = Array();
    for(let element of form.elements){
        type = element.type;
        if(type == "submit") submit_elements.push(element);
    }
    if(submit_elements.length == 1){ // If we locate exactly one submit button, click it
        submit_elements[0].click()
        console.log("clicked it")
    }
    else{ // Otherwise (no submit buttons or more than 1), trigger the form's submit()
        HTMLFormElement.prototype.submit.call(form); // Get prototype's submit func, since it's quite common for input elements to be named 'submit' and overwrite it
        console.log("submitted")
    }
}

var fill_file_input = function(element, data, filename){ // # -- ReScan
    const data_transfer = new DataTransfer();
    data_transfer.items.add(new File([data], filename));
    element.files = data_transfer.files;
}

var is_confirmation_form = function(css_selector){
    var form = document.querySelector(css_selector);
    if(!form){
        return false;
    }
    var hidden = 0;
    for(let element of form.elements){
        if(!is_displayed(element)){
            hidden++;
        }
    }
    return hidden == form.elements.length-1; // Account for displayed submit buttons
}

// Kudos to https://medv.io/finder/finder.js for the cool (and unique!) CSS selector generator
var Limit;(function(a){a[a.All=0]="All",a[a.Two=1]="Two",a[a.One=2]="One"})(Limit||(Limit={}));let config,rootDocument; function finder1(a,b){if(a.nodeType!==Node.ELEMENT_NODE)throw new Error(`Can't generate CSS selector for non-element node type.`);if("html"===a.tagName.toLowerCase())return"html";const c={root:document.body,idName:()=>!0,className:()=>!0,tagName:()=>!0,attr:()=>!1,seedMinLength:1,optimizedMinLength:2,threshold:1e3,maxNumberOfTries:1e4};config=Object.assign(Object.assign({},c),b),rootDocument=findRootDocument(config.root,c);let d=bottomUpSearch(a,Limit.All,()=>bottomUpSearch(a,Limit.Two,()=>bottomUpSearch(a,Limit.One)));if(d){const b=sort(optimize(d,a));return 0<b.length&&(d=b[0]),selector(d)}throw new Error(`Selector was not found.`)}function findRootDocument(a,b){return a.nodeType===Node.DOCUMENT_NODE?a:a===b.root?a.ownerDocument:a}function bottomUpSearch(a,b,c){let d=null,e=[],f=a,g=0;for(;f&&f!==config.root.parentElement;){let a=maybe(finder_id(f))||maybe(...attr(f))||maybe(...classNames(f))||maybe(tagName(f))||[any()];const h=index(f);if(b===Limit.All)h&&(a=a.concat(a.filter(dispensableNth).map(a=>nthChild(a,h))));else if(b===Limit.Two)a=a.slice(0,1),h&&(a=a.concat(a.filter(dispensableNth).map(a=>nthChild(a,h))));else if(b===Limit.One){const[b]=a=a.slice(0,1);h&&dispensableNth(b)&&(a=[nthChild(b,h)])}for(let b of a)b.level=g;if(e.push(a),e.length>=config.seedMinLength&&(d=findUniquePath(e,c),d))break;f=f.parentElement,g++}return d||(d=findUniquePath(e,c)),d}function findUniquePath(a,b){const c=sort(combinations(a));if(c.length>config.threshold)return b?b():null;for(let d of c)if(unique(d))return d;return null}function selector(a){let b=a[0],c=b.name;for(let d=1;d<a.length;d++){const e=a[d].level||0;c=b.level===e-1?`${a[d].name} > ${c}`:`${a[d].name} ${c}`,b=a[d]}return c}function penalty(a){return a.map(a=>a.penalty).reduce((a,b)=>a+b,0)}function unique(a){switch(rootDocument.querySelectorAll(selector(a)).length){case 0:throw new Error(`Can't select any node with this selector: ${selector(a)}`);case 1:return!0;default:return!1;}}function finder_id(a){const b=a.getAttribute("id");return b&&config.idName(b)?{name:"#"+cssesc(b,{isIdentifier:!0}),penalty:0}:null}function attr(a){const b=Array.from(a.attributes).filter(a=>config.attr(a.name,a.value));return b.map(a=>({name:"["+cssesc(a.name,{isIdentifier:!0})+"=\""+cssesc(a.value)+"\"]",penalty:.5}))}function classNames(a){const b=Array.from(a.classList).filter(config.className);return b.map(a=>({name:"."+cssesc(a,{isIdentifier:!0}),penalty:1}))}function tagName(a){const b=a.tagName.toLowerCase();return config.tagName(b)?{name:b,penalty:2}:null}function any(){return{name:"*",penalty:3}}function index(a){const b=a.parentNode;if(!b)return null;let c=b.firstChild;if(!c)return null;let d=0;for(;c&&(c.nodeType===Node.ELEMENT_NODE&&d++,c!==a);)c=c.nextSibling;return d}function nthChild(a,b){return{name:a.name+`:nth-child(${b})`,penalty:a.penalty+1}}function dispensableNth(a){return"html"!==a.name&&!a.name.startsWith("#")}function maybe(...a){const b=a.filter(notEmpty);return 0<b.length?b:null}function notEmpty(a){return null!==a&&a!==void 0}function*combinations(a,b=[]){if(0<a.length)for(let c of a[0])yield*combinations(a.slice(1,a.length),b.concat(c));else yield b}function sort(a){return Array.from(a).sort((c,a)=>penalty(c)-penalty(a))}function*optimize(a,b,c={counter:0,visited:new Map}){if(2<a.length&&a.length>config.optimizedMinLength)for(let d=1;d<a.length-1;d++){if(c.counter>config.maxNumberOfTries)return;c.counter+=1;const e=[...a];e.splice(d,1);const f=selector(e);if(c.visited.has(f))return;unique(e)&&same(e,b)&&(yield e,c.visited.set(f,!0),yield*optimize(e,b,c))}}function same(a,b){return rootDocument.querySelector(selector(a))===b}const regexAnySingleEscape=/[ -,\.\/:-@\[-\^`\{-~]/,regexSingleEscape=/[ -,\.\/:-@\[\]\^`\{-~]/,regexExcessiveSpaces=/(^|\\+)?(\\[A-F0-9]{1,6})\x20(?![a-fA-F0-9\x20])/g,defaultOptions={escapeEverything:!1,isIdentifier:!1,quotes:"single",wrap:!1};function cssesc(a,b={}){const c=Object.assign(Object.assign({},defaultOptions),b);"single"!=c.quotes&&"double"!=c.quotes&&(c.quotes="single");const d="double"==c.quotes?"\"":"'",e=c.isIdentifier,f=a.charAt(0);let g="",h=0;for(const f=a.length;h<f;){const b=a.charAt(h++);let i,j=b.charCodeAt(0);if(32>j||126<j){if(55296<=j&&56319>=j&&h<f){const b=a.charCodeAt(h++);56320==(64512&b)?j=((1023&j)<<10)+(1023&b)+65536:h--}i="\\"+j.toString(16).toUpperCase()+" "}else i=c.escapeEverything?regexAnySingleEscape.test(b)?"\\"+b:"\\"+j.toString(16).toUpperCase()+" ":/[\t\n\f\r\x0B]/.test(b)?"\\"+j.toString(16).toUpperCase()+" ":"\\"==b||!e&&("\""==b&&d==b||"'"==b&&d==b)||e&&regexSingleEscape.test(b)?"\\"+b:b;g+=i}return e&&(/^-[-\d]/.test(g)?g="\\-"+g.slice(1):/\d/.test(f)&&(g="\\3"+f+" "+g.slice(1))),g=g.replace(regexExcessiveSpaces,function(a,b,c){return b&&b.length%2?a:(b||"")+c}),!e&&c.wrap?d+g+d:g}
var finder = function(el){ // # -- ReScan
    try{
        return finder1(el);
    } catch(error){
        // Kudos: https://stackoverflow.com/questions/4588119/get-elements-css-selector-when-it-doesnt-have-an-id -- Fallback to finder, since in certain cases it will throw a "Cannot read property 'name' of undefined"
        // console.log(error)
        var names = [];
        while (el.parentNode){
            if (el.id){
              names.unshift('#'+el.id);
              break;
            }else{
              if (el==el.ownerDocument.documentElement) names.unshift(el.tagName);
              else{
                for (var c=1,e=el;e.previousElementSibling;e=e.previousElementSibling,c++);
                names.unshift(el.tagName+":nth-child("+c+")");
              }
              el=el.parentNode;
            }
        }
        return names.join(" > ");
    }

}

console.log("XDriver lib is setup!");