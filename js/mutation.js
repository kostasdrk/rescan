// # -- ReScan
mutation_body = document.getElementsByTagName('body')[0];
mutation_config = { attributes: true, childList: true, subtree: true }; // Need to get all DOM changes in the body, as well as attribute changes

recordedMutations = Array()
attributeMutations = Array()

xdriver_mutation_callback = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        if (mutation.type === "childList") {
        	for(let node of mutation.addedNodes){
                if(!node.tagName) continue;
	        	// tag = node.tagName.toLowerCase();
	        	// if(tag != "form" && tag != "a" && tag != "iframe" && tag != "frame") continue; // Only keep DOM changes we are interested in
                recordedMutations.push({"xpath" : "/"+get_dompath(node), "str" : node.outerHTML}) // Need both the element handle to directly interact with the element and also its XPath so we can re-fetch it if needed
        	}
        }else if(mutation.type === "attributes"){
            var node = mutation.target;
            var attribute_name = mutation.attributeName;
            attributeMutations.push({"xpath" : "/"+get_dompath(node), "str" : node.outerHTML, "attribute" : attribute_name, "old" : mutation.oldValue, "new" : node[attribute_name]})
        }
    }
};

if(mutation_body){ // In case there is no body
    const observer = new MutationObserver(xdriver_mutation_callback);
    observer.observe(mutation_body, mutation_config);
    console.log("Mutation observer is setup!")
}