#!/usr/bin/python3

from apted import APTED
from apted.helpers import Tree
from bs4 import BeautifulSoup

''' Custom tree class implementing the fine-grained pruning technique. # -- ReScan
'''
class Node():
	_ignored_labels = set(["a", "br", "p", "span", "div", "b", "i", "u", "strong",
					   "link", "meta", "style", "img", "svg", "g", "math", "audio", "track", "video", "path", "picture", "source", # Head/meta + media
					   "ul", "ol", "li", "h1", "h2", "h3", "h4", "h5", "h6", "blockquote", "em", "dd", "dl", "dt", "figcaption", "figure", "hr", "pre", # Text
					   "abbr", "bdi", "bdo", "cite", "code", "dfn", "mark", "q", "s", "samp", "sub", "sup", "time", "var", "wbr",
					   "article", "address", "footer", "header", "del", "ins",
					   "caption", "col", "colgroup", "table", "tbody", "td", "tfoot", "th", "thead", "tr",
					   "acronym", "center", "content", "big", "font", "hgroup", "marquee", "menuitem", "nobr", "plaintext", "strike", "tt", "xmp"]) # Deprecated

	def __init__(self, label, children, parent):
		self._label = label
		self._children = children
		self._parent = parent
		self._size = 1

	def get_label(self): return self._label
	def get_children(self): return self._children
	def get_parent(self): return self._parent
	def get_size(self): return self._size

	def set_size(self, size): self._size = size

	def add_child(self, node): self._children.append(node)
	def remove_child(self, node):
		self._children.remove(node)
		self.set_size(self.get_size() - node.get_size())
		parent = self._parent
		while parent is not None: # Propagate size change to parents
			parent.set_size(parent.get_size() - node.get_size())
			parent = parent.get_parent()

	def remove_ignored_nodes(self):
		removed = []
		for child_node in self._children:
			if child_node.remove_ignored_nodes():
				removed.append(child_node)

		for child_node in removed: self.remove_child(child_node)
		
		if not self._children and self._label in Node._ignored_labels: return True
		return False

class NDD(): # -- ReScan
	@classmethod
	def _create_tree(cls, source, labelling = lambda node: node.name, smart_pruning = False):
		def preorder(tree, bs_node):
			new_node = Node(labelling(bs_node).lower(), [], tree)
			tree.add_child(new_node)
			for bs_child_node in bs_node.find_all(recursive = False):
				preorder(new_node, bs_child_node)
			new_node.get_parent().set_size(new_node.get_parent().get_size() + new_node.get_size())
			return tree

		soup = BeautifulSoup(source, "html5lib")
		if not soup: return None
		tree = preorder(Node("ROOT", [], None), soup)
		if smart_pruning: tree.remove_ignored_nodes()
		return tree

	@classmethod
	def _bracket_notation_from_source(cls, source, labelling, smart_pruning = False): # By default do *not* prune the DOMs and label the tree nodes using each element's tag name
		def preorder(bracket_tree, node):
			bracket_tree += "{%s" % node.get_label()
			for child_node in node.get_children(): bracket_tree = preorder(bracket_tree, child_node)
			bracket_tree += "}"
			return bracket_tree

		tree = NDD._create_tree(source, labelling = labelling, smart_pruning = smart_pruning)
		return preorder("", tree), tree.get_size()
			
	@classmethod
	def compare(cls, source1, source2, labelling = lambda node: node.name, smart_pruning = False):
		_apted_tree_1, _size_1 = NDD._bracket_notation_from_source(source1, labelling, smart_pruning = smart_pruning)
		_apted_tree_2, _size_2 =  NDD._bracket_notation_from_source(source2, labelling, smart_pruning = smart_pruning)

		apted = APTED(Tree.from_text(_apted_tree_1), Tree.from_text(_apted_tree_2))
		return apted.compute_edit_distance() / (_size_1 + _size_2)