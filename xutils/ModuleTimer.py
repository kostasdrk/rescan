#!/usr/bin/python3

from .Logger import Logger
from time import time


class Timer():

	_caller_prefix = "ModuleTimer"

	def __init__(self):
		self._start = time()
		self._total = None

	def end(self):
		self._total = time() - self._start
		return self._total

	def get_time(self):
		return self._total if self._total else time() - self._start