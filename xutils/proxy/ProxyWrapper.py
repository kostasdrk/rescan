#!/usr/bin/python3

from xdriver.xutils.Logger import Logger
from xdriver.xutils.Exceptions import *
from xdriver.xutils.URLUtils import URLUtils
from xdriver.xutils.Misc import uuid, get_install_dir, pdump

import os
import subprocess
import signal
import socket

from random import choice
from time import sleep, time
from copy import deepcopy
import pickle as cp
from _pickle import UnpicklingError

class ProxyWrapper():

	_caller_prefix = "ProxyWrapper"
	
	_abs_path = os.path.dirname(os.path.abspath(__file__))

	_port_range = list(range(9000, 60000))

	_proxy_intercomm_dir = "/tmp"
	_proxy_port = None # Will be set when bootstrapped

	_mitm_bin = os.path.join(_abs_path, "mitm/mitmdump")
	_mitm_script = os.path.join(_abs_path, "MITMProxy.py")

	_base_config = {
		"force_cookies" : { # Useful to force cookies on the wire. This can be done to avoid sending out persistent cookies (even though they will be present in the browser)
			# Each rule should be a dict of type {'domain' : 'example.com', 'cookies' : 'A=1; B=2'}. The `domain` field follows the cookie-domain notation,
			# e.g. 'example.com' will send the cookies only to that domain (host-only), while '.example.com' will also send the cookies to any subdomains of example.com
			# The `cookies` field should be a valid cookie string that will be set in each HTTP request's "Cookie" header towards `domain`
			"enabled" : False,
			"rules" : []
		},
		"redirection_mode" : { # Extract redirection flow from a given starting URL (e.g. http://example.com -> https://example.com -> https://www.example.com). Headers included
			"enabled" : False,
			"url" : None, # Starting URL
			"flow" : None, # Results. Will be filled by the mitmdump subprocess
			"strict_query" : None
		},
		"spoofing_mode" : { # Spoof any header. If the header exists, it will be replaced with the spoofed one
			"enabled" : False,
			"headers" : {}, # Dictionary of the form: {"origin" : "abc.com", "user-agent" : "whatever"}. These will be spoofed for all requests
			"domain_headers" : [] # Domain-specific rules. Similar as the `force_cookies` mode rules. E.g. {"domain" : "abc.com", "headers" : {"x-header" : "blah"}"}
		},
		"tamper_mode" : { # Request tampering mode
			"enabled" : False,
			"url" : None, # Capture the request to that URL,
			"update" : False, # The given payload will be used to augment existing request data -- common parameters will be overwritten. Has precedence over other modes
			"strict" : False, # The given payload will overwrite any existing request data. Has precedence over the `conservative` mode
			"conservative" : False, # The given payload will only overwrite not existing values (e.g. will not overwrite CSRF tokens)
			"tamper" : {"method" : None, "headers" : {}, "payload" : {}} # And modify it accordingly
		},
		"capture_mode" : { # While enabled, capture all seen requests and responses. If enabled with other modes, e.g. spoofing or tamper, it will include the tampered requests # -- ReScan
			"enabled" : False,
			"http_messages" : []
		},
		"rescan_mode" : { # Special proxy case, where it's setup **before** the browser as an endpoint for 3rd party web scanners and mirrors their behavior (reqs/resps) through the ReScan module # -- ReScan
			"enabled" : False,
			"requests" : [] # Will be filled by Intercepting proxy subprocess
		}
	}

	@classmethod
	def _get_free_port(cls):
		while True:
			chosen_port = choice(ProxyWrapper._port_range)
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			try:
				s.bind(('localhost', chosen_port))
				return chosen_port, s
			except (OSError, socket.error) as e:
				if e.errno == 98: # Port is used
					continue
				raise # Raise anything else
		raise Exception("Not available ports in range [%s,%s)" % (ProxyWrapper._port_range[0], ProxyWrapper._port_range[-1]))

	def __init__(self, port = None, strip_media = False, mitm_script = None, tor = {"enabled" : False}, custom_proxy = {"enabled" : False}, proxy_config = None):
		self._proxy_intercomm_file = os.path.join(ProxyWrapper._abs_path, ProxyWrapper._proxy_intercomm_dir, "xdriver_proxy-%s" % uuid()) # Unique ID used for inter-process comm between ProxyWrapper and the mitmproxy subprocess

		self._proxy_port, sock = (port, None) if port else ProxyWrapper._get_free_port()
		Logger.spit("Reserved port %s" % self._proxy_port, caller_prefix = ProxyWrapper._caller_prefix)

		self.dump_config(config = ProxyWrapper._base_config if not proxy_config else proxy_config)
		self._proxy_config = self.load_config()

		if self._proxy_config["rescan_mode"]["enabled"]: # If rescan mode was enabled for this instance, disable it in the base config so other instances can be booted as well (e.g. XDriver's internal proxy) # -- ReScan
			ProxyWrapper._base_config["rescan_mode"]["enabled"] = False

		# Close socket holding the port directly before launching the proxy
		if sock:
			sock.close()

		mitm_script_args = [ProxyWrapper._mitm_bin, "-s", "%s %s %s %s" % (ProxyWrapper._mitm_script if not mitm_script else mitm_script, self._proxy_intercomm_file, int(strip_media), get_install_dir()), "-p", str(self._proxy_port), "--insecure", "--quiet"]
		if custom_proxy["enabled"]: mitm_script_args += ["-U", "%s://%s:%s" % (custom_proxy["scheme"], custom_proxy["host"], custom_proxy["port"])] # Route everything through user defined proxy
		self._proc = subprocess.Popen(mitm_script_args)

		sleep(2)
		retcode = self._proc.poll() # Just to make sure
		if not self._has_booted(): raise FrameworkException("Proxy subprocess did not boot in time")

	def _has_booted(self, timeout = 30):
		from requests import get
		start = time()
		r = None
		proxy_dict = {scheme : "%s://127.0.0.1:%s" % (scheme, self._proxy_port) for scheme in ["http", "https"]}
		while time() - start < timeout and not r:
			# print(1)
			try: r = get("http://google.com", headers = {"XDRIVER-HEARTBEAT" : "check"}, proxies = proxy_dict)
			except Exception as ex: pass
		return bool(r)

	def set_port(self, port):
		self._proxy_port = port
	def get_port(self):
		return self._proxy_port

	def get_intercomm_file(self):
		return self._proxy_intercomm_file

	def load_config(self):
		while True:
			try:
				with open(self._proxy_intercomm_file, 'rb') as fp: return cp.load(fp)
			except (EOFError, UnpicklingError): continue
			except Exception as ex:
				Logger.spit(stringify_exception(ex), warning = True, caller_prefix = ProxyWrapper._caller_prefix)
	def dump_config(self, config = None):
		# If proxy settings/modes have been passed before bootstrap
		if self._proxy_intercomm_file:
			with open(self._proxy_intercomm_file, 'wb') as wfp:
				cp.dump(config if config else self._proxy_config, wfp)
	
	def force_cookies(self, rules = {}, enabled = True):
		self._proxy_config["force_cookies"]["enabled"] = enabled
		self._proxy_config["force_cookies"]["rules"] = rules
		self.dump_config()

	''' If `strict_query` is False we don't require exact query value matches. Useful to avoid nonce tokens.
		Also, if `html = True`, store the landing page's source. Useful if we want the *pre-rendered* page source. '''
	def redirection_mode(self, url, html = False, strict_query = True):
		self._proxy_config["redirection_mode"]["enabled"] = True
		self._proxy_config["redirection_mode"]["url"] = URLUtils.normalize_url(url if url.startswith("http://") or url.startswith("https://") else "%s%s" % ("http://", url))
		self._proxy_config["redirection_mode"]["flow"] = []
		self._proxy_config["redirection_mode"]["domain"] = URLUtils.get_domain(url)
		self._proxy_config["redirection_mode"]["html"] = html
		self._proxy_config["redirection_mode"]["strict_query"] = strict_query
		self.dump_config()

	def get_redirection_flow(self, timeout = 1): # Consume the stored redirection flow
		end = time() + timeout
		sleep(0.5) # Make sure the request left, i.e. if we try to capture the redirection flow for something that *won't* trigger a request, it doesn't make sense to wait for it
		while time() < end or not timeout:
			config = self.load_config()
			flow = config["redirection_mode"]["flow"]
			if not flow: break # No request ever left the browser; abort (see comment above)
			if not timeout: break
			# if not flow: continue
			if "status_code" not in flow[-1]: continue # Partial flow captured, need to wait more (e.g. due to slow app response times)
			if "location" in flow[-1]["response_headers"]: continue # More redirections to capture, wait
			break

		self._proxy_config["redirection_mode"]["enabled"] = False
		self.dump_config()
		return flow

	def spoof_headers(self, headers = None, domain_headers = None, reset = False, enabled = True):
		headers = headers if headers else {}
		domain_headers = domain_headers if domain_headers else []
		self._proxy_config["spoofing_mode"]["enabled"] = enabled
		if reset:
			self._proxy_config["spoofing_mode"]["headers"] = headers
			self._proxy_config["spoofing_mode"]["domain_headers"] = domain_headers
		else:
			self._proxy_config["spoofing_mode"]["headers"].update(headers)
			self._proxy_config["spoofing_mode"]["domain_headers"].extend(domain_headers)
		self.dump_config()

	def tamper_mode(self, url, tamper = None, enabled = True, update = False, strict = False, conservative = False): # -- ReScan
		tamper = tamper if tamper else {}
		self._proxy_config["tamper_mode"]["enabled"] = enabled
		self._proxy_config["tamper_mode"]["url"] = URLUtils.normalize_url(url)
		self._proxy_config["tamper_mode"]["update"] = True if not strict and not conservative else update
		self._proxy_config["tamper_mode"]["strict"] = False if update else strict
		self._proxy_config["tamper_mode"]["conservative"] = False if update or strict else conservative

		self._proxy_config["tamper_mode"]["tamper"]["method"] = tamper.get("method", None)
		self._proxy_config["tamper_mode"]["tamper"]["headers"] = tamper.get("headers", {})
		self._proxy_config["tamper_mode"]["tamper"]["payload"] = tamper.get("payload", {})
		self.dump_config()

	def capture_mode(self, enabled = True): # -- ReScan
		self._proxy_config["capture_mode"]["enabled"] = enabled
		self._proxy_config["capture_mode"]["http_messages"] = []
		self.dump_config()
	def get_captured_messages(self): # -- ReScan
		msgs = self.load_config()["capture_mode"]["http_messages"]
		self._proxy_config["capture_mode"]["enabled"] = False # Auto-disable mode and reset http message list
		self._proxy_config["capture_mode"]["http_messages"] = []
		self.dump_config()
		return msgs

	# -- ReScan
	@classmethod
	def rescan_mode(cls):
		ProxyWrapper._base_config["rescan_mode"]["enabled"] = True

	def remove_config(self):
		os.remove(self._proxy_intercomm_file)

	def shutdown(self):
		self._proc.terminate()
		self.remove_config()