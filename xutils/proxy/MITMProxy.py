#!/usr/bin/python3

import os
import sys
sys.path.extend([
	os.path.join(os.path.expanduser("~"), ".local/lib/python3.8/site-packages"),
	'/usr/local/lib/python3.8/dist-packages',
    '/usr/lib/python3/dist-packages',
    '/usr/lib/python38.zip',
    '/usr/lib/python3.8',
    '/usr/lib/python3.8/plat-x86_64-linux-gnu',
    '/usr/lib/python3.8/lib-dynload'])

# from torrequest import TorRequest
from mitmproxy.script import concurrent
from mitmproxy import http
from time import time
import re
import _pickle

from threading import Lock

def load(filename):
	with open(filename, 'rb') as fp:
		return _pickle.load(fp)
def dump(obj, filename):
	with open(filename, 'wb') as wfp:
		_pickle.dump(obj, wfp, protocol = 2)

if len(sys.argv) < 1:
	print("[MiTM][!] Need proxy intercomm filename")
	sys.exit(1)

_install_dir = sys.argv[3] # xdriver's install directory
sys.path.extend([_install_dir]) # xdriver's imports below this line
from xdriver.xutils.URLUtils import URLUtils

from urllib.parse import urlparse, quote

_proxy_intercomm_file = sys.argv[1]
_strip_media = int(sys.argv[2])

_forbidden_suffixes = r"\.(crt|mp3|wav|wma|ogg|mkv|zip|gz|tar|xz|rar|z|deb|bin|iso|csv|tsv|dat|txt|log|sql|xml|sql|mdb|apk|bat|bin|exe|jar|wsf|fnt|fon|otf|ttf|ai|bmp|gif|ico|jp(e)?g|png|ps|psd|svg|tif|tiff|cer|rss|key|odp|pps|ppt|pptx|c|class|cpp|cs|h|java|sh|swift|vb|odf|xlr|xls|xlsx|bak|cab|cfg|cpl|cur|dll|dmp|drv|icns|ini|lnk|msi|sys|tmp|3g2|3gp|avi|flv|h264|m4v|mov|mp4|mp(e)?g|rm|swf|vob|wmv|doc(x)?|odt|pdf|rtf|tex|txt|wks|wps|wpd|woff|eot|xap)$"

_modes = ["force_cookies", "redirection_mode", "spoofing_mode", "tamper_mode", "capture_mode", "rescan_mode"]

_evasion_payload = None

_seen_urls = set()

_capture_mode_lock = Lock()

def fetch_config(max_retries = 200): # Keep loading until successful, or the max retries are exceeded
	retries = 0
	while retries < max_retries:
		try:
			config = load(_proxy_intercomm_file)
			integrity = True
			for mode in _modes:
				if mode not in config:
					print("[MiTM] Missing mode: %s" % mode)
					integrity = False
					break
				if "enabled" not in config[mode]:
					integrity = False
					print("[MiTM] Missing enabled in: %s" % mode)
					break
			if not integrity: raise Exception("[MiTM] Pickle integrity broken...")
			return config
		except (EOFError): continue
		except Exception as ex:
			retries += 1
			if retries >= max_retries: raise
			continue

def dump_config(config):
	dump(config, _proxy_intercomm_file)

''' Given a URL, transform its query values to '.*' regex wildcards
'''
def get_wildcard_query(url):
	parsed = urlparse(url)
	query = ""
	for param in parsed.query.split("&"):
		tok = param.split("=")
		name, value = tok if len(tok) > 1 else (tok[0], "")
		query += "%s=%s&" % (name, ".*")
	query = query[:-1] # Skip last '&'
	return re.escape("%s://%s%s%s" % (parsed.scheme, parsed.netloc, parsed.path, "?%s" % query if query else "")).replace("\\.\\*", ".*") # escape regex special chars, except '.*'

''' Capture each request and operate according to the current mode of operation.
	It is mandatory that FOR EACH request the config is loaded, so we are in sync with the framework.
	Sounds expensive, but I/O to a small file.
	Cookie-mode: Simply set the req's Cookie header to the config's current cookie string, for the specified domain
	Redirection mode: Given a starting URL, extract its redirection flow along with each pair of request/response headers '''

@concurrent
def request(flow):
	if "XDRIVER-HEARTBEAT" in flow.request.headers: return  # Skip boot/heartbeat requests from XDriver

	request_url = URLUtils.normalize_url(flow.request.url)
	request_payload = flow.request.urlencoded_form
	if flow.request.multipart_form: request_payload = flow.request.multipart_form
	
	crafted_response = None
	if _strip_media and re.search(_forbidden_suffixes, request_url, re.IGNORECASE):
		crafted_response = http.HTTPResponse.make(status_code = 404, headers = {})
		crafted_response.text = ""

	config = fetch_config()

	if config["force_cookies"]["enabled"] and not crafted_response:
		req_domain = urlparse(request_url).netloc
		rules = config["force_cookies"]["rules"]
		
		cookie_header = ""
		for rule in rules:
			if rule["domain"] == req_domain or (rule["domain"].startswith(".") and ((req_domain.endswith(rule["domain"]) or req_domain == rule["domain"][1:]))):
				cookie_header += "%s%s" % (";" if cookie_header else "", str(rule["cookies"]))

		if cookie_header:
			flow.request.headers["Cookie"] = cookie_header

	if config["spoofing_mode"]["enabled"] and not crafted_response:
		req_domain = URLUtils.get_main_domain(request_url, suffix = True, port = True)
		for header, value in config["spoofing_mode"]["headers"].items(): flow.request.headers[header] = value

		for domain_rule in config["spoofing_mode"]["domain_headers"]:
			domain = domain_rule["domain"]
			headers = domain_rule["headers"]

			if domain == req_domain or (domain.startswith(".") and ((req_domain.endswith(domain) or req_domain == domain[1:]))):
				for header, value in headers.items():
					flow.request.headers[header] = value

	if config["tamper_mode"]["enabled"] and not crafted_response: # -- ReScan
		url = config["tamper_mode"]["url"]
		if re.match("%s/?$" % re.escape(url), request_url) or re.match("%s/?$" % re.escape(re.sub("^http://", "https://", url)), request_url):
			tamper = config["tamper_mode"]["tamper"]
			flow.request.method = tamper["method"] if tamper["method"] else flow.request.method
			if tamper["headers"]: flow.request.headers.update(tamper["headers"])

			if config["tamper_mode"]["update"]: request_payload.update(tamper["payload"])
			elif config["tamper_mode"]["strict"]: request_payload = tamper["payload"]
			else: # Conservative mode
				print("="*40)
				print("TAMPERING %s" % request_url)
				for key in request_payload:
					if key not in tamper["payload"] or not request_payload[key] or re.search(r"token|csrf|nonce|transientkey|hpt", key, re.IGNORECASE):
						print("XHR SKIP: %s = %s (%s)" % (key, request_payload[key], tamper["payload"].get(key)))
						continue # Keep existing payload params not set by the tampering or the ones matching one of the nonce/token keywords, OR the ones that are empty (even if there was a provided value)
					print("XHR SET: %s = %s (%s)" % (key, request_payload[key], tamper["payload"][key]))
					request_payload[key] = tamper["payload"][key] # Keep same values and update empty ones. First check is implied; written for clarity

	if config["redirection_mode"]["enabled"]:
		global _seen_urls # Some websites might send async requests to the same resource (e.g. everhome.de) which is part of the flow. We need only one of them
		if not config["redirection_mode"]["flow"]: # On a new redirection flow, reset the known URLs
			_seen_urls = set()
		cur_url = config["redirection_mode"]["url"]

		if cur_url not in _seen_urls:
			matched = False
			if (re.match("%s/?$" % re.escape(cur_url), request_url) or re.match("%s/?$" % re.escape(re.sub("^http://", "https://", cur_url)), request_url)): matched = True
			
			if not matched and not config["redirection_mode"]["strict_query"]: # If instructed so, match against dynamic query values
				wildcard_url = get_wildcard_query(cur_url)
				if (re.match("%s/?$" % wildcard_url, request_url) or re.match("%s/?$" % re.sub("^http://", "https://", wildcard_url), request_url)):
					config["redirection_mode"]["url"] = request_url # Update the request URL so it'll be picked up by the response thread
					matched = True
			if matched:
				config["redirection_mode"]["flow"].append({"url" : request_url, "method" : flow.request.method, "payload" : dict(request_payload), "request_headers" : dict(flow.request.headers), "upgraded" : urlparse(cur_url).scheme == "http" and urlparse(request_url).scheme == "https"})
				_seen_urls.add(cur_url)
				dump_config(config)

	if crafted_response:
		flow.response = crafted_response # Return crafted response

@concurrent
def response(flow):
	config = fetch_config()

	request_url = URLUtils.normalize_url(flow.request.url)
	request_payload = flow.request.urlencoded_form
	if flow.request.multipart_form: request_payload = flow.request.multipart_form
	request_payload = dict(request_payload)

	if config["redirection_mode"]["enabled"]:
		cur_url = config["redirection_mode"]["url"]

		if re.match("%s/?$" % re.escape(cur_url), request_url) or re.match("%s/?$" % re.escape(re.sub("^http://", "https://", cur_url)), request_url):
			try:
				config["redirection_mode"]["flow"][-1]["response_headers"] = dict({k.lower() : v for k,v in list(flow.response.headers.items())}) # Lower-case all header names
				config["redirection_mode"]["flow"][-1]["status_code"] = flow.response.status_code
			except Exception as e: print(("---------> %s" % request_url))

			loc = flow.response.headers.get("location", "").strip()
			if loc:
				scheme = urlparse(cur_url).scheme
				domain = urlparse(cur_url).netloc
				new_redirect = None

				if re.search(r"^http(s)?://", loc, re.IGNORECASE): # truly absolute
					new_redirect = loc
				elif loc.startswith("//"): # Inherits scheme from triggering request, stll absolute URL
					new_redirect = "%s:%s" % (scheme, loc)
				elif config["redirection_mode"]["domain"] in loc.split("/")[0]: # Absolute, without scheme, but the domain should be before any path (e.g. /login.php?redirect=`domain`)
					new_redirect = "%s://%s" % (scheme, loc)
				else: # Relative
					new_redirect = "%s://%s%s%s" % (scheme, domain, "/" if not loc.startswith("/") else "", loc)

				# Domain includes port #; Chrome removes the default ports, so we need to do it as well
				if domain.endswith(":80") or domain.endswith(":443"): new_redirect = new_redirect.replace(":443", "").replace(":80", "")
				# Chrome adds a '/' right before the query, if it is missing and there is no path
				if not urlparse(new_redirect).path and urlparse(new_redirect).query: new_redirect = new_redirect.replace('?', '/?')

				new_redirect = new_redirect.split("#")[0] # Chrome drops the fragment in the Location header

				# config["redirection_mode"]["url"] = new_redirect
				try: config["redirection_mode"]["url"] = URLUtils.normalize_url(new_redirect)
				except Exception as ex:
					print(str(ex))
					print(loc)
					print(new_redirect)
			else:
				if config["redirection_mode"]["html"]: config["redirection_mode"]["flow"][-1]["html"] = flow.response.text # Store the landing page's source, if instructed to


			dump_config(config)

	if config["capture_mode"]["enabled"]: # -- ReScan
		with _capture_mode_lock: # Necessary lock to ensure integrity of the exhcanged HTTP messages list
			config["capture_mode"]["http_messages"].append(
				{
					"request" : {"url" : request_url, "method" : flow.request.method, "payload" : request_payload, "request_headers" : dict(flow.request.headers)},
					"response" : {"status_code" : flow.response.status_code, "response_headers" : dict({k.lower() : v for k,v in list(flow.response.headers.items())})}
				}
			)
		dump_config(config)