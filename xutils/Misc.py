#!/usr/bin/python3

from uuid import uuid4
import pickle as cp
import json

from os import path

import re
from bs4 import BeautifulSoup
from string import ascii_lowercase, ascii_uppercase, digits

try:
	import pandas as pd
	from scipy.stats import entropy
except Exception as ex: pass

def uuid():
	return str(uuid4())

def pload(filename):
	with open(filename, "rb") as fp:
		return cp.load(fp)

def pdump(obj, filename):
	with open(filename, "wb") as wfp:
		cp.dump(obj, wfp)


def jload(filename):
	with open(filename, "r") as fp:
		return json.load(fp)

def jdump(obj, filename, indent = 2):
	with open(filename, "w") as wfp:
		json.dump(obj, wfp, indent = indent)


def get_install_dir():
	return path.dirname(path.realpath(__file__)).split("xdriver")[0]

def generate_token(length = 8): # -- ReScan
	from random import choices
	return "".join(choices(ascii_uppercase + ascii_lowercase + digits, k = length))

def value_entropy(value): # -- ReScan
	return float(entropy(pd.Series(list(value)).value_counts()))

''' Given a piece of HTML code, add any missing opening or closing tags, in the correct order.
	If a value is given (any string, e.g. an XSS payload), do not consider it for the process. '''
def fix_html(html, value = None):
	self_closing_tags = set(["input", "img", "br", "hr", "area", "base", "col", "command", "embed", "keygen", "link", "meta", "param", "source", "track", "wbr"])
	html = html.replace("</html>", "").replace("</body>", "")
	
	check_html = html if not value else html[:html.index(value)] + "{{XDRIVER_VALUE}}" + html[html.index(value)+len(value):]
	
	opening_regex = r"<[^/<]+>"
	opening_tags = []
	for m in re.findall(opening_regex, check_html):
		try:
			tag = BeautifulSoup(m, "html5lib").find_all()[0].name.lower() # Should contain only one element
		except Exception as ex: continue
		if tag in self_closing_tags: continue # No need to account for self-closing tags
		opening_tags.append(tag)

	closing_regex = r"</([^<]+)>"
	closing_tags = []
	for tag in re.findall(closing_regex, check_html):
		if tag in self_closing_tags: continue
		closing_tags.append(tag.lower())

	to_remove = [] # Filter out already matching tags
	for tag in reversed(opening_tags): # Last occurence of opening tag should be mapped to first occurence of closing tag
		if tag in closing_tags:
			to_remove.append(tag)
			closing_tags.remove(tag)

	for tag in to_remove: # Remove matching opening tags
		opening_tags.reverse()
		opening_tags.remove(tag)
		opening_tags.reverse()

	for tag in reversed(opening_tags): html += "</%s>" % tag # Append missing closing tags
	for tag in closing_tags: html = "<%s>" % tag + html # Prepend missing opening tags

	return html