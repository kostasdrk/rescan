#!/usr/bin/python3

from .Exceptions import *
from .Misc import uuid
import sys

import logging

class Logger():

	_caller_prefix = "LOGGER"
	_verbose = True
	_logfile = None
	_logger = None
	_screenshot_dir = None
	_debug = False # Off by default
	_warning = True
	_driver = None
	_flush = True # Useful when used by subprocesses

	@classmethod
	def set_verbose(cls, verbose):
		cls._verbose = verbose

	@classmethod
	def set_logfile(cls, logfile):
		Logger._logfile = logfile
		
		Logger._logger = logging.getLogger("xdriver_logger")
		Logger._logger.setLevel(logging.DEBUG)
		
		file_handler = logging.FileHandler(logfile, mode = "w")
		file_handler.setFormatter(logging.Formatter("%(asctime)s:%(levelname)s: %(message)s", datefmt="%d/%m/%Y %H:%M:%S"))
		Logger._logger.addHandler(file_handler)
		Logger._logger.propagate = False

		Logger.spit("Setting logfile to: %s" % Logger._logfile, caller_prefix = Logger._caller_prefix)

	@classmethod
	def unset_logfile(cls):
		Logger.set_logfile(None)

	@classmethod
	def set_screenshot_dir(cls, dirname):
		Logger._screenshot_dir = dirname

	@classmethod
	def set_driver(cls, driver):
		Logger._driver = driver

	@classmethod
	def set_debug_on(cls):
		Logger._debug = True

	@classmethod
	def set_debug_off(cls): # Call if need to turn debug messages off
		Logger._debug = False

	@classmethod
	def set_warning_on(cls):
		Logger._warning = True

	@classmethod
	def set_warning_off(cls): # Call if need to turn warnings off
		Logger._warning = False

	@classmethod
	def spit(cls, msg, warning = False, debug = False, error = False, exception = False, caller_prefix = ""):
		if not Logger._logfile: Logger.set_logfile("/tmp/xdriver-%s.log" % uuid()) # Make sure we have a logfile 

		caller_prefix = "[%s]" % caller_prefix if caller_prefix else ""
		prefix = "[FATAL]" if error else "[DEBUG]" if debug else "[WARNING]" if warning else "[EXCEPTION]" if exception else ""
		txtcolor = TxtColors.FATAL if error else TxtColors.DEBUG if debug else TxtColors.WARNING if warning else "[EXCEPTION]" if exception else TxtColors.OK
		if Logger._verbose:
			# if not debug or Logger._debug:
			if (not debug and not warning) or (debug and Logger._debug) or (warning and Logger._warning):
				print("%s%s%s %s%s" % (txtcolor, caller_prefix, prefix, msg, TxtColors.ENDC))

		logging_msg = "%s %s" % (caller_prefix, msg)
		if debug: Logger._logger.debug(logging_msg)
		elif warning: Logger._logger.warning(logging_msg)
		elif exception: Logger._logger.exception(logging_msg)
		elif error: Logger._logger.error(logging_msg)
		else: Logger._logger.info(logging_msg)

		if Logger._flush:
			sys.stdout.flush()

	@classmethod
	def screenshot(cls, filename): # No filename extension needed; default to .png
		try:
			Logger._driver.save_screenshot("%s/%s.png" % (Logger._screenshot_dir, filename))
		except Exception as ex: # Catch it, we don't want these to be fatal
			Logger.spit("Failed to save screenshot for: %s" % filename, caller_prefix = Logger._caller_prefix)
			Logger.spit(stringify_exception(ex), caller_prefix = Logger._caller_prefix)

class TxtColors:
	OK = '\033[92m'
	DEBUG = '\033[94m'
	WARNING = "\033[93m"
	FATAL = '\033[91m'
	EXCEPTION = '\033[100m'
	ENDC = '\033[0m'

if __name__ == "__main__":
	Logger.spit("This is a regular message")
	Logger.set_debug_on()
	Logger.spit("DEBUG "*10, debug = True)
	Logger.spit("WARNING "*8, warning = True)
	Logger.spit("ERROR "*10, error = True)