#!/usr/bin/python3

import urllib.parse
import tldextract as tld
import socket

# Necessary to make tldextract quiet about a debug level log
from tldextract.tldextract import LOG
import logging
logging.basicConfig(level=logging.CRITICAL)

class URLUtils():

	''' Get the given URL's domain (sub-domains included).
	'''
	@classmethod
	def get_domain(cls, url, strip_www = False, strip_port = True):
		if url is None:
			return None
		domain = urllib.parse.urlparse(url).netloc
		domain = domain.split(":")[0] if strip_port else domain
		return domain if not strip_www else domain.replace("www.", "")

	@classmethod
	def get_main_domain(cls, url, suffix = False, port = False):
		if url is None:
			return None
		ret = tld.extract(url)
		uport = URLUtils.get_port(url)
		return ret.domain if not suffix else ret.domain + (".%s" % ret.suffix if ret.suffix else "") + (":%s" % uport if uport and port and suffix else "")

	@classmethod
	def get_full_domain(cls, url):
		if url is None:
			return None
		ret = tld.extract(url)
		if ret.subdomain:
			return "%s.%s.%s" % (ret.subdomain, ret.domain, ret.suffix)
		else:
			return "%s.%s" % (ret.domain, ret.suffix)

	@classmethod
	def get_subdomain(cls, url):
		if url is None:
			return None
		ret = tld.extract(url)
		return ret.subdomain

	@classmethod
	def get_path(cls, url, with_query = False):
		if url is None:
			return None
		path = urllib.parse.urlparse(url).path
		if with_query: query = URLUtils.get_query(url)
		if with_query and query: return "%s?%s" % (path, query)
		return path

	@classmethod
	def get_query(cls, url):
		return urllib.parse.urlparse(url).query if url else None

	@classmethod
	def get_query_values(cls, url): # -- ReScan
		query = URLUtils.get_query(url)
		return ["=".join(value.split("=")[1:]) for value in query.split("&") if value] if query else []

	@classmethod
	def get_query_parameters(cls, url): # -- ReScan
		query = URLUtils.get_query(url)
		return ["".join(param.split("=")[0]) for param in query.split("&") if param] if query else []

	@classmethod
	def get_query_as_list(cls, url): # Get a list of tuples (key, value) for each URL param # -- ReScan
		return [(param, value) for param, value in zip(URLUtils.get_query_parameters(url), URLUtils.get_query_values(url))]

	@classmethod
	def get_scheme(cls, url):
		return urllib.parse.urlparse(url).scheme if url else None

	@classmethod
	def get_port(cls, url):
		return urllib.parse.urlparse(url).port if url else None

	@classmethod
	def join(cls, domain, path):
		return "%s%s" % (domain, path) if domain.endswith("/") or path.startswith("/") else "%s/%s"% (domain, path)

	@classmethod
	def join_scheme(cls, scheme, url):
		return scheme+"://"+url

	@classmethod
	def strip_scheme(cls, url):
		if url is None:
			return None
		scheme = URLUtils.get_scheme(url)
		return url.replace(scheme+"://", "")

	@classmethod
	def get_main_url(cls, url):
		scheme = URLUtils.get_scheme(url)
		domain = URLUtils.get_domain(url)
		port = URLUtils.get_port(url)
		path = URLUtils.get_path(url)
		main_url = URLUtils.join("%s%s" % (domain, ":%s" % port if port else ""), path)
		return URLUtils.join_scheme(scheme, main_url) if scheme else main_url

	@classmethod
	def strip_fragment(cls, url):
		return url.split("#")[0]

	@classmethod
	def normalize_url(cls, url): # -- ReScan
		if not url: return url
		parameters = URLUtils.get_query_as_list(url)
		return "%s%s%s" % (URLUtils.get_main_url(url), "?" if parameters else "", "&".join(["%s=%s" % (param, value) for param, value in sorted(parameters, key = lambda p: p[0])]))

	@classmethod
	def payload_to_dict(cls, payload): # Convert given payload string to equivalent dict # -- ReScan
		payload_dict = {URLUtils.urldecode(key) : URLUtils.urldecode(value) for key, value in [token.split("=") if len(token.split("=")) == 2 else (token.split("=")[0], "=".join(token.split("=")[1:])) if len(token.split("=")) > 2 else (token.split("=")[0], "") for token in payload.split("&")]}
		payload_dict.pop('', None)
		return payload_dict
	@classmethod
	def dict_to_payload(cls, payload_dict): # Convert given dictionary to equivalent payload string # -- ReScan
		payload = ""
		for key, value in payload_dict.items(): payload += "%s%s=%s" % ("&" if payload else "", URLUtils.urldecode(key), URLUtils.urldecode(value))
		return payload

	@classmethod
	def urldecode(cls, url):
		if not url: return url
		if type(url) == str: return urllib.parse.unquote(url)
		elif type(url) == bytes: return urllib.parse.unquote(url.decode())
	@classmethod
	def urlencode(cls, url):
		return urllib.parse.quote_plus(url) if url else url

	@classmethod
	def is_valid_ip(cls, ip):
		try: socket.inet_aton(ip)
		except socket.error: return False
		return True